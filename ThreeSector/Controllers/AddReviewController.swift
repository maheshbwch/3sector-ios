//
//  AddReviewController.swift
//  ThreeSector
//
//  Created by BestWeb on 07/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit

class AddReviewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var overAllRating: UIImageView!
    @IBOutlet weak var productQualityRating: UIImageView!
    @IBOutlet weak var writeYourCommentTF: UITextField!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var productReview: FloatRatingView!
    @IBOutlet weak var overallReview: FloatRatingView!
    var id:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.layer.shadowColor = UIColor.gray.cgColor
        headerView.layer.shadowOpacity = 0.3
        headerView.layer.shadowOffset = CGSize.zero
        headerView.layer.shadowRadius = 6        //headerView.addShadow()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func tapSubmit(_ sender: UIButton) {
        self.AddReviewAPICall(param: ["reviewTitle" : self.titleTF.text!,"yourReview":writeYourCommentTF.text!,"ID" :id])

    }
    func AddReviewAPICall(param: [String:Any]){
        LoadingIndicatorView.show("Loading...")
        service.AddReviewAPI(param: param, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                         self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                        self.navigationController?.popViewController(animated: true)
                        
                    }else{
                        self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.msg, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }

}
