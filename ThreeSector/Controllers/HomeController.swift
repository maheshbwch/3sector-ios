//
//  HomeController.swift
//  ThreeSector
//
//  Created by BestWeb on 23/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import MXSegmentedControl
import Nuke
import UIView_Shimmer
class HomeController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,iCarouselDelegate,iCarouselDataSource , UITableViewDelegate,UITableViewDataSource,CategoriesDelegate{
    
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var slideView: iCarousel!
    @IBOutlet weak var collectionUndusties: UICollectionView!
    @IBOutlet weak var pagination: UIPageControl!
    @IBOutlet weak var experienceServicesCollection: UICollectionView!
    @IBOutlet weak var hotDealsCollection: UICollectionView!
    @IBOutlet weak var realestateCollection: UICollectionView!
    @IBOutlet weak var reviewMachineryCollection: UICollectionView!
    @IBOutlet weak var imgAds: UIImageView!
    @IBOutlet weak var tblFeaturedCompanies: UITableView!
    var timer = Timer()
    var tagValue : Int = 0
    @IBOutlet weak var heightTbldata: NSLayoutConstraint!
    @IBOutlet weak var btnBecomeAuthor: UIButton!
    @IBOutlet weak var subViewHeader: UIView!
    var categoryID : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCategoriesList()
        
        self.updateUI()

    }
    override func viewWillAppear(_ animated: Bool) {
        self.getCategoriesList()
        
        self.updateUI()
    }
    func updateUI(){
        slideView.delegate = self
        slideView.dataSource = self
        slideView.scrollSpeed = 1.2
        slideView.isPagingEnabled = true
        subViewHeader.layer.shadowColor = UIColor.gray.cgColor
        subViewHeader.layer.shadowOpacity = 0.3
        subViewHeader.layer.shadowOffset = CGSize.zero
        subViewHeader.layer.shadowRadius = 6
        let userDefault = UserDefaults.standard
        
        let isAuthMerch = userDefault.bool(forKey: "Author")
        btnBecomeAuthor.layer.shadowColor = UIColor.gray.cgColor
        btnBecomeAuthor.layer.shadowOpacity = 0.3
        btnBecomeAuthor.layer.shadowOffset = CGSize.zero
        btnBecomeAuthor.layer.shadowRadius = 6
        let role = s_default.value(forKey: "Role")
        if role as! String == "subscriber"{
            btnBecomeAuthor.setTitle("Become A Merchant", for: .normal)
        }else{
            btnBecomeAuthor.setTitle("Add Listing", for: .normal)
        }
        
//        if isAuthMerch == false{
//            btnBecomeAuthor.setTitle("Become An Author", for: .normal)
//        }else{
//            btnBecomeAuthor.setTitle("Add Listing", for: .normal)
//        }
        //btnBecomeAuthor.setTitle("Add Listing", for: .normal)
        slideView.decelerationRate = 0
        
        pagination.isHidden = false
        
    }
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 3
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let tempView = UIView()
        tempView.frame = CGRect(x: 0, y: 0, width: slideView.frame.width, height: slideView.frame.height)
        let imgView = UIImageView()
        imgView.frame = tempView.frame
        imgView.contentMode = .scaleToFill
        imgView.image = UIImage(named: "HeaderDummy")
        tempView.addSubview(imgView)
        return tempView
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        pagination.currentPage = slideView.currentItemIndex
    }
    func carouselWillBeginDragging(_ carousel: iCarousel) {
        timer.invalidate()
    }
    
    
    func carouselDidEndDragging(_ carousel: iCarousel, willDecelerate decelerate: Bool) {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true);
    }
    
    /// pagecontrol action
    @objc func update() {
        
        let carouselCurrentIndex = slideView.currentItemIndex
        if carouselCurrentIndex == 3 - 1{
            slideView.scrollToItem(at: 0, animated: true)
            pagination.currentPage = 0
        }else{
            slideView.scrollToItem(at: carouselCurrentIndex + 1, animated: true)
            pagination.currentPage = carouselCurrentIndex + 1
        }
        
    }
    @objc func updateTimer() {
        print("update")
        if slideView.currentItemIndex > 1{
            slideView.currentItemIndex = 0
            pagination.currentPage = 0
        }else{
            slideView.currentItemIndex = slideView.currentItemIndex + 1
            pagination.currentPage = slideView.currentItemIndex
        }
    }
    @IBAction func tapTopCategories(_ sender: UIButton) {
        performSegue(withIdentifier: "search", sender: self)
    }
    
    @IBAction func tapNearBy(_ sender: UIButton) {
        self.performSegue(withIdentifier: "Nearby", sender: self)
        
    }
    @IBAction func tapNews(_ sender: UIButton) {
        self.performSegue(withIdentifier: "News", sender: self)
    }
    @IBAction func tapQuestions(_ sender: UIButton) {
        self.performSegue(withIdentifier: "Questions", sender: self)
    }
    @IBAction func tapSelectCategory(_ sender: UIButton) {
        self.navigationController?.navigationBar.isHidden = true
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesListView") as! CategoriesListView
        popOverVC.delegate = self
        self.addChild(popOverVC)
        popOverVC.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    @IBAction func viewAllExperienceServices(_ sender: UIButton) {
    }
    @IBAction func viewAllHotDeals(_ sender: UIButton) {
    }
    @IBAction func viewAllRecomRealEstate(_ sender: UIButton) {
    }
    @IBAction func viewAllMachinery(_ sender: UIButton) {
    }
    @IBAction func tapSeeAllLatestJoined(_ sender: UIButton) {
    }
    @IBAction func tapBecomeAuthor(_ sender: UIButton) {
//        if btnBecomeAuthor.titleLabel?.text == "Add Listing"{
//            self.performSegue(withIdentifier: "Add Company", sender: self)
//        }else{
//            self.performSegue(withIdentifier: "BecomeMerchant", sender: self)
//        }
        self.performSegue(withIdentifier: "AddWebView", sender: self)
    }
    @IBAction func tapViewAllExperienced(_ sender: UIButton) {
        tagValue = 0
        performSegue(withIdentifier: "HomeToCategory", sender: self)
    }
    
    @IBAction func tapViewAllHotDeals(_ sender: UIButton) {
        tagValue = 1
        performSegue(withIdentifier: "HomeToCategory", sender: self)
    }
    @IBAction func tapViewAllRealEstate(_ sender: UIButton) {
        tagValue = 2
        performSegue(withIdentifier: "HomeToCategory", sender: self)
    }
    @IBAction func tapViewAllHighReview(_ sender: Any) {
        tagValue = 3
        performSegue(withIdentifier: "HomeToCategory", sender: self)
    }
    //MARK:- API CALL CATEGORIS By ID
    func getCategoriesListDetails(){
        LoadingIndicatorView.show("Loading...")
        service.getCategoryById(Id: categoryID, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                categoriesById = userInfo!
                if categoriesById.status == "success"{
                    self.experienceServicesCollection.delegate = self
                    self.experienceServicesCollection.dataSource = self
                    self.experienceServicesCollection.reloadData()
                    self.categoryID = categoriesList.data[1].term_id
                    self.getCategoriesListDetails1()
                }else if userInfo?.msg == "tokenExpired"{
                    let alertController = UIAlertController(title: "Alert", message: "Session Expired. Please Login again.", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                        self.performSegue(withIdentifier: "Logout", sender: self)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }else{
                    self.alert(message: userInfo!.msg)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    func getCategoriesListDetails1(){
        LoadingIndicatorView.show("Loading...")
        service.getCategoryById(Id: categoryID, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                categoriesById1 = userInfo!
                if categoriesById1.status == "success"{
                    self.hotDealsCollection.delegate = self
                    self.hotDealsCollection.dataSource = self
                    self.hotDealsCollection.reloadData()
                    self.categoryID = categoriesList.data[2].term_id
                    self.getCategoriesListDetails2()
                }else{
                    self.alert(message: userInfo!.msg)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    func getCategoriesListDetails2(){
        LoadingIndicatorView.show("Loading...")
        service.getCategoryById(Id: categoryID, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                categoriesById2 = userInfo!
                if categoriesById2.status == "success"{
                    self.realestateCollection.delegate = self
                    self.realestateCollection.dataSource = self
                    self.realestateCollection.reloadData()
                  self.categoryID = categoriesList.data[3].term_id

                    self.getCategoriesListDetails3()
                }else{
                    self.alert(message: userInfo!.msg)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    func getCategoriesListDetails3(){
        LoadingIndicatorView.show("Loading...")
        service.getCategoryById(Id: categoryID, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                categoriesById3 = userInfo!
                if categoriesById3.status == "success"{
                    self.reviewMachineryCollection.delegate = self
                    self.reviewMachineryCollection.dataSource = self
                    self.reviewMachineryCollection.reloadData()
                }else{
                    self.alert(message: userInfo!.msg)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    func selectedTitle(CategoryName: String, ID: Int) {
        //dropDownTF.text = CategoryName
    }
    
    func back() {
        
    }
    //MARK:- API CALL CATEGORIS LIST INDUSTRIES
    func getCategoriesList(){
        LoadingIndicatorView.show("Loading...")
        service.getCategoriesList(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                categoriesList = userInfo!
                if categoriesList.status == "success"{
                    self.collectionUndusties.delegate = self
                    self.collectionUndusties.dataSource = self
                    self.collectionUndusties.keyboardDismissMode = .onDrag
                    self.collectionUndusties.reloadData()
                    
                    self.categoryID = categoriesList.data[0].term_id
                    self.getCategoriesListDetails()
              
                    self.tblFeaturedCompanies.delegate = self
                    self.tblFeaturedCompanies.dataSource = self
                    self.tblFeaturedCompanies.reloadData()
                    self.heightTbldata.constant = 0
                }else{
                    self.alert(message: userInfo!.status)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! joinedCompanyCell
        //  cell.heightAnchor.constraint(equalToConstant: 286)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 273
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == experienceServicesCollection{
            return categoriesById.data.count
        }else if collectionView == hotDealsCollection{
            return categoriesById1.data.count

        }else if collectionView == realestateCollection{
            return categoriesById2.data.count

        }else if collectionView == reviewMachineryCollection{
            return categoriesById3.data.count

        }
        return categoriesList.data.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        if collectionView == experienceServicesCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! experienceCell
            cell.title.text = categoriesById.data[indexPath.row].postTitle
            cell.miniIcon.layer.borderWidth = 1
            cell.miniIcon.layer.masksToBounds = false
            cell.miniIcon.layer.borderColor = UIColor.clear.cgColor
            cell.miniIcon.layer.cornerRadius = cell.miniIcon.frame.height/2
            cell.miniIcon.clipsToBounds = true
            cell.btnCall.addTarget(self, action: #selector(self.callToCompany(_:)), for: .touchUpInside)
            cell.btnFav.addTarget(self, action: #selector(self.favouriteBtn(_:)), for: .touchUpInside)
            cell.btnFav.tag = indexPath.row
            
            if categoriesById.data[indexPath.row].oFavorite.isMyFavorite == true{
                cell.btnFav.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
                cell.btnFav.isSelected = true
            }else{
                cell.btnFav.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
                cell.btnFav.isSelected = false
                
            }
            cell.title2.text = categoriesById.data[indexPath.row].oTerm.name
            
            if !(categoriesById.data[indexPath.row].logo.isEmpty){
                
                if URL(string: (categoriesById.data[indexPath.row].logo)) != nil{
                    let imageURL = URL(string: (categoriesById.data[indexPath.row].logo))
                    Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                        placeholder: UIImage(named: "Category"),
                        transition: .fadeIn(duration: 0.20)
                    ), into: cell.image)
                }
            }else{
                //cell.image.image = #imageLiteral(resourceName: "rectangle-70")
                print("no profile image")
            }
            if !(categoriesById.data[indexPath.row].featuredImg.isEmpty){
                
                if URL(string: (categoriesById.data[indexPath.row].featuredImg)) != nil{
                    let imageURL = URL(string: (categoriesById.data[indexPath.row].featuredImg))
                    Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                        placeholder: UIImage(named: "Category"),
                        transition: .fadeIn(duration: 0.20)
                    ), into: cell.miniIcon)
                }
            }else{
               // cell.miniIcon.image = #imageLiteral(resourceName: "group-157")
                print("no profile image")
            }
            if !( categoriesById.data[indexPath.row].phone.isEmpty){
                cell.btnCall.setTitle(categoriesById.data[indexPath.row].phone, for: .normal)
            }else{
                cell.btnCall.setTitle("NA", for: .normal)
            }
         
            cell.experienceShadowView.layer.shadowColor = UIColor.gray.cgColor
            cell.experienceShadowView.layer.shadowOpacity = 0.3
            cell.experienceShadowView.layer.shadowOffset = CGSize.zero
            cell.experienceShadowView.layer.shadowRadius = 6
            
            return cell
            
        }else if collectionView == hotDealsCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! HotDealsCell
            cell.title.text = categoriesById1.data[indexPath.row].postTitle
            cell.miniIcon.layer.borderWidth = 1
            cell.miniIcon.layer.masksToBounds = false
            cell.miniIcon.layer.borderColor = UIColor.clear.cgColor
            cell.miniIcon.layer.cornerRadius = cell.miniIcon.frame.height/2
            cell.miniIcon.clipsToBounds = true
            cell.btnCall.addTarget(self, action: #selector(self.callToCompany1(_:)), for: .touchUpInside)
            cell.btnFav.addTarget(self, action: #selector(self.favouriteBtn1(_:)), for: .touchUpInside)
            cell.btnFav.tag = indexPath.row
            
            if categoriesById1.data[indexPath.row].oFavorite.isMyFavorite == true{
                cell.btnFav.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
                cell.btnFav.isSelected = true
            }else{
                cell.btnFav.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
                cell.btnFav.isSelected = false
                
            }
            cell.title2.text = categoriesById1.data[indexPath.row].oTerm.name

            
            if !(categoriesById1.data[indexPath.row].logo.isEmpty){
                
                if URL(string: (categoriesById1.data[indexPath.row].logo)) != nil{
                    let imageURL = URL(string: (categoriesById1.data[indexPath.row].logo))
                    Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                        placeholder: UIImage(named: "Category"),
                        transition: .fadeIn(duration: 0.20)
                    ), into: cell.image)
                }
            }else{
               // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
                print("no profile image")
            }
            if !(categoriesById1.data[indexPath.row].featuredImg.isEmpty){
                
                if URL(string: (categoriesById1.data[indexPath.row].featuredImg)) != nil{
                    let imageURL = URL(string: (categoriesById1.data[indexPath.row].featuredImg))
                    Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                        placeholder: UIImage(named: "Category"),
                        transition: .fadeIn(duration: 0.20)
                    ), into: cell.miniIcon)
                }
            }else{
               // cell.image.image = #imageLiteral(resourceName: "group-157")
                print("no profile image")
            }
            if !( categoriesById1.data[indexPath.row].phone.isEmpty){
                cell.btnCall.setTitle(categoriesById1.data[indexPath.row].phone, for: .normal)
            }else{
                cell.btnCall.setTitle("NA", for: .normal)
            }
            
            cell.experienceShadowView.layer.shadowColor = UIColor.gray.cgColor
            cell.experienceShadowView.layer.shadowOpacity = 0.3
            cell.experienceShadowView.layer.shadowOffset = CGSize.zero
            cell.experienceShadowView.layer.shadowRadius = 6
            return cell
            
        }else if collectionView == realestateCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! realEstate
            cell.title.text = categoriesById2.data[indexPath.row].postTitle
            cell.miniIcon.layer.borderWidth = 1
            cell.miniIcon.layer.masksToBounds = false
            cell.miniIcon.layer.borderColor = UIColor.clear.cgColor
            cell.miniIcon.layer.cornerRadius = cell.miniIcon.frame.height/2
            cell.miniIcon.clipsToBounds = true
            cell.btnCall.addTarget(self, action: #selector(self.callToCompany2(_:)), for: .touchUpInside)
            cell.btnFav.addTarget(self, action: #selector(self.favouriteBtn2(_:)), for: .touchUpInside)
            cell.btnFav.tag = indexPath.row
            
            if categoriesById2.data[indexPath.row].oFavorite.isMyFavorite == true{
                cell.btnFav.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
                cell.btnFav.isSelected = true
            }else{
                cell.btnFav.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
                cell.btnFav.isSelected = false
                
            }
            cell.title2.text = categoriesById2.data[indexPath.row].oTerm.name

            
            if !(categoriesById2.data[indexPath.row].logo.isEmpty){
                
                if URL(string: (categoriesById2.data[indexPath.row].logo)) != nil{
                    let imageURL = URL(string: (categoriesById2.data[indexPath.row].logo))
                    Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                        placeholder: UIImage(named: "Category"),
                        transition: .fadeIn(duration: 0.20)
                    ), into: cell.image)
                }
            }else{
               // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
                print("no profile image")
            }
            if !(categoriesById2.data[indexPath.row].featuredImg.isEmpty){
                
                if URL(string: (categoriesById2.data[indexPath.row].featuredImg)) != nil{
                    let imageURL = URL(string: (categoriesById2.data[indexPath.row].featuredImg))
                    Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                        placeholder: UIImage(named: "Category"),
                        transition: .fadeIn(duration: 0.20)
                    ), into: cell.miniIcon)
                }
            }else{
              //  cell.image.image = #imageLiteral(resourceName: "group-157")
                print("no profile image")
            }
            if !( categoriesById2.data[indexPath.row].phone.isEmpty){
                cell.btnCall.setTitle(categoriesById2.data[indexPath.row].phone, for: .normal)
            }else{
                cell.btnCall.setTitle("NA", for: .normal)
            }
            
            cell.experienceShadowView.layer.shadowColor = UIColor.gray.cgColor
            cell.experienceShadowView.layer.shadowOpacity = 0.3
            cell.experienceShadowView.layer.shadowOffset = CGSize.zero
            cell.experienceShadowView.layer.shadowRadius = 6
            return cell
            
        }else if collectionView == reviewMachineryCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! machineryCell
            cell.title.text = categoriesById3.data[indexPath.row].postTitle
            cell.miniIcon.layer.borderWidth = 1
            cell.miniIcon.layer.masksToBounds = false
            cell.miniIcon.layer.borderColor = UIColor.clear.cgColor
            cell.miniIcon.layer.cornerRadius = cell.miniIcon.frame.height/2
            cell.miniIcon.clipsToBounds = true
            cell.btnCall.addTarget(self, action: #selector(self.callToCompany3(_:)), for: .touchUpInside)
            cell.btnFav.addTarget(self, action: #selector(self.favouriteBtn3(_:)), for: .touchUpInside)
            cell.btnFav.tag = indexPath.row
            
            if categoriesById3.data[indexPath.row].oFavorite.isMyFavorite == true{
                cell.btnFav.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
                cell.btnFav.isSelected = true
            }else{
                cell.btnFav.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
                cell.btnFav.isSelected = false
                
            }
            cell.title2.text = categoriesById3.data[indexPath.row].oTerm.name

            
            if !(categoriesById3.data[indexPath.row].logo.isEmpty){
                
                if URL(string: (categoriesById3.data[indexPath.row].logo)) != nil{
                    let imageURL = URL(string: (categoriesById3.data[indexPath.row].logo))
                    Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                        placeholder: UIImage(named: "Category"),
                        transition: .fadeIn(duration: 0.20)
                    ), into: cell.image)
                }
            }else{
              //  cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
                print("no profile image")
            }
            if !(categoriesById3.data[indexPath.row].featuredImg.isEmpty){
                
                if URL(string: (categoriesById3.data[indexPath.row].featuredImg)) != nil{
                    let imageURL = URL(string: (categoriesById3.data[indexPath.row].featuredImg))
                    Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                        placeholder: UIImage(named: "Category"),
                        transition: .fadeIn(duration: 0.20)
                    ), into: cell.miniIcon)
                }
            }else{
              //  cell.image.image = #imageLiteral(resourceName: "group-157")
                print("no profile image")
            }
            if !( categoriesById3.data[indexPath.row].phone.isEmpty){
                cell.btnCall.setTitle(categoriesById3.data[indexPath.row].phone, for: .normal)
            }else{
                cell.btnCall.setTitle("NA", for: .normal)
            }
            
            cell.experienceShadowView.layer.shadowColor = UIColor.gray.cgColor
            cell.experienceShadowView.layer.shadowOpacity = 0.3
            cell.experienceShadowView.layer.shadowOffset = CGSize.zero
            cell.experienceShadowView.layer.shadowRadius = 6
            return cell
            
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! industryCell
        cell.lblListing.text = categoriesList.data[indexPath.row].name
        
        cell.image.layer.borderWidth = 1
        cell.image.layer.masksToBounds = false
        cell.image.layer.borderColor = UIColor.clear.cgColor
        cell.image.layer.cornerRadius = cell.image.frame.height/2
        cell.image.clipsToBounds = true
        cell.image.layer.backgroundColor = UIColor.lightGray.cgColor
        if !(categoriesList.data[indexPath.row].oIcon.url.isEmpty){
            
            if URL(string: (categoriesList.data[indexPath.row].oIcon.url)) != nil{
                let imageURL = URL(string: (categoriesList.data[indexPath.row].oIcon.url))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.image)
            }
        }else{
            // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        tagValue = indexPath.item
        performSegue(withIdentifier: "HomeToCategory", sender: self)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeToCategory"{
            let vc = segue.destination as! CategoryController
            vc.categoryId = categoriesList.data[tagValue].term_id
        }else if segue.identifier == "AddWebView"{
            let vc = segue.destination as! AddListingWebViewController
            let role = s_default.value(forKey: "Role")
            if role as! String == "subscriber"{
                vc.isBecomeAnAuthor = true
            }else{
                vc.isBecomeAnAuthor = false
            }
        }
    }
    @objc func callToCompany(_ sender: UIButton){
        dialNumber(number: categoriesById.data[sender.tag].phone)
    }
    @objc func callToCompany1(_ sender: UIButton){
        dialNumber(number: categoriesById1.data[sender.tag].phone)
    }
    @objc func callToCompany2(_ sender: UIButton){
        dialNumber(number: categoriesById2.data[sender.tag].phone)
    }
    @objc func callToCompany3(_ sender: UIButton){
        dialNumber(number: categoriesById3.data[sender.tag].phone)
    }
    func dialNumber(number : String) {
        if let phoneCallURL = URL(string: "tel://\(number)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    @objc func favouriteBtn(_ sender: UIButton){
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            sender.isSelected = false
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById.data[sender.tag].ID])
            })
            
        }else{
            sender.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            sender.isSelected = true
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById.data[sender.tag].ID])
            })
        }
    }
    @objc func favouriteBtn3(_ sender: UIButton){
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            sender.isSelected = false
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById3.data[sender.tag].ID])
            })
            
        }else{
            sender.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            sender.isSelected = true
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById3.data[sender.tag].ID])
            })
        }
    }
    @objc func favouriteBtn1(_ sender: UIButton){
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            sender.isSelected = false
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById1.data[sender.tag].ID])
            })
            
        }else{
            sender.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            sender.isSelected = true
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById1.data[sender.tag].ID])
            })
        }
    }
    @objc func favouriteBtn2(_ sender: UIButton){
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            sender.isSelected = false
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById2.data[sender.tag].ID])
            })
            
        }else{
            sender.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            sender.isSelected = true
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById2.data[sender.tag].ID])
            })
        }
    }
    //MARK:- API CALL ADD/REMOVE FAVUORITES
    func favouriesAdd(param: [String:Any]){
        LoadingIndicatorView.show("Loading...")
        service.favouritesAddRemoveAPI(param: param, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        if userInfo?.isAdded == "removed"{
                            
                            self.view.makeToast( "Removed as Favourite", duration: 2.0, position: .bottom)
                        }else{
                            self.view.makeToast( "Added as Favourite", duration: 2.0, position: .bottom)
                        }
                        
                    }else if userInfo?.msg == "tokenExpired"{
//                        self.tblData.reloadData()
//                        self.viewExpired.isHidden = false
                    }else{
                        self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }else{
                   // self.tblData.reloadData()
                    
                    self.view.makeToast(userInfo!.msg, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
}
class industryCell : UICollectionViewCell{
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lblListing: UILabel!
    
}
class experienceCell : UICollectionViewCell{
    @IBOutlet weak var experienceShadowView: UIView!
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var miniIcon: UIImageView!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnFav: UIButton!

}
class HotDealsCell : UICollectionViewCell{
    @IBOutlet weak var experienceShadowView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var btnFav: UIButton!

    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var miniIcon: UIImageView!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var time: UILabel!
}
class realEstate : UICollectionViewCell{
    @IBOutlet weak var experienceShadowView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var miniIcon: UIImageView!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var btnFav: UIButton!

}
class machineryCell : UICollectionViewCell{
    @IBOutlet weak var experienceShadowView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var miniIcon: UIImageView!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var btnFav: UIButton!

}
class joinedCompanyCell : UITableViewCell{
    
}
extension HomeController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionUndusties{
            let width = (collectionUndusties.frame.size.width) / 5.0
            return CGSize(width: width, height: 114.0)
            
        }
        let yourWidth = collectionView.bounds.width
        return CGSize(width: (yourWidth/1) - 150, height: 260)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}
