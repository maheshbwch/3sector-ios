//
//  FreeProfileController.swift
//  ThreeSector
//
//  Created by BestWeb on 22/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit

class FreeProfileController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var btnProfilePhoto: UIButton!
    @IBOutlet weak var btnSSMDocument: UIButton!
    @IBOutlet weak var companyNameTF: UITextField!
    @IBOutlet weak var companyRegisterTF: UITextField!
    @IBOutlet weak var companyAddressTF: UITextField!
    @IBOutlet weak var locationTF1: UITextField!
    @IBOutlet weak var locationTF2: UITextField!
    @IBOutlet weak var streetLocationTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var areaTF: UITextField!
    @IBOutlet weak var officeNumbTF: UITextField!
    @IBOutlet weak var faxNumberTF: UITextField!
    @IBOutlet weak var whatsAppNumbTF: UITextField!
    @IBOutlet weak var emaiTF: UITextField!
    @IBOutlet weak var websiteTF: UITextField!
    @IBOutlet weak var btnCheckTerms: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        
//        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height+300)
//        print(scrollView.contentSize)
//        
//    }
    @IBAction func tapProfilePhoto(_ sender: UIButton) {
    }
    @IBAction func tapSSMDocument(_ sender: UIButton) {
    }
    @IBAction func streetLocationDropDown(_ sender: UIButton) {
    }
    @IBAction func cityDropDown(_ sender: UIButton) {
    }
    @IBAction func areaDropDown(_ sender: UIButton) {
    }
    @IBAction func tapTermsAndConditions(_ sender: UIButton) {
    }
    @IBAction func tapSignUp(_ sender: UIButton) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
