//
//  SignUpController.swift
//  ThreeSector
//
//  Created by BestWeb on 22/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
class SignUpController: UIViewController {

    @IBOutlet weak var flowImage: UIImageView!
    @IBOutlet weak var btnMerchant: UIButton!
    @IBOutlet weak var btnFreeSignUp: UIButton!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var greenTickEmail: UIImageView!
    @IBOutlet weak var greenTickPassword: UIImageView!
    @IBOutlet weak var greenTickConfirm: UIImageView!
    @IBOutlet weak var btnGoBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    var selected : String = "Merchant"
    @IBOutlet weak var btnTermsAndConditions: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateUI()
        // Do any additional setup after loading the view.
    }
    func updateUI(){
        greenTickEmail.isHidden = true
        greenTickConfirm.isHidden = true
        greenTickPassword.isHidden = true
    }
    @IBAction func tapMerchant(_ sender: UIButton) {
        btnMerchant.setImage(UIImage.init(named: "SubscribeMerchantSelected"), for: .normal)
        btnFreeSignUp.setImage(UIImage.init(named: "FreeSignupUnSelected"), for: .normal)
        flowImage.image = #imageLiteral(resourceName: "SignUpTop1")
        selected = "Merchant"

    }
    
    @IBAction func tapFreeSignUp(_ sender: UIButton) {
        btnMerchant.setImage(UIImage.init(named: "SubscribeMerchantUnselected"), for: .normal)
        btnFreeSignUp.setImage(UIImage.init(named: "FreeSignupSelected"), for: .normal)
        flowImage.image = #imageLiteral(resourceName: "FreeSignUpTop1")
        selected = "Free"
    }
    @IBAction func tapGoBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func tapPrivacy(_ sender: UIButton) {
        if sender.isSelected{
            btnTermsAndConditions.setImage(#imageLiteral(resourceName: "ic_check_box_outline_blank"), for: .normal)
            sender.isSelected = false
        }else{
            btnTermsAndConditions.setImage(#imageLiteral(resourceName: "ic_check_box"), for: .normal)
            sender.isSelected = true

        }
    }
    @IBAction func dofacebook(_ sender: UIButton) {
        //        if TheGlobalPoolManager.hasConnection() == false{
        //            TheGlobalPoolManager.showProgressHud(Constants.ToastMessages.Check_Internet_Connection)
        //            return
        //        }
        let userDefult = UserDefaults.standard
        userDefult.setValue("", forKey: "Google access_tocken")
        userDefult.synchronize()
        let fbLoginManager : LoginManager = LoginManager()
        
        fbLoginManager.logIn(permissions: ["public_profile","email"], from: self) { (result, error) in
            
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        if((AccessToken.current) != nil){
                            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                                if (error == nil){
                                    let dict: NSDictionary = result as! NSDictionary
                                    if let token = AccessToken.current?.tokenString {
                                        print("tocken: \(token)")
                                        // self.tocken = token
                                        // let userDefult = UserDefaults.standard
                                        //  userDefult.setValue(token, forKey: "access_tocken")
                                        // userDefult.synchronize()
                                    }
                                    if let user : NSString = dict.object(forKey:"name") as! NSString? {
                                        print("user: \(user)")
                                        
                                    }
                                    if let id : NSString = dict.object(forKey:"id") as? NSString {
                                        print("id: \(id)")
                                    }
                                    if let email : NSString = (result! as AnyObject).value(forKey: "email") as? NSString {
                                        print("email: \(email)")
                                        //self.emailTF.text = email as String
                                        //self.passwordTF.text = "NoPassword"
                                        //                                        self.userName = email as String
                                        //                                        self.password = "NoPassword"
                                        //                                        self.authLogIn = "facebook"
                                        //                                        TheGlobalPoolManager.showProgressHud("Loading..")
                                        //
                                        //                                        self.loginAPI()
                                    }
                                }
                            })
                        }
                    }
                }
            }
        }
        //        TheGlobalPoolManager.hideProgressHud()
    }
    @IBAction func tapNext(_ sender: UIButton) {
        
                if(validateSignUp()){
                    
                    DispatchQueue.main.async(execute: {
                        LoadingIndicatorView.show("Creating...")
                        self.signUp_api()
                    })
                }
                
        }
        
        @IBAction func previous(_ sender: Any){
            
            self.navigationController?.popViewController(animated: true)
        }
        func signUp_api(){
            
            DispatchQueue.main.async {
                
                let emailtxt = self.emailTF.text!
                let passwordtxt = self.passwordTF.text!
                
               
                self.createCustomer(param: ["username" : self.userNameTF.text!,"email":emailtxt,"password" : passwordtxt,"isAgreeToPrivacyPolicy" : "true","isAgreeToTermsAndConditionals" : "true"])
            }
        }
        
        func createCustomer(param: [String:Any]){
            
            LoadingIndicatorView.show("Creating...")
            service.newUserRegisteration(param: param, completionHandler: {success, userInfo in
                DispatchQueue.main.async {
                    LoadingIndicatorView.hide()
                    
                    if success{
                        if userInfo?.message == "createdAccountSuccessfully"{
                            print((userInfo?.data)!)
                            s_default.set(token, forKey: "token")

                            loginUserID = ((userInfo?.data.userId)!)
                            s_default.synchronize()
                            token = userInfo!.token
                            s_default.set(self.emailTF.text!, forKey: "UserName")
                            s_default.set(self.passwordTF.text!, forKey: "Password")
                            self.navigationController?.popViewController(animated: true)

                        }else{
                            
                            self.view.makeToast( userInfo!.message, duration: 2.0, position: .bottom)
                        }
                    }else{
                        
                        self.view.makeToast(userInfo!.message, duration: 2.0, position: .bottom)
                    }
                }
                
            })
        }
        
        //MARK: -  Validation
        func validateSignUp() -> Bool {
            if (userNameTF.text?.count == 0 ) {
                
                alert(message: "", title: "Please enter your Username")
                return false
                
            }else if (emailTF.text?.count == 0 ) {
                
                alert(message: "", title: "Please enter your Email")
                return false
                
            }else if ( !isValidEmail(emailStr: emailTF.text! as String) ) {
                
                alert(message: "", title: "Please Enter a valid Email")
                return false
                
            }else if (passwordTF.text?.count == 0 ) {
                
                alert(message: "", title: "Please Enter your password")
                return false
                
            }else if ( (passwordTF.text?.count)! < 6 ) {
                alert(message: "", title: "Password should be minimum 6 characters")
                return false
                
            }
            
            return true
        }
        
        //MARK: -  UITextField Delegate Methods
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            
//            if textField == emailTF {
//                passwordTF.becomeFirstResponder()
//
//            }else if textField == passwordTF {
//                passwordTF.becomeFirstResponder()
//
//            } else if textField == userNameTF {
//                tapNext(self.btnNext)
//
//            }
//            textField.resignFirstResponder()
            return true;
       }


}
