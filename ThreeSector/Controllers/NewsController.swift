//
//  NewsController.swift
//  ThreeSector
//
//  Created by BestWeb on 04/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
class NewsController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate , UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tblNews: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getNewsData()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- API CALL GET NEWS
    func getNewsData(){
        LoadingIndicatorView.show("Loading...")
        service.getNews(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                newsDataDetails = userInfo!
                if newsDataDetails.status == "success"{
                    self.tblNews.delegate = self
                    self.tblNews.dataSource = self
                    self.tblNews.reloadData()
                }else{
                    self.alert(message: userInfo!.status)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsDataDetails.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! newsCell
        cell.lblTitle.text = newsDataDetails.data[indexPath.row].postTitle
        cell.lblSubTitle.text = newsDataDetails.data[indexPath.row].postContent
        cell.lblPersonNane.text = newsDataDetails.data[indexPath.row].oAuthor.displayName
        cell.lblDate.text = newsDataDetails.data[indexPath.row].postDate

                if !(newsDataDetails.data[indexPath.row].oAuthor.avatar.isEmpty){
        
                    if URL(string: (newsDataDetails.data[indexPath.row].oAuthor.avatar)) != nil{
                        let imageURL = URL(string: (newsDataDetails.data[indexPath.row].oAuthor.avatar))
                        Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                            placeholder: UIImage(named: "Category"),
                            transition: .fadeIn(duration: 0.20)
                        ), into: cell.imgPerson)
                    }
                }else{
                    // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
                    print("no profile image")
                }
        if !(newsDataDetails.data[indexPath.row].oFeaturedImg.large.isEmpty){
            
            if URL(string: (newsDataDetails.data[indexPath.row].oFeaturedImg.large)) != nil{
                let imageURL = URL(string: (newsDataDetails.data[indexPath.row].oFeaturedImg.large))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.imgNews)
            }
        }else{
            // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
        cell.imgPerson.layer.borderWidth = 1
        cell.imgPerson.layer.masksToBounds = false
        cell.imgPerson.layer.borderColor = UIColor.clear.cgColor
        cell.imgPerson.layer.cornerRadius = cell.imgPerson.frame.height/2
        cell.imgPerson.clipsToBounds = true       // cell.viewBG.layer.cornerRadius = 10
        cell.viewBG.layer.masksToBounds = true
        cell.viewBG.layer.cornerRadius = 10
        //cell.viewBG.roundCorners(cornerRadius: 6)
        cell.viewBG.addShadow()
        //  cell.heightAnchor.constraint(equalToConstant: 286)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! newsCollectionCell
        if indexPath.row == 0{
            cell.lblSelected.isHidden = false
        }else{
            cell.lblSelected.isHidden = true
        }
        cell.contentView.roundCorners(cornerRadius: 5)
//        cell.lblFeatured.text = categoriesList.data[indexPath.row].name
//        //cell.lblListing.text = String(format:"%i Listings",categoriesList.data[indexPath.row].count)
//        cell.image.layer.borderWidth = 1
//        cell.image.layer.masksToBounds = false
//        cell.image.layer.borderColor = UIColor.clear.cgColor
//        cell.image.layer.cornerRadius = cell.image.frame.height/2
//        cell.image.clipsToBounds = true
//        cell.image.layer.backgroundColor = UIColor.lightGray.cgColor
//        if !(categoriesList.data[indexPath.row].oIcon.url.isEmpty){
//
//            if URL(string: (categoriesList.data[indexPath.row].oIcon.url)) != nil{
//                let imageURL = URL(string: (categoriesList.data[indexPath.row].oIcon.url))
//                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
//                    placeholder: UIImage(named: "Category"),
//                    transition: .fadeIn(duration: 0.20)
//                ), into: cell.image)
//            }
//        }else{
//            // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
//            print("no profile image")
//        }
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }
    
}
class newsCollectionCell : UICollectionViewCell{
   
    @IBOutlet weak var lblSelected: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
}
class newsCell : UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblPersonNane: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgPerson: UIImageView!

}
extension NewsController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if collectionView == collectionUndusties{
//            let width = (collectionUndusties.frame.size.width) / 5.0
//            return CGSize(width: width, height: 114.0)
//            
//        }
        return CGSize(width: collectionView.bounds.width / 2, height: collectionView.bounds.height)

//        let yourWidth = collectionView.bounds.width
//        return CGSize(width: (yourWidth/1) - 150, height: 260)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}
