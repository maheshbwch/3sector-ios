//
//  ChatListController.swift
//  ThreeSector
//
//  Created by BestWeb on 24/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
import PhotosUI
import MaterialComponents
import FirebaseStorage
import Firebase
import Alamofire
class ChatListController: UIViewController,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,CameraDelegate,PhotoDelegate  {
  
    var selected: String = ""
    var isClick: Bool = false
    var isSkip: Bool = false
    var isFrom : String = ""
    //    func selectedTitle(data: data) {
    //
    //
    //    }
    func backToPhone() {
        let bottomController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraController") as! CameraController
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: bottomController)
        bottomSheet.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height * 2/3)
        bottomController.delegate1 = self
        present(bottomSheet, animated: true, completion: nil)
    }
    func backToImage() {
    }

    @IBOutlet weak var tapBack: UIButton!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var chatList: UITableView!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var writeMessageTF: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var viewExpired: UIView!
    @IBOutlet weak var viewSub: UIView!
    var receiverID : String = ""
    var receiverName : String = ""
    var receiverImage : String = ""
     var chatListModel: [ChatListDataModel]!
    var namesArray = [String]()
    var picker:UIImagePickerController?=UIImagePickerController()
    var selectedIndex = Int()
    var imageData = Data()
    var isClicked : Bool = false
    var isGenderClicked : Bool = false
    var isRaceClicked : Bool = false
    var isEducationClicked : Bool = false
    var startingPoint : Bool = false
    var selectedRace = String()
    var imgFileName:URL!
    var imgFileName1:URL!
    var observing: Bool = false
    var refresh = UIRefreshControl()
    var keyDate : String = ""
    var imageData1 = Data()
    var isClickedProfile : Bool = false
    var initValue : Int = 0
    var initValue1 : Int = 0
    var idIncome : String = ""
    @IBOutlet weak var viewImagePreview: UIView!
    @IBOutlet weak var imagePreview: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewImagePreview.isHidden = true
        //Camera
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                print("response",response)
            } else {
                
            }
        }
        
        //Photos
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    
                }
            })
        }else if photos == .authorized{
            print("authorized")
        }else if photos == .denied{
            print("authorized")
        }else if photos == .restricted{
            print("restricted")
        }
        chatList.register(UINib(nibName: "IncomingMsgCell", bundle: nil), forCellReuseIdentifier: "IncomingMsgCell")
        chatList.register(UINib(nibName: "OutgoingMsgCell", bundle: nil), forCellReuseIdentifier: "OutgoingMsgCell")
        self.tabBarController?.tabBar.isHidden = true
        //self.chatListModel = [ChatListDataModel]()
       self.updateChat()
        //self.getChatListData()
        // Do any additional setup after loading the view.
    }
    func updateChat(){
       // var count = self.chatListModel.count
        
        self.namesArray = []
        self.userIcon.layer.borderWidth = 1
        self.userIcon.layer.masksToBounds = false
        self.userIcon.layer.borderColor = UIColor.clear.cgColor
        self.userIcon.layer.cornerRadius = self.userIcon.frame.height/2
        let userID = s_default.integer(forKey: "user_id")
        let userID2 = Int(receiverID)!
        // self.chatListModel = [ChatListDataModel]()
        //LoadingIndicatorView.show("loading..")
        self.userName.text = receiverName
        if !(receiverImage.isEmpty){
            
            if URL(string: (receiverImage)) != nil{
                let imageURL = URL(string: (receiverImage))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: self.userIcon)
            }
        }else{
            //self.logoTop.image = #imageLiteral(resourceName: "rectangle-408")
            print("no profile image")
        }
        let large = max(userID, userID2)
        let mini = min(userID, userID2)
        let msgKey = String(format:"%i_%i",mini,large)
        let query = Constants.refs.databaseRoot.child("Messages").child(msgKey)
        
        _ = query.observe(.childAdded, with: { snapshot in
            
            if  let data        = snapshot.value as? [String: AnyObject],
                let messageDate          = data["message_date"],
                let messageKey           = data["message_key"],
                let senderId             = data["sender_id"],
                let type                 = data["type"],
                let messageText          = data["message_text"],
                !(messageText as! String).isEmpty
            {
                
                let order = ChatListDataModel.init(messageKey: messageKey as? String, messageDate: messageDate as! String, messageText: messageText as! String, senderID: senderId as! String, messageType: type as! String)
                //                    for snap in snapshot.children {
                //                    let userSnap = snap as! DataSnapshot
                //                    let uid = userSnap.key
                //                    let userDict = userSnap.value as! [String:AnyObject]
                //                    let name = userDict["name"] as! String
                //                    self.namesArray.append(name)
                //                }
                //                print(self.namesArray)
                if self.chatListModel == nil
                {
                    self.chatListModel = [ChatListDataModel]()
                }
                //                if let bool = self?.startingPoint{
                //                    self?.startingPoint = bool
                //                }else{
                //                    self?.startingPoint = true
                //                }
                
                //                if self.startingPoint == true{
                //                    if self.keyDate.count == 0{
                //                        self.startingPoint = false
                //                    }
                //                    if self.keyDate == order.messageDate{
                //                       // self.chatListModel.append(order)
                //                       self.startingPoint = false
                //                }else{
                //                        self.chatListModel.removeAll()
                //
                //
                //                    }
                //                }else{
                ////                     if self.keyDate == order.messageDate{
                ////                        self.initValue = self.initValue + 1
                ////                        let value = self.initValue+self.initValue1
                ////                        if self.initValue == count{
                ////                            count = self.chatListModel.count+1
                ////                        }else{
                ////                        if let dateVa =  self.chatListModel[self.initValue].messageDate as? String{
                ////                                self.keyDate = dateVa
                ////                        }
                ////                        } //self.initValue = self.initValue - 1
                ////                       // return
                ////                     }else{
                //                    self.chatListModel.append(order)  // Here
                //                    self.keyDate = self.chatListModel[0].messageDate!
                //                    self.initValue1 = self.initValue1 + 1
                //                  //  }
                //                  //  self.startingPoint = true
                //
                //                }
                
                    self.chatListModel.append(order)
                self.idIncome = order.messageDate!
                if self.chatListModel.count > 0{
                    
                    self.chatList.delegate = self
                    self.chatList.dataSource = self
                    self.chatList.reloadData()
                    
                    LoadingIndicatorView.hide()
                    
                }else{
                    LoadingIndicatorView.hide()
                    
                    //  self!.viewExpired.isHidden = false
                    // self!.viewSub.isHidden = true
                }
                
            }
            
            self.refresh.endRefreshing()
            
        })
        
    }
    func updateChat1(){
        var count = self.chatListModel.count
        self.namesArray = []
        self.userIcon.layer.borderWidth = 1
        self.userIcon.layer.masksToBounds = false
        self.userIcon.layer.borderColor = UIColor.clear.cgColor
        self.userIcon.layer.cornerRadius = self.userIcon.frame.height/2
        let userID = s_default.integer(forKey: "user_id")
        let userID2 = Int(receiverID)!
       // self.chatListModel = [ChatListDataModel]()
        //LoadingIndicatorView.show("loading..")
        self.userName.text = receiverName
        if !(receiverImage.isEmpty){
            
            if URL(string: (receiverImage)) != nil{
                let imageURL = URL(string: (receiverImage))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: self.userIcon)
            }
        }else{
            //self.logoTop.image = #imageLiteral(resourceName: "rectangle-408")
            print("no profile image")
        }
        let large = max(userID, userID2)
        let mini = min(userID, userID2)
        let msgKey = String(format:"%i_%i",mini,large)
        let query = Constants.refs.databaseRoot.child("Messages").child(msgKey)
        self.chatListModel = [ChatListDataModel]()

        _ = query.observe(.childAdded, with: { snapshot in
            
            if  let data        = snapshot.value as? [String: AnyObject],
                let messageDate          = data["message_date"],
                let messageKey           = data["message_key"],
                let senderId             = data["sender_id"],
                let type                 = data["type"],
                let messageText          = data["message_text"],
                !(messageText as! String).isEmpty
            {
                if self.idIncome == messageDate as! String{
                    self.idIncome = messageDate as! String
                    return
                }
                let order = ChatListDataModel.init(messageKey: messageKey as? String, messageDate: messageDate as! String, messageText: messageText as! String, senderID: senderId as! String, messageType: type as! String)
//                    for snap in snapshot.children {
//                    let userSnap = snap as! DataSnapshot
//                    let uid = userSnap.key
//                    let userDict = userSnap.value as! [String:AnyObject]
//                    let name = userDict["name"] as! String
//                    self.namesArray.append(name)
//                }
//                print(self.namesArray)
                if self.chatListModel == nil
                {
                    self.chatListModel = [ChatListDataModel]()
                }
//                if let bool = self?.startingPoint{
//                    self?.startingPoint = bool
//                }else{
//                    self?.startingPoint = true
//                }
                
//                if self.startingPoint == true{
//                    if self.keyDate.count == 0{
//                        self.startingPoint = false
//                    }
//                    if self.keyDate == order.messageDate{
//                       // self.chatListModel.append(order)
//                       self.startingPoint = false
//                }else{
//                        self.chatListModel.removeAll()
//
//
//                    }
//                }else{
////                     if self.keyDate == order.messageDate{
////                        self.initValue = self.initValue + 1
////                        let value = self.initValue+self.initValue1
////                        if self.initValue == count{
////                            count = self.chatListModel.count+1
////                        }else{
////                        if let dateVa =  self.chatListModel[self.initValue].messageDate as? String{
////                                self.keyDate = dateVa
////                        }
////                        } //self.initValue = self.initValue - 1
////                       // return
////                     }else{
//                    self.chatListModel.append(order)  // Here
//                    self.keyDate = self.chatListModel[0].messageDate!
//                    self.initValue1 = self.initValue1 + 1
//                  //  }
//                  //  self.startingPoint = true
//
//                }
                
                self.chatListModel.append(order)
                self.idIncome = order.messageDate!
                if self.chatListModel.count > 0{
                    
                    //self.chatList.delegate = self
                    //self.chatList.dataSource = self
                    self.chatList.reloadData()
                  
                    LoadingIndicatorView.hide()
                    
                }else{
                    LoadingIndicatorView.hide()
                    
                    //  self!.viewExpired.isHidden = false
                   // self!.viewSub.isHidden = true
                }
                
            }
           
            self.refresh.endRefreshing()

        })
        
    }
    func sendMessage(type:String,message:String){
       
        let userID = s_default.integer(forKey: "user_id")
        let userID1 = s_default.string(forKey: "user_id")
        
        let userID2 = Int(receiverID)!
        let userImage = s_default.string(forKey: "Image")
        let timeStamp = Date.currentTimeStamp
        print(timeStamp)
        let timeStampStr = String(timeStamp)
        
        
        let large = max(userID, userID2)
        let mini = min(userID, userID2)
        let msgKey = String(format:"%i_%i",mini,large)
        let ref = Constants.refs.databaseRoot.child("Messages").child(msgKey).childByAutoId()

        let userData = ["message_date" : timeStampStr,
                        "message_key" : msgKey,
                        "message_text" : message,
                        "type" : type,
                        "sender_id" : userID1] as [String : Any]
        
        
        ref.setValue(userData)
        
        self.writeMessageTF.resignFirstResponder()
        
        self.writeMessageTF.placeholder = "Write a message here.."
//        if self.chatListModel == nil{
//           // self.chatListModel.removeAll()
//        }else{
//
//           self.chatListModel.e
//       }
        //LoadingIndicatorView.show("Loading...")
        self.startingPoint = true
       self.chatListModel.removeAll()
        self.sendPushNotification()
      
    }
    func sendPushNotification(){
        let query = Constants.refs.databaseRoot.child("Notifications").observe(.childAdded, with: {  snapshot in
            var fcmToken : String = ""
            if snapshot.exists() {
                print("we have that artist, the id is \(snapshot.key)")
            } else {
                print("we don't have that, add it to the DB now")
            }
            //if  let data        = snapshot.value as? [String: AnyObject],
//            if self.receiverID == snapshot.key{
//                let y = snapshot.value(forKey: self.receiverID)
//                fcmToken          =  snapshot.value(forKey: self.receiverID) as! String
//                if fcmToken.count == 0{
//                    DispatchQueue.main.async {
//                        self.writeMessageTF.text = ""
//                        self.updateChat1()
//                         self.chatListModel.removeAll()
//                    }
//                }
//            }
            
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "key=AAAARsUK3vk:APA91bHAAhOyEJjtfIqbmbRxT08-xalVR3tROHqVsUFlmWEDvK2Rx1qbMVcUSQwMV1vO8VWGnYkLFqEI_5ARoLblTlGiOn6wiqnJB-j9p-FT6SwtwE6UhvKq0lyXHtEZvnSfz8sRlA5F",
           
        ]
        let parameters = [
            "to": fcmToken,
            "notification": [
                "body": "Message",
                "title": self.writeMessageTF.text!
            ]
            ] as [String : Any]
        
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
            self.writeMessageTF.text = ""
        let request = NSMutableURLRequest(url: NSURL(string: "https://fcm.googleapis.com/fcm/send")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as! Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                DispatchQueue.main.async {
                  //  self!.chatListModel = [ChatListDataModel]()
                    self.updateChat1()
                }
                self.chatListModel.removeAll()
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
            }
        })
        
        dataTask.resume()
        
            //query.removeAllObservers()
        })
    }
    @IBAction func tapsend(_ sender: UIButton) {
        self.sendMessage(type: "text", message: writeMessageTF.text!)
    }
    @IBAction func tapLogIn(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        self.performSegue(withIdentifier: "LogIn", sender: self)
        
    }
    
    func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "H:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
        
        return dateFormatter.string(from: dt!)
    }


        
        func sendMessage(param: [String:Any]){
            
            LoadingIndicatorView.show("Sending...")
            service.sendMessageAPI(param: param, completionHandler: {success, userInfo in
                DispatchQueue.main.async {
                    LoadingIndicatorView.hide()
                    
                    if success{
                        if userInfo?.status == "success"{
                            self.writeMessageTF.resignFirstResponder()
                            self.writeMessageTF.text = ""
                            self.writeMessageTF.placeholder = "Write a message here.."
                           self.viewSub.isHidden = false
                            self.viewExpired.isHidden = true
                        }else if userInfo?.msg == "token Expired"{
                            self.viewSub.isHidden = true
                            self.viewExpired.isHidden = false
                        }else{
//                            self.viewSub.isHidden = true
//                            self.viewExpired.isHidden = false
                            self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                        }
                    }else{
                        
                        self.view.makeToast(userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }
                
            })
        }
        
    @IBAction func tapChooseAttachment(_ sender: UIButton) {
        let bottomController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraController") as! CameraController
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: bottomController)
        bottomSheet.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height * 2/3)
        bottomController.delegate1 = self
        present(bottomSheet, animated: true, completion: nil)
    }
    func photoTapped(index:Int){
        if index == 0{
            selectedIndex = index
            openGallary()
        }else{
            selectedIndex = index
            openCamera()
        }
    }
    
    func openGallary()
    {
        picker?.delegate = self
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker!.allowsEditing = false
            picker?.delegate = self
            picker!.sourceType = UIImagePickerController.SourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("inf0",info)
        var chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        
        imageData = chosenImage.jpegData(compressionQuality: 0.3)!
        if isClickedProfile == true{
            self.userIcon.image = chosenImage
            self.userIcon.layer.cornerRadius = self.userIcon.frame.height/2
            self.userIcon.layer.borderColor = UIColor.lightGray.cgColor
            self.userIcon.layer.borderWidth = 0.5
            self.userIcon.contentMode = .scaleToFill
            imageData1 = chosenImage.jpegData(compressionQuality: 0.3)!
            
            if let data = chosenImage.pngData() {
                imgFileName = getDocumentsDirectory().appendingPathComponent("image.png")
            }
        }else{
          //  self.bgCoverImg.image = chosenImage
            //self.bgCoverImg.layer.cornerRadius = self.bgCoverImg.frame.height/2
            //self.bgCoverImg.layer.borderColor = UIColor.lightGray.cgColor
            //self.bgCoverImg.layer.borderWidth = 0.5
           // self.bgCoverImg.contentMode = .scaleToFill
            imageData = chosenImage.jpegData(compressionQuality: 0.3)!
            
            if let data = chosenImage.pngData() {
                imgFileName1 = getDocumentsDirectory().appendingPathComponent("image.png")
            }
        }
        viewSub.isHidden = true
        viewExpired.isHidden = true
        viewImagePreview.isHidden = false
        viewImagePreview.bringSubviewToFront(self.view)
        imagePreview.image = chosenImage
//        let data: Data = chosenImage.jpegData(compressionQuality: 0.3)!
//
//        // ref should be like this
//        let ref = Storage.storage().reference(withPath: "Images/" + "1" + "/" + "02032020")
//        ref.putData(data, metadata: nil,
//                    completion: { (meta , error) in
//                        if error == nil {
//                            // return url
//                            ref.downloadURL(completion: { (url, error) in
//
//                                print(url?.absoluteString)
//                               // completion(((url?.absoluteString) != nil), "")
//                            })
//
//                        } else {
//                           // completion(false, "")
//                        }
//        })
        
        print("chosenImage",chosenImage)
        
        isClick = true
        
        dismiss(animated: true, completion:  nil)
    }
    
    func imgUploading(data: Data){
        
        
    }
    
    @IBAction func tapDone(_ sender: UIButton) {
        viewSub.isHidden = false
        viewExpired.isHidden = false
        viewImagePreview.isHidden = true
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "ddMMyyyyhh:mm:ssa"
        let convertedDate: String = dateFormatter.string(from: currentDate)
        let userID1 = s_default.string(forKey: "user_id")
        if let data = imagePreview.image!.jpegData(compressionQuality: 0.3) { // convert your UIImage into Data object using png representation
            FirebaseStorageManager().uploadImageData(data: data, serverFileName: String(format:"%@%@.png",convertedDate,userID1!)) { (isSuccess, url) in
                print("uploadImageData: \(isSuccess), \(url)")
                self.sendMessage(type: "Image", message: url!)

                
            }
        }
    }
    @IBAction func tapCancel(_ sender: UIButton) {
        viewSub.isHidden = false
        viewExpired.isHidden = false
        viewImagePreview.isHidden = true
    }
    
}
//MARK:-----TableView Methods------
extension ChatListController : UITableViewDataSource,UITableViewDelegate {
    //Messages delegate methods
    //        func numberOfSections(in tableView: UITableView) -> Int {
    //                return self.getChatHeadsDetailsModel.result.count
    //    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatListModel.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let senderID = s_default.string(forKey: "user_id")
        let userID2 = Int(receiverID)!
        if self.chatListModel.count == 0{
            
        }else{
        if senderID == self.chatListModel[indexPath.row].senderID {
            let cell = chatList.dequeueReusableCell(withIdentifier: "OutgoingMsgCell") as! OutgoingMsgCell
            if self.chatListModel[indexPath.row].messageType == "text"{
                cell.outGoingText.text = self.chatListModel[indexPath.row].messageText
                cell.imgOutgoing.isHidden = true
                cell.viewOutGoing.isHidden = false
            }else{
                cell.imgOutgoing.isHidden = false
                cell.viewOutGoing.isHidden = true
                if !(self.chatListModel[indexPath.row].messageText!.isEmpty){
                    
                    if URL(string: (self.chatListModel[indexPath.row].messageText!)) != nil{
                        let imageURL = URL(string: (self.chatListModel[indexPath.row].messageText!))
                        Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                            placeholder: UIImage(named: "Category"),
                            transition: .fadeIn(duration: 0.20)
                        ), into: cell.imgOutgoing!)
                    }
                }else{
                    //self.logoTop.image = #imageLiteral(resourceName: "rectangle-408")
                    print("no profile image")
                }

            }
                cell.imageOut.image = #imageLiteral(resourceName: "avatar-default")
//            if !(self.chatListModel[indexPath.row].messageText.isEmpty){
//
//                if URL(string: (receiverImage)) != nil{
//                    let imageURL = URL(string: (receiverImage))
//                    Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
//                        placeholder: UIImage(named: "Category"),
//                        transition: .fadeIn(duration: 0.20)
//                    ), into: cell.imgIncoming!)
//                }
//            }else{
//                //self.logoTop.image = #imageLiteral(resourceName: "rectangle-408")
//                print("no profile image")
//            }
            let date = Date(timeIntervalSince1970: TimeInterval(self.chatListModel[indexPath.row].messageDate!) as! TimeInterval)
            
            cell.timeOut.text = self.convertTimestamp(serverTimestamp: Double(self.chatListModel[indexPath.row].messageDate!) as! Double)

            cell.imageOut.layer.borderWidth = 1
            cell.imageOut.layer.masksToBounds = false
            cell.imageOut.layer.borderColor = UIColor.clear.cgColor
            cell.imageOut.layer.cornerRadius = cell.imageOut.frame.height/2
            cell.imageOut.clipsToBounds = true
                //cell.timeOut.text = self.chatListModel[indexPath.row].messageDate
            
            return cell
        }
        else{
            let cell = chatList.dequeueReusableCell(withIdentifier: "IncomingMsgCell") as! IncomingMsgCell
//            if self.idIncome == self.chatListModel[indexPath.row].messageDate!{
//
//            }else{
            if self.chatListModel[indexPath.row].messageType == "text"{
                cell.incomingMessage.text = self.chatListModel[indexPath.row].messageText
                cell.incomingImg.isHidden = true
                cell.incomingView.isHidden = false
            }else{
                cell.incomingImg.isHidden = false
                cell.incomingView.isHidden = true
                if !(self.chatListModel[indexPath.row].messageText!.isEmpty){
                    
                    if URL(string: (self.chatListModel[indexPath.row].messageText!)) != nil{
                        let imageURL = URL(string: (self.chatListModel[indexPath.row].messageText!))
                        Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                            placeholder: UIImage(named: "Category"),
                            transition: .fadeIn(duration: 0.20)
                        ), into: cell.incomingImg!)
                    }
                }else{
                    //self.logoTop.image = #imageLiteral(resourceName: "rectangle-408")
                    print("no profile image")
                }
                
            }
            self.idIncome = self.chatListModel[indexPath.row].messageDate!
            let date = Date(timeIntervalSinceReferenceDate: Double(self.chatListModel[indexPath.row].messageDate!) as! TimeInterval)
            cell.incomingTime.text = self.convertTimestamp(serverTimestamp: Double(self.chatListModel[indexPath.row].messageDate!) as! Double)

          //  cell.incomingTime.text = self.chatListModel[indexPath.row].messageDate
            if !(receiverImage.isEmpty){
                
                if URL(string: (receiverImage)) != nil{
                    let imageURL = URL(string: (receiverImage))
                    Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                        placeholder: UIImage(named: "Category"),
                        transition: .fadeIn(duration: 0.20)
                    ), into: cell.imgIncoming!)
                }
            }else{
                //self.logoTop.image = #imageLiteral(resourceName: "rectangle-408")
                print("no profile image")
            }
           // cell.imgIncoming.image = #imageLiteral(resourceName: "rectangle-408")
            cell.imgIncoming.layer.borderWidth = 1
            cell.imgIncoming.layer.masksToBounds = false
            cell.imgIncoming.layer.borderColor = UIColor.clear.cgColor
            cell.imgIncoming.layer.cornerRadius = cell.imgIncoming.frame.height/2
            cell.imgIncoming.clipsToBounds = true

//            let formatter1 = DateFormatter()
//            // initially set the format based on your datepicker date / server String
//
//            formatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
//
//
//            if let yourDate1 = formatter1.date(from: getChatHeaders.oResults[indexPath.row].oProfile.oMessage.at) as? Date{
//            //let yourDate1 = formatter1.date(from: getChatHeaders.oResults[indexPath.row].oProfile.oMessage.at)!
//
//            formatter1.dateFormat = "h:mm a"
//
//            let myStringafd1 = formatter1.string(from: yourDate1)
           // }
            return cell
        }
        }
            let cell = chatList.dequeueReusableCell(withIdentifier: "IncomingMsgCell") as! IncomingMsgCell
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 84
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    func convertTimestamp(serverTimestamp: Double) -> String {
        let x = serverTimestamp / 1000
        let date = NSDate(timeIntervalSince1970: x)
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        
        return formatter.string(from: date as Date)
    }
    
    func relativeDate(for date:Date) -> String {
        let components = Calendar.current.dateComponents([.day, .year, .month, .weekOfYear], from: date, to: Date())
        if let year = components.year, year == 1{
            return "\(year) year ago"
        }
        if let year = components.year, year > 1{
            return "\(year) years ago"
        }
        if let month = components.month, month == 1{
            return "\(month) month ago"
        }
        if let month = components.month, month > 1{
            return "\(month) months ago"
        }
        
        if let week = components.weekOfYear, week == 1{
            return "\(week) week ago"
        }
        if let week = components.weekOfYear, week > 1{
            return "\(week) weeks ago"
        }
        
        if let day = components.day{
            if day > 1{
                return "\(day) days ago"
            }else{
                "Yesterday"
            }
        }
        return "Today"
    }
}
class FirebaseStorageManager {
    
    public func uploadFile(localFile: URL, serverFileName: String, completionHandler: @escaping (_ isSuccess: Bool, _ url: String?) -> Void) {
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        // Create a reference to the file you want to upload
        let directory = "Images/"
        let fileRef = storageRef.child(directory + serverFileName)
        
        _ = fileRef.putFile(from: localFile, metadata: nil) { metadata, error in
            fileRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    completionHandler(false, nil)
                    return
                }
                // File Uploaded Successfully
                completionHandler(true, downloadURL.absoluteString)
            }
        }
    }
    
    public func uploadImageData(data: Data, serverFileName: String, completionHandler: @escaping (_ isSuccess: Bool, _ url: String?) -> Void) {
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        // Create a reference to the file you want to upload
        let directory = "Images/"
        let fileRef = storageRef.child(directory + serverFileName)
        
        _ = fileRef.putData(data, metadata: nil) { metadata, error in
            fileRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    completionHandler(false, nil)
                    return
                }
                // File Uploaded Successfully
                completionHandler(true, downloadURL.absoluteString)
            }
        }
    }
    
}
