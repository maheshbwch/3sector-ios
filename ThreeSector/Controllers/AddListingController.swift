//
//  AddListingController.swift
//  ThreeSector
//
//  Created by BestWeb on 31/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit

class AddListingController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblList: UITableView!
    var titles = ["ADD COMPANY","ADD JOB","ADD PROJECT"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tblList.delegate = self
        tblList.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! listingCell
        if indexPath.row == 0{
            cell.viewBG.backgroundColor = #colorLiteral(red: 0.9412739277, green: 0.3842895031, blue: 0.5740821362, alpha: 1)
            cell.title.text = "Add Company"
        }else {
            cell.viewBG.backgroundColor = #colorLiteral(red: 0.2190509439, green: 0.711820066, blue: 0.7835419774, alpha: 1)
            cell.title.text = "Add Job"

        }
//        else{
//            cell.viewBG.backgroundColor = #colorLiteral(red: 0.8728467822, green: 0.2057414651, blue: 0.9121063352, alpha: 1)
//            cell.title.text = "Add Company"
//
//        }
        //  cell.heightAnchor.constraint(equalToConstant: 286)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 167
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if indexPath.row == 0{
            performSegue(withIdentifier: "AddCompany", sender: self)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class listingCell : UITableViewCell{
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var title: UILabel!

}
