//
//  EditProfileController.swift
//  ThreeSector
//
//  Created by BestWeb on 13/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
import MaterialComponents
import PhotosUI
class EditProfileController: UIViewController,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,CameraDelegate,PhotoDelegate  {
  
    var selected: String = ""
    var isClick: Bool = false
    var isSkip: Bool = false
    var isFrom : String = ""
//    func selectedTitle(data: data) {
//
//
//    }
    func backToPhone() {
        let bottomController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraController") as! CameraController
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: bottomController)
        bottomSheet.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height * 2/3)
        bottomController.delegate1 = self
        present(bottomSheet, animated: true, completion: nil)
    }
    func backToImage() {
    }
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var displayNameTF: UITextField!
    @IBOutlet weak var positionTF: UITextField!
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var bgCoverImg: UIImageView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var descriptionTF: UITextField!

    var picker:UIImagePickerController?=UIImagePickerController()
    var selectedIndex = Int()
    var imageData = Data()
    var isClicked : Bool = false
    var isGenderClicked : Bool = false
    var isRaceClicked : Bool = false
    var isEducationClicked : Bool = false
    
    var selectedRace = String()
    var imgFileName:URL!
    var imgFileName1:URL!

    var imageData1 = Data()
    var isClickedProfile : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        //Camera
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                print("response",response)
            } else {
                
            }
        }
        
        //Photos
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    
                }
            })
        }else if photos == .authorized{
            print("authorized")
        }else if photos == .denied{
            print("authorized")
        }else if photos == .restricted{
            print("restricted")
        }
        self.updateUI()
        // Do any additional setup after loading the view.
    }
    func updateUI(){
        self.userIcon.layer.borderWidth = 1
        self.userIcon.layer.masksToBounds = false
        self.userIcon.layer.borderColor = UIColor.clear.cgColor
        self.userIcon.layer.cornerRadius = self.userIcon.frame.height/2
        self.userIcon.clipsToBounds = true
        self.userIcon.layer.backgroundColor = UIColor.lightGray.cgColor
        self.displayNameTF.text = profileDetails.oResults.oBasicInfo1.displayName
        self.firstNameTF.text = profileDetails.oResults.oBasicInfo1.first_name
        self.lastNameTF.text = profileDetails.oResults.oBasicInfo1.last_name
        self.positionTF.text = profileDetails.oResults.oBasicInfo1.position
        self.emailTF.text = profileDetails.oResults.oBasicInfo1.email
        //self.positionTF.text = profileDetails.oResults.oBasicInfo1.

        if !(profileDetails.oResults.oBasicInfo1.avatar.isEmpty){
            
            if URL(string: (profileDetails.oResults.oBasicInfo1.avatar)) != nil{
                let imageURL = URL(string: (profileDetails.oResults.oBasicInfo1.avatar))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: self.userIcon)
            }
        }else{
            // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
        if !(profileDetails.oResults.oBasicInfo1.coverImg.isEmpty){
            
            if URL(string: (profileDetails.oResults.oBasicInfo1.coverImg)) != nil{
                let imageURL = URL(string: (profileDetails.oResults.oBasicInfo1.coverImg))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: self.bgCoverImg)
            }
        }else{
            // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
    }

    @IBAction func changeCoverImg(_ sender: UIButton) {
        isClickedProfile = false
        let bottomController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraController") as! CameraController
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: bottomController)
        bottomSheet.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height * 2/3)
        bottomController.delegate1 = self
        present(bottomSheet, animated: true, completion: nil)
    }
    @IBAction func changeProfileIcon(_ sender: UIButton) {
        isClickedProfile = true
        let bottomController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraController") as! CameraController
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: bottomController)
        bottomSheet.preferredContentSize = CGSize(width: view.frame.width, height: view.frame.height * 2/3)
        bottomController.delegate1 = self
        present(bottomSheet, animated: true, completion: nil)
    }
    @IBAction func tapEditProfile(_ sender: UIButton) {
        updateProfile(param: ["first_name":firstNameTF.text!,
                              "last_name": lastNameTF.text!,//userId,
            "display_name": displayNameTF.text!,
            "email": emailTF.text!,
            "description": descriptionTF.text!,
            "position":positionTF.text!
            ])
    }
    func updateProfile(param: [String:Any]){
        LoadingIndicatorView.show("Updating...")
        service.updateProfile(data: imageData,data1: imageData1,param: param, completionHandler: {success, response in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            if response?.status == "success"{
                _ = self.navigationController?.popViewController(animated: true)
            }else{
                self.alert(message: response!.message)
            }
        })
    }
    func updateProfileImageParams(param: [String:Any]){
        LoadingIndicatorView.show("Updating...")
        service.updateProfileImage(data: imageData1,data1: imageData,param: param, completionHandler: {success, response in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            if response?.status == "success"{
                self.alert(message: response!.message)
                _ = self.navigationController?.popViewController(animated: true)
            }else{
                self.alert(message: response!.message)
            }
        })
    }
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    func photoTapped(index:Int){
        if index == 0{
            selectedIndex = index
            openGallary()
        }else{
            selectedIndex = index
            openCamera()
        }
    }
    
    func openGallary()
    {
        picker?.delegate = self
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker!.allowsEditing = false
            picker?.delegate = self
            picker!.sourceType = UIImagePickerController.SourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("inf0",info)
        var chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        
        imageData = chosenImage.jpegData(compressionQuality: 0.3)!
        if isClickedProfile == true{
            self.userIcon.image = chosenImage
            self.userIcon.layer.cornerRadius = self.userIcon.frame.height/2
            self.userIcon.layer.borderColor = UIColor.lightGray.cgColor
            self.userIcon.layer.borderWidth = 0.5
            self.userIcon.contentMode = .scaleToFill
            imageData1 = chosenImage.jpegData(compressionQuality: 0.3)!
            let userID1 = s_default.string(forKey: "user_id")
            LoadingIndicatorView.show("Uploading...")
            self.updateProfileImageParams(param: ["action":"upload-attachment",
                                                  "user_id": userID1,//userId,
                "upload_image": "1"
                ])
            if let data = chosenImage.pngData() {
                imgFileName = getDocumentsDirectory().appendingPathComponent("image.png")
            }
        }else{
            self.bgCoverImg.image = chosenImage
            let userID1 = s_default.string(forKey: "user_id")
            //self.bgCoverImg.layer.cornerRadius = self.bgCoverImg.frame.height/2
            //self.bgCoverImg.layer.borderColor = UIColor.lightGray.cgColor
            //self.bgCoverImg.layer.borderWidth = 0.5
            self.bgCoverImg.contentMode = .scaleToFill
            imageData1 = chosenImage.jpegData(compressionQuality: 0.3)!
            LoadingIndicatorView.show("Uploading...")
            self.updateProfileImageParams(param: ["action":"upload-attachment",
                                                  "user_id": userID1,//userId,
                "upload_image": "2"
                ])
            if let data = chosenImage.pngData() {
                imgFileName1 = getDocumentsDirectory().appendingPathComponent("image.png")
            }
        }
        
        
        print("chosenImage",chosenImage)
        
        isClick = true
        
        dismiss(animated: true, completion:  nil)
    }
    
    func imgUploading(data: Data){
        
        
    }

}
