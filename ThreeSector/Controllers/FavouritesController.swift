//
//  FavouritesController.swift
//  ThreeSector
//
//  Created by BestWeb on 12/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
class FavouritesController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var viewExpired: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewExpired.isHidden = true
        self.tblData.isHidden = false
        self.getFavouritesDataAPI()
        // Do any additional setup after loading the view.
    }
    func getFavouritesDataAPI(){
        LoadingIndicatorView.show("Loading...")
        service.getFavouritesAPI(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                self.tblData.delegate = self
                self.tblData.dataSource = self
                self.tblData.reloadData()
                if success{
                    if userInfo?.status == "success"{
                        favouriteDetails = userInfo!
                        self.viewExpired.isHidden = true
                        self.tblData.isHidden = false
                        self.tblData.delegate = self
                        self.tblData.dataSource = self
                        self.tblData.reloadData()
                        
                    }else if userInfo?.msg == "tokenExpired"{
                        self.viewExpired.isHidden = false
                        self.alert(message: "Session Expired. Please Login again.")
                        self.tblData.isHidden = true
                    }else{
                        
                        self.view.makeToast( userInfo!.status, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.status, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    @IBAction func tapLogIn(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        self.performSegue(withIdentifier: "Logout", sender: self)

    }
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouriteDetails.oResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! favouritesCell
        cell.title.text = favouriteDetails.oResults[indexPath.row].postTitle
        cell.lblDesc.text = favouriteDetails.oResults[indexPath.row].tagLine
        cell.icon.layer.borderWidth = 1
        cell.icon.layer.masksToBounds = false
        cell.icon.layer.borderColor = UIColor.clear.cgColor
        cell.icon.layer.cornerRadius = cell.icon.frame.height/2
        cell.icon.clipsToBounds = true
        cell.icon.layer.backgroundColor = UIColor.clear.cgColor
        cell.favBtn.tag = indexPath.row

        cell.favBtn.addTarget(self, action: #selector(self.removeFavouriteBtn(_:)), for: .touchUpInside)
        if !(favouriteDetails.oResults[indexPath.row].oFeaturedImg.medium.isEmpty){
            
            if URL(string: (favouriteDetails.oResults[indexPath.row].oFeaturedImg.medium)) != nil{
                let imageURL = URL(string: (favouriteDetails.oResults[indexPath.row].oFeaturedImg.medium))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.icon)
            }
        }else{
            // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
        cell.viewBG.layer.shadowColor = UIColor.gray.cgColor
        cell.viewBG.layer.shadowOpacity = 0.3
        cell.viewBG.layer.shadowOffset = CGSize.zero
        cell.viewBG.layer.shadowRadius = 6
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 176
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //performSegue(withIdentifier: "CategoryToDetails", sender: self)
    }
    @objc func removeFavouriteBtn(_ sender: UIButton){
        if sender.isSelected{
           
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : favouriteDetails.oResults[sender.tag].ID])
            })
            
        }else{
           
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : favouriteDetails.oResults[sender.tag].ID])
            })
        }
    }
    //MARK:- API CALL ADD/REMOVE FAVUORITES
    func favouriesAdd(param: [String:Any]){
        LoadingIndicatorView.show("Loading...")
        service.favouritesRemoveAPI(param: param, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        if userInfo?.isAdded == "removed"{
                            self.view.makeToast( "Remove as Favourite", duration: 2.0, position: .bottom)
                        }else{
                            self.view.makeToast( "Remove as Favourite", duration: 2.0, position: .bottom)
                        }
                        self.getFavouritesDataAPI()

                    }else if userInfo?.msg == "tokenExpired"{
                        self.tblData.reloadData()
                        self.viewExpired.isHidden = false
                    }else{
                        self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.msg, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
}
class favouritesCell : UITableViewCell{
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var viewBG : UIView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var favBtn : UIButton!
}
