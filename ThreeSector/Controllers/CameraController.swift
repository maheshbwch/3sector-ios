//
//  CameraController.swift
//  ThreeSector
//
//  Created by BestWeb on 20/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit

protocol CameraDelegate {
    func backToImage()
    var isClick : Bool { get set };
}
protocol PhotoDelegate {
    func photoTapped(index:Int)
}
class CameraController: UIViewController {
    var delegate:CameraDelegate?
    var delegate1:PhotoDelegate?
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var galleryLabel: UILabel!
    @IBOutlet weak var outerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        outerView.layer.cornerRadius = 6
        outerView.addShadow()
        //self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapGallery(_ sender: UIButton) {
        dismiss(animated: false, completion: {  DispatchQueue.main.async{
            self.delegate1?.photoTapped(index: 0)
            } })
        delegate?.isClick = true
        delegate?.backToImage()
        self.view.removeFromSuperview()
        print("galleryTapped")
        //  openGallary()
        //delegate?.photoTapped(index: 0)
        
        
    }
    
    @IBAction func tapCamera(_ sender: UIButton) {
        delegate?.backToImage()
        self.view.removeFromSuperview()
        print("camera")
        //openCamera()
        dismiss(animated: false, completion: {  DispatchQueue.main.async{
            self.delegate1?.photoTapped(index: 1)
            } })
    }
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


