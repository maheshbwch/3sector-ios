//
//  QuestionsController.swift
//  ThreeSector
//
//  Created by BestWeb on 26/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import WebKit
class QuestionsController: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let myBlog = "http://demo.bestweb.com.my/threesector/questions/"
        let encodedStr = myBlog.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = NSURL(string: encodedStr!)
        let request = NSURLRequest(url: url! as URL)
        
        webView.navigationDelegate = self
        webView.load(request as URLRequest)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    //MARK:- WKNavigationDelegate
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        LoadingIndicatorView.show("Loading...")
        
        print("Strat to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        LoadingIndicatorView.hide()
        print("finish to load")
    }
}
