//
//  LoginViewController.swift
//  ThreeSector
//
//  Created by BestWeb on 22/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Toast_Swift
import FBSDKLoginKit
import FBSDKCoreKit
class LoginViewController: UIViewController {
    
    
    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Properties
    
    @IBOutlet weak var rectangle285Button: CornerButton!
    @IBOutlet weak var path746Button: CornerButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Lifecycle
    @IBOutlet weak var btnLogIn: UIButton!
    
    override public func viewDidLoad()  {
        super.viewDidLoad()
        self.setupComponents()
        self.setupUI()
        self.setupGestureRecognizers()
        self.setupLocalization()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height:subView.frame.origin.y + subView.frame.height + 40)
        //print(scrollView.contentSize)
        scrollView.layoutIfNeeded()
        
    }
    override public func viewWillAppear(_ animated: Bool)  {
        super.viewWillAppear(animated)
        
        // Navigation bar, if any
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    @IBAction func dofacebook(_ sender: UIButton) {

//        let userDefult = UserDefaults.standard
//        userDefult.setValue("", forKey: "Google access_tocken")
//        userDefult.synchronize()
//        let fbLoginManager : LoginManager = LoginManager()
//
//        fbLoginManager.logIn(permissions: ["public_profile","email"], from: self) { (result, error) in
//
//            if (error == nil){
//                let fbloginresult : LoginManagerLoginResult = result!
//                if fbloginresult.grantedPermissions != nil {
//                    if(fbloginresult.grantedPermissions.contains("email")) {
//                        if((AccessToken.current) != nil){
//                            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
//                                if (error == nil){
//                                    let dict: NSDictionary = result as! NSDictionary
//                                    if let token = AccessToken.current?.tokenString {
//                                        print("tocken: \(token)")
//                                        self.performSegue(withIdentifier: "LogIn", sender: self)
//
//                                       // self.tocken = token
//                                        // let userDefult = UserDefaults.standard
//                                        //  userDefult.setValue(token, forKey: "access_tocken")
//                                        // userDefult.synchronize()
//                                    }
//                                    if let user : NSString = dict.object(forKey:"name") as! NSString? {
//                                        print("user: \(user)")
//
//                                    }
//                                    if let id : NSString = dict.object(forKey:"id") as? NSString {
//                                        print("id: \(id)")
//                                    }
//                                    if let email : NSString = (result! as AnyObject).value(forKey: "email") as? NSString {
//                                        print("email: \(email)")
//                                        //self.emailTF.text = email as String
//                                        //self.passwordTF.text = "NoPassword"
////                                        self.userName = email as String
////                                        self.password = "NoPassword"
////                                        self.authLogIn = "facebook"
////                                        TheGlobalPoolManager.showProgressHud("Loading..")
////
////                                        self.loginAPI()
//                                    }
//                                }
//                            })
//                        }
//                    }
//                }
//            }
//        }
        //        TheGlobalPoolManager.hideProgressHud()
    }
    
    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Setup
    
    private func setupComponents()  {
        // Setup path747Button
        
        // Setup rectangle285Button
       
        
        // Setup path746Button
        
    }
    
    private func setupUI()  {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    private func setupGestureRecognizers()  {
        
    }
    
    private func setupLocalization()  {
        
    }
    
    
    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Status Bar
    
    override public var prefersStatusBarHidden: Bool  {
        return true
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle  {
        return .default
    }
    
    
    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Actions
    
    @IBAction func tapLogIn(_ sender: UIButton) {
        if(validatLogin()){
            
            DispatchQueue.main.async {
                
                let emailtxt = self.emailTF.text!
                let passwordtxt = self.passwordTF.text!
                
              
                self.createCustomerLogin(param: ["username" : emailtxt,"password" : passwordtxt])
            }
            
        }
        
    }
    
    
    func createCustomerLogin(param: [String:Any]){
        
        LoadingIndicatorView.show("Checking SignIn...")
        
        service.UserLogin(param: param, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "loggedIn"{
                        print((userInfo?.data)!)
                        loginData = userInfo!
                        loginUserID = (userInfo?.data.userId)!
                        token = userInfo!.token
                        s_default.set(userInfo?.data.role, forKey: "Role")

                        s_default.set(token, forKey: "token")
                        s_default.set(true, forKey: "isLoggedIn")
                        s_default.set(loginUserID, forKey: "user_id")
                        s_default.set(self.emailTF.text!, forKey: "UserName")
                        s_default.set(self.passwordTF.text!, forKey: "Password")
                        s_default.set(userInfo?.data.avatar, forKey: "Image")
                        s_default.set(userInfo?.data.userName, forKey: "GetUserName")
                        let userID = s_default.string(forKey: "user_id")
                        let FCM_TOK = s_default.string(forKey: "FCMToken")

                        let userImage = s_default.string(forKey: "Image")
                        let timeStamp = Date.currentTimeStamp
                        print(timeStamp)
                        let timeStampStr = String(timeStamp)
                        
                        let ref = Constants.refs.databaseRoot.child("Notifications").child(userID!)
                        
                        let userData = ["deviceToken" : FCM_TOK!] as [String : Any]
                        
                        
                        ref.setValue(userData)
                        self.performSegue(withIdentifier: "LogIn", sender: self)
                        
                        
                    }else{
                        
                        self.view.makeToast( userInfo!.message, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.message, duration: 2.0, position: .bottom)
                }
            }
            
        })
    }
    //MARK: -  Validation
    func validatLogin() -> Bool {
        
        if (emailTF.text?.count == 0 ) {
            
            alert(message: "", title: "Please enter your Email")
            return false
            
        }else if (passwordTF.text?.count == 0 ) {
            
            alert(message: "", title: "Please Enter your password")
            return false
            
        }else if ( (passwordTF.text?.count)! < 2 ) { //INVALID PASSWORD LENGTH SHOULD BE ATLEAST 6 CHARACTERS
            alert(message: "", title: "Password should be minimum 6 characters")
            return false
            
        }
        
        return true
    }
    //MARK: -  UITextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == emailTF {
            passwordTF.becomeFirstResponder()
            
        } else if textField == passwordTF {
            
            tapLogIn(self.btnLogIn)
        }
        textField.resignFirstResponder()
        return true;
    }

    @IBAction public func onPath747Pressed(_ sender: UIButton)  {
        
    }
    
    @IBAction public func onRectangle285Pressed(_ sender: UIButton)  {
    }
    
    @IBAction public func tapSignUp(_ sender: UIButton)  {
        self.performSegue(withIdentifier: "Dashboard", sender: nil)

    }
}
