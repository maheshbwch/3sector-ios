//
//  AllReviewsController.swift
//  ThreeSector
//
//  Created by BestWeb on 07/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
class AllReviewsController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblReviews: UITableView!
    var id:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getReviewsAPI()
        // Do any additional setup after loading the view.
    }
    
    func getReviewsAPI(){
        LoadingIndicatorView.show("Loading...")
        service.getReviews(Id:id,completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
               
                if success{
                        getReviewsData = userInfo!
                        self.tblReviews.delegate = self
                        self.tblReviews.dataSource = self
                        self.tblReviews.reloadData()
                  
                }else{
                    
                    self.view.makeToast("No response", duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getReviewsData.aReviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! reviewsCell
        cell.overallQuality.rating = Double(Float(getReviewsData.aReviews[indexPath.row].oReviews.oDetails[0].score))
        cell.productQualityRating.rating = Double(Float(getReviewsData.aReviews[indexPath.row].oReviews.oDetails[0].score))
        cell.overAllRating.text = String(format:"%i",getReviewsData.aReviews[indexPath.row].oReviews.oDetails[0].score)
        cell.productQuality.text = String(format:"%i",getReviewsData.aReviews[indexPath.row].oReviews.oDetails[0].score)
        cell.userName.text = getReviewsData.aReviews[indexPath.row].oUserInfo.displayName
        cell.time.text =  getReviewsData.aReviews[indexPath.row].postDate
        cell.lblRatingDesc.text = getReviewsData.aReviews[indexPath.row].postContent
        if !(getReviewsData.aReviews[indexPath.row].oUserInfo.avatar.isEmpty){
            
            if URL(string: (getReviewsData.aReviews[indexPath.row].oUserInfo.avatar)) != nil{
                let imageURL = URL(string: (getReviewsData.aReviews[indexPath.row].oUserInfo.avatar))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.imgReviewPerson)
            }
        }else{
            // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }

        cell.viewBG.layer.shadowColor = UIColor.gray.cgColor
        cell.viewBG.layer.shadowOpacity = 0.3
        cell.viewBG.layer.shadowOffset = CGSize.zero
        cell.viewBG.layer.shadowRadius = 6
        //  cell.heightAnchor.constraint(equalToConstant: 286)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 273
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class reviewsCell : UITableViewCell{
    @IBOutlet weak var imgReviewPerson : UIImageView!
    @IBOutlet weak var userName : UILabel!
    @IBOutlet weak var time : UILabel!
    @IBOutlet weak var productQualityRatingImg : UIImageView!
    @IBOutlet weak var overAllRatingImg : UIImageView!
    @IBOutlet weak var productQuality : UILabel!
    @IBOutlet weak var overAllRating : UILabel!
    @IBOutlet weak var lblRatingDesc : UILabel!
    @IBOutlet weak var viewBG : UIView!
    @IBOutlet weak var productQualityRating:FloatRatingView!
    @IBOutlet weak var overallQuality:FloatRatingView!
}
extension Date {
    
    func getElapsedInterval() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" :
                "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" :
                "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" :
                "\(day)" + " " + "days ago"
        } else {
            return "a moment ago"
            
        }
        
    }
}
