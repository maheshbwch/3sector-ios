//
//  ImagePopUpController.swift
//  ThreeSector
//
//  Created by BestWeb on 02/03/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
protocol ImagePreviewDelegate {
    func backToImage()
    //var imageURL : UIImageView { get set };
}

class ImagePopUpController: UIViewController {
    var delegate:ImagePreviewDelegate?

    @IBOutlet weak var imagePreview: UIImageView!
    var imageURL : UIImageView!
    var imgFileNameData:URL!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let data = try? Data(contentsOf: imgFileNameData!)
        {
            let image: UIImage = UIImage(data: data)!
            imagePreview.image = image
        }
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func tapDone(_ sender: UIButton) {
    }
    @IBAction func tapCancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
