//
//  ChatHeadsController.swift
//  ThreeSector
//
//  Created by BestWeb on 24/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
class ChatHeadsController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var btnNewChat: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var searchPlaceHolder: UILabel!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var viewNoChat: UIView!
    @IBOutlet weak var viewExpired: UIView!
    var myArray = [[String: String]]()
    fileprivate var ordersHeaders = [ChatHeadersDataModel]()
    var orders: [ChatHeadersDataModel]!
    var tagValues : String = ""
    var indexPathValue : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        let userID1 = s_default.string(forKey: "user_id")
        //LoadingIndicatorView.show("loading..")
        let query = Constants.refs.databaseRoot.child("Friends").child(userID1!)
//        "date" : "1582750257642",
//        "message_key" : "19_1",
//        "receiver_id" : "19",
//        "receiver_image" : "http://bestweb.my/3sectornew/wp-content/uploads/2018/08/avatar-default.png",
//        "receiver_name" : "pothiraj123",
//        "sender_id" : "1",
//        "sender_image" : "https://bestweb.my/3sectornew/wp-content/uploads/2018/08/about_002-150x150.jpg",
//        "sender_name" : "root"
        _ = query.observe(.childAdded, with: { [weak self] snapshot in
            
            if  let data        = snapshot.value as? [String: AnyObject],
                let messageDate          = data["date"],
                let messageKey           = data["message_key"],
                let receiverId             = data["receiver_id"],
                let receiverImage                 = data["receiver_image"],
                let receiverName          = data["receiver_name"],
                !(receiverName as! String).isEmpty
            {
                let messageDate1:String = String(format: "%@", messageDate as! CVarArg)

                let order = ChatHeadersDataModel.init(messageKey: messageKey as? String, receiverId: receiverId as? String, receiverImage: receiverImage as? String, receiverName: receiverName as? String,receiveDate: messageDate as! String)
                if self!.orders == nil
                {
                    self!.orders = [ChatHeadersDataModel]()
                }
                
                self!.orders.append(order)
                self!.ordersHeaders.append(order)
                LoadingIndicatorView.hide()
                let dict = ["date": messageDate, "message_key": messageKey,"receiverId": receiverId, "receiverImage": receiverImage,"receiverName": receiverName]
//                if let message = JSQMessage(senderId: receiverId as? String, displayName:receiverName, text: "" as? String)
//                {
                if (self?.orders.count)! > 0{
                    self!.viewNoChat.isHidden = true
                    self?.tblChat.isHidden = false
                    self?.tblChat.delegate = self
                    self?.tblChat.dataSource = self
                    self?.tblChat.reloadData()
                }else{
                    self?.viewNoChat.isHidden = false
                    self?.tblChat.isHidden = true
                }
                print("\(self?.orders)")

               // }
            }
        })
         LoadingIndicatorView.hide()
       // self.getChatHeadersData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapNewChat(_ sender: UIButton) {
    }
    @IBAction func tapLogIn(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        self.performSegue(withIdentifier: "LogIn", sender: self)
        
    }
    //UITableview Delegate and Datasource
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.orders.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! chatCell
        cell.title.text = self.orders[indexPath.row].receiverName

        
        cell.timeLbl.text = self.convertTimestamp(serverTimestamp: Double(self.orders[indexPath.row].receiveDate!) as! Double)
        if !(self.orders[indexPath.row].receiverImage!.isEmpty){
            
            if URL(string: (self.orders[indexPath.row].receiverImage!)) != nil{
                let imageURL = URL(string: (self.orders[indexPath.row].receiverImage!))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.userIcon)
            }
        }else{
            //cell.userIcon.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
       // cell.userIcon?.image = UIImage.init(named: self.orders[indexPath.row].receiverImage!)
        cell.userIcon.layer.borderWidth = 1
        cell.userIcon.layer.masksToBounds = false
        cell.userIcon.layer.borderColor = UIColor.clear.cgColor
        cell.userIcon.layer.cornerRadius = cell.userIcon.frame.height/2
        cell.userIcon.clipsToBounds = true
        
//        if let time = getChatHeaders.oResults[indexPath.row].oMessage.content as? String{
//            cell.titleDescription.text = time
//        }else{
            //cell.titleDescription.text = ""
       // }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tagValues = self.orders[indexPath.row].receiverId!
        indexPathValue = indexPath.row
        self.performSegue(withIdentifier: "ChatList", sender: self)
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return UITableView.automaticDimension
//    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ChatList"{
            let vc = segue.destination as! ChatListController
            vc.receiverID = tagValues
            vc.receiverName = self.orders[indexPathValue].receiverName!
            vc.receiverImage = self.orders[indexPathValue].receiverImage!

        }
    }
    func getChatHeadersData(){
        LoadingIndicatorView.show("Loading...")
        service.getChatHeaders(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            //print("Response",userInfo!)
            
            if success{
                getChatHeaders = userInfo!
                if getChatHeaders.status == "success"{
                    self.tblChat.isHidden = false
                    self.tblChat.delegate = self
                    self.tblChat.dataSource = self
                    self.tblChat.reloadData()
                    self.viewExpired.isHidden = true
                    self.viewNoChat.isHidden = true
                }else if userInfo?.msg == "token Expire"{
                    self.viewExpired.isHidden = false
                    self.viewNoChat.isHidden = false
                    self.tblChat.isHidden = true
                    self.tblChat.delegate = self
                    self.tblChat.dataSource = self
                    self.tblChat.reloadData()
                }else{
                    self.viewNoChat.isHidden = false
                    self.tblChat.isHidden = true
                    self.tblChat.delegate = self
                    self.tblChat.dataSource = self
                    self.tblChat.reloadData()
                    self.alert(message: userInfo!.status)
                }
            }else{
                self.viewNoChat.isHidden = true
                self.tblChat.isHidden = false
                self.tblChat.delegate = self
                self.tblChat.dataSource = self
                self.tblChat.reloadData()
//                self.alert(message: "Bad response")
                self.alert(message: "success")

                
            }
        })
    }
    func convertTimestamp(serverTimestamp: Double) -> String {
        let x = serverTimestamp / 1000
        let date = NSDate(timeIntervalSince1970: x)
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        
        return formatter.string(from: date as Date)
    }
    func relativeDate(for date:Date) -> String {
        let components = Calendar.current.dateComponents([.day, .year, .month, .weekOfYear], from: date, to: Date())
        if let year = components.year, year == 1{
            return "\(year) year ago"
        }
        if let year = components.year, year > 1{
            return "\(year) years ago"
        }
        if let month = components.month, month == 1{
            return "\(month) month ago"
        }
        if let month = components.month, month > 1{
            return "\(month) months ago"
        }
        
        if let week = components.weekOfYear, week == 1{
            return "\(week) week ago"
        }
        if let week = components.weekOfYear, week > 1{
            return "\(week) weeks ago"
        }
        
        if let day = components.day{
            if day > 1{
                return "\(day) days ago"
            }else{
                "Yesterday"
            }
        }
        return "Today"
    }
}
class chatCell : UITableViewCell{
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var titleDescription: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
}
