//
//  ChangePasswordController.swift
//  ThreeSector
//
//  Created by BestWeb on 06/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit

class ChangePasswordController: UIViewController {

    @IBOutlet weak var currentPasswordTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmNewPassworsTF: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func tapSave(_ sender: UIButton) {
        if(validation()){
            
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.changePasswordAPI(param: ["new_password" : self.newPasswordTF.text!])
            })
        }
       

    }
    func changePasswordAPI(param: [String:Any]){
        LoadingIndicatorView.show("Loading...")
        service.changesPasswordAPI(param: param, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                     
                        self.view.makeToast( "Password updated successfully", duration: 2.0, position: .bottom)
                        self.navigationController?.popViewController(animated: true)

                    }else{
                        self.view.makeToast( userInfo!.status, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.status, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    //MARK: -  Validation
    func validation() -> Bool {
        if (newPasswordTF.text?.count == 0 ) {
            
            alert(message: "", title: "Please Enter your password")
            return false
            
        }else if ( (newPasswordTF.text?.count)! < 6 ) {
            alert(message: "", title: "Password should be minimum 6 characters")
            return false
            
        }else if (confirmNewPassworsTF.text?.count == 0 ) {
            
            alert(message: "", title: "Please Enter your confirm password")
            return false
            
        }else if ( (confirmNewPassworsTF.text?.count)! < 6 ) {
            alert(message: "", title: "Confirm Password should be minimum 6 characters")
            return false
            
        }else if (newPasswordTF.text == confirmNewPassworsTF.text){
            return true

        }
        alert(message: "", title: "Password and confirm password not matching")
        return false
    }
}
