//
//  SignUpMerchantController.swift
//  ThreeSector
//
//  Created by BestWeb on 23/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit

class SignUpMerchantController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var companyNameTF: UITextField!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var viewCollectionCells: UIView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionCategories: UICollectionView!
    @IBOutlet weak var businessCategories: UITextField!
    @IBOutlet weak var viewPackage1: UIView!
    @IBOutlet weak var btnPackage1: UIButton!
    @IBOutlet weak var selectPackage1: UIButton!
    @IBOutlet weak var btnPackage2: UIButton!
    @IBOutlet weak var selectPackage2: UIButton!
    @IBOutlet weak var btnPackage3: UIButton!
    @IBOutlet weak var selectPackage3: UIButton!
    @IBOutlet weak var btnPackage4: UIButton!
    @IBOutlet weak var selectPackage4: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionHeight.constant = 0
        viewCollectionCells.isHidden = true
        selectPackage1.setImage(UIImage.init(named: "RadioSelected"), for: .normal)
        selectPackage2.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage3.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage4.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        btnPackage1.setImage(UIImage.init(named: "Package1Selected"), for: .normal)
        btnPackage4.setImage(UIImage.init(named: "Package4UnSelected"), for: .normal)
        btnPackage3.setImage(UIImage.init(named: "Package3UnSelected"), for: .normal)
        btnPackage2.setImage(UIImage.init(named: "Package2UnSelected-1"), for: .normal)
        btnNext.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        btnNext.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)

        // Do any additional setup after loading the view.
    }
//    override func viewWillLayoutSubviews(){
//        super.viewWillLayoutSubviews()
//        
//        scrollView.contentSize = CGSize(width: scrollView.frame.width, height:subView.frame.origin.y + subView.frame.height + 40)
//        print(scrollView.contentSize)
//        scrollView.layoutIfNeeded()
//        
//    }

    @IBAction func tapDropDownBusinessCategories(_ sender: UIButton) {
    }
    
    @IBAction func tapSelectRadio1(_ sender: UIButton) {
        selectPackage1.setImage(UIImage.init(named: "RadioSelected"), for: .normal)
        selectPackage2.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage3.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage4.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        btnPackage1.setImage(UIImage.init(named: "Package1Selected"), for: .normal)
        btnPackage4.setImage(UIImage.init(named: "Package4UnSelected"), for: .normal)
        btnPackage3.setImage(UIImage.init(named: "Package3UnSelected"), for: .normal)
        btnPackage2.setImage(UIImage.init(named: "Package2UnSelected-1"), for: .normal)
    }
    @IBAction func tapSelectPackage1(_ sender: UIButton) {
        selectPackage1.setImage(UIImage.init(named: "RadioSelected"), for: .normal)
        selectPackage2.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage3.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage4.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        btnPackage1.setImage(UIImage.init(named: "Package1Selected"), for: .normal)
        btnPackage4.setImage(UIImage.init(named: "Package4UnSelected"), for: .normal)
        btnPackage3.setImage(UIImage.init(named: "Package3UnSelected"), for: .normal)
        btnPackage2.setImage(UIImage.init(named: "Package2UnSelected-1"), for: .normal)
    }
    @IBAction func tapSelectRadio2(_ sender: UIButton) {
        selectPackage1.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage2.setImage(UIImage.init(named: "RadioSelected"), for: .normal)
        selectPackage3.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage4.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        btnPackage1.setImage(UIImage.init(named: "Package1UnSelected"), for: .normal)
        btnPackage4.setImage(UIImage.init(named: "Package4UnSelected"), for: .normal)
        btnPackage3.setImage(UIImage.init(named: "Package3UnSelected"), for: .normal)
        btnPackage2.setImage(UIImage.init(named: "Package2Selected"), for: .normal)
    }
    @IBAction func tapSelectPackage2(_ sender: UIButton) {
        selectPackage1.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage2.setImage(UIImage.init(named: "RadioSelected"), for: .normal)
        selectPackage3.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage4.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        btnPackage1.setImage(UIImage.init(named: "Package1UnSelected"), for: .normal)
        btnPackage4.setImage(UIImage.init(named: "Package4UnSelected"), for: .normal)
        btnPackage3.setImage(UIImage.init(named: "Package3UnSelected"), for: .normal)
        btnPackage2.setImage(UIImage.init(named: "Package2Selected"), for: .normal)
    }
    @IBAction func tapSelectRadio3(_ sender: UIButton) {
        selectPackage3.setImage(UIImage.init(named: "RadioSelected"), for: .normal)
        selectPackage2.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage1.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage4.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        btnPackage1.setImage(UIImage.init(named: "Package1UnSelected"), for: .normal)
        btnPackage4.setImage(UIImage.init(named: "Package4UnSelected"), for: .normal)
        btnPackage3.setImage(UIImage.init(named: "Package3Selected"), for: .normal)
        btnPackage2.setImage(UIImage.init(named: "Package2UnSelected-1"), for: .normal)
    }
    @IBAction func tapSelectPackage3(_ sender: UIButton) {
        selectPackage3.setImage(UIImage.init(named: "RadioSelected"), for: .normal)
        selectPackage2.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage1.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage4.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        btnPackage1.setImage(UIImage.init(named: "Package1UnSelected"), for: .normal)
        btnPackage4.setImage(UIImage.init(named: "Package4UnSelected"), for: .normal)
        btnPackage3.setImage(UIImage.init(named: "Package3Selected"), for: .normal)
        btnPackage2.setImage(UIImage.init(named: "Package2UnSelected-1"), for: .normal)
    }
    @IBAction func tapSelectRadio4(_ sender: UIButton) {
        btnPackage1.setImage(UIImage.init(named: "Package1UnSelected"), for: .normal)
        btnPackage4.setImage(UIImage.init(named: "Package4Selected"), for: .normal)
        btnPackage3.setImage(UIImage.init(named: "Package3UnSelected"), for: .normal)
        btnPackage2.setImage(UIImage.init(named: "Package2UnSelected-1"), for: .normal)
        selectPackage4.setImage(UIImage.init(named: "RadioSelected"), for: .normal)
        selectPackage2.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage1.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage3.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
    }
    @IBAction func tapSelectPackage4(_ sender: UIButton) {
        btnPackage1.setImage(UIImage.init(named: "Package1UnSelected"), for: .normal)
        btnPackage4.setImage(UIImage.init(named: "Package4Selected"), for: .normal)
        btnPackage3.setImage(UIImage.init(named: "Package3UnSelected"), for: .normal)
        btnPackage2.setImage(UIImage.init(named: "Package2UnSelected-1"), for: .normal)
        selectPackage4.setImage(UIImage.init(named: "RadioSelected"), for: .normal)
        selectPackage2.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage1.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
        selectPackage3.setImage(UIImage.init(named: "RadioUncheck"), for: .normal)
    }
    @IBAction func tapGoBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func tapNext(_ sender: UIButton) {
        performSegue(withIdentifier: "AddCompany1", sender: self)
    }
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
