//
//  ListingController.swift
//  ThreeSector
//
//  Created by BestWeb on 12/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
class ListingController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblData: UITableView!
    var categoryId : Int = 0
    var tagValue : Int = 0
    @IBOutlet weak var titleHeader: UILabel!
    var pageCount : Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        titleHeader.text = "My Listings"
        self.getList()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listingData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! listingsCell
        cell.shadowView.layer.shadowColor = UIColor.gray.cgColor
        cell.shadowView.layer.shadowOpacity = 0.3
        cell.shadowView.layer.shadowOffset = CGSize.zero
        cell.shadowView.layer.shadowRadius = 6
       cell.title.text = listingData.data[indexPath.row].postTitle
        cell.imageCategory.layer.borderWidth = 1
        cell.imageCategory.layer.masksToBounds = false
        cell.imageCategory.layer.borderColor = UIColor.clear.cgColor
        cell.imageCategory.layer.cornerRadius = cell.imageCategory.frame.height/2
        cell.imageCategory.clipsToBounds = true
        cell.btnfav.addTarget(self, action: #selector(self.favouriteBtn(_:)), for: .touchUpInside)
        cell.btnfav.tag = indexPath.row
        
        if listingData.data[indexPath.row].oFavorite.isMyFavorite == true{
            cell.btnfav.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            cell.btnfav.isSelected = true
        }else{
            cell.btnfav.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            cell.btnfav.isSelected = false
            
        }
        if !(listingData.data[indexPath.row].logo.isEmpty){

            if URL(string: (listingData.data[indexPath.row].logo)) != nil{
                let imageURL = URL(string: (listingData.data[indexPath.row].logo))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.imageCategory)
            }
        }else{
            cell.imageCategory.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
        cell.lblReviews.text = String(format:"%i/5 Reviews",listingData.data[indexPath.row].oReview.mode)

        if !( listingData.data[indexPath.row].oAddress.address.isEmpty){
            cell.lblAddress.text = listingData.data[indexPath.row].oAddress.address
        }else{
            cell.lblAddress.text = listingData.data[indexPath.row].tagLine
        }
        if !( listingData.data[indexPath.row].oAddress.address.isEmpty){
            cell.lblLocation.text = listingData.data[indexPath.row].oAddress.address
        }else{
            cell.lblLocation.text = "NA"
        }
//        if listingData.next == false{
//
//        }else{
//            pageCount = pageCount+1
//            self.getList()
//        }
//        if indexPath.row == listingData.data.count-3{
//            if listingData.next == false{
//
//            }else{
//            pageCount = pageCount+1
//            self.getList()
//            }
//        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 273
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
       // performSegue(withIdentifier: "CategoryToDetails", sender: self)
    }
    @objc func favouriteBtn(_ sender: UIButton){
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            sender.isSelected = false
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : listingData.data[sender.tag].ID])
            })
            
        }else{
            sender.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            sender.isSelected = true
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : listingData.data[sender.tag].ID])
            })
        }
    }
    //MARK:- API CALL ADD/REMOVE FAVUORITES
    func favouriesAdd(param: [String:Any]){
        LoadingIndicatorView.show("Loading...")
        service.favouritesAddRemoveAPI(param: param, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        if userInfo?.isAdded == "removed"{
                            self.view.makeToast( "Removed as Favourite", duration: 2.0, position: .bottom)
                        }else{
                            self.view.makeToast( "Added as Favourite", duration: 2.0, position: .bottom)
                        }
                        
                    }else if userInfo?.msg == "tokenExpired"{
                        //                        self.tblData.reloadData()
                        //                        self.viewExpired.isHidden = false
                    }else{
                        self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }else{
                    // self.tblData.reloadData()
                    
                    self.view.makeToast(userInfo!.msg, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
   // MARK:- API CALL CATEGORIS By ID
    func getList(){
        LoadingIndicatorView.show("Loading...")
        service.getListings(page: pageCount, postType: "listing",  completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)

            if success{
                listingData = userInfo!
                if listingData.status == "success"{
                   // self.titleHeader.text = listingData.data[0].oTerm.name
                    self.tblData.delegate = self
                    self.tblData.dataSource = self
                    self.tblData.keyboardDismissMode = .onDrag
                    self.tblData.reloadData()
                }else{
                    self.alert(message: userInfo!.status)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "CategoryToDetails"{
//            let vc = segue.destination as! CategoryDetailsController
//            vc.listingId = categoriesById.data[tagValue].ID
//        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
class listingsCell : UITableViewCell{
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var ratingImage: UIImageView!
    
    @IBOutlet weak var registeredNo: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var lblcategory: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnfav: UIButton!
    
}
