//
//  ChatViewController.swift
//  ThreeSector
//
//  Created by BestWeb on 27/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import JSQMessagesViewController
class ChatViewController: JSQMessagesViewController {
    var messages = [JSQMessage]()
    var receiverID : String = ""
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }()
    
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addViewOnTop()
        senderId = "1"
        senderDisplayName = "Xavier"
        inputToolbar.contentView.leftBarButtonItem = nil
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 20, height: 20)
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: 20, height: 20)
        self.collectionView?.collectionViewLayout.sectionInset = UIEdgeInsets(top: 80, left: 0, bottom: 0, right: 0)
        let userID = s_default.integer(forKey: "user_id")
        let userID2 = Int(receiverID)!
        //LoadingIndicatorView.show("loading..")

        let large = max(userID, userID2)
        let mini = min(userID, userID2)
        let msgKey = String(format:"%i-%i",mini,large)
        let query = Constants.refs.databaseRoot.child("Messages").queryOrdered(byChild: "message_key")
            .queryEqual(toValue: msgKey)
        
        _ = query.observe(.childAdded, with: { [weak self] snapshot in
            
            if  let data        = snapshot.value as? [String: AnyObject],
                let messageDate          = data["message_date"],
                let messageKey           = data["message_key"],
                let senderId             = data["sender_id"],
                let type                 = data["type"],
                let messageText          = data["message_text"],
                !(messageText as! String).isEmpty
            {
                if let message = JSQMessage(senderId: senderId as! String, displayName: "", text: messageText as? String)
                {
                    self?.messages.append(message)
                    
                    self?.finishReceivingMessage()
                }
            }
        })
        LoadingIndicatorView.hide()

    }
    func addViewOnTop() {
        let selectableView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 84))
        selectableView.backgroundColor = #colorLiteral(red: 0.2221801281, green: 0.6598004103, blue: 0.9055172801, alpha: 1)
        let randomViewLabel = UILabel(frame: CGRect(x: 20, y: 10, width: 100, height: 16))
        randomViewLabel.text = "Chat"
        randomViewLabel.center = selectableView.center
        randomViewLabel.textColor = .white
        let button:UIButton = UIButton(frame: CGRect(x: 20, y: 50, width: 30, height: 30))
        button.backgroundColor = .clear
        button.setImage(UIImage.init(named: "Backbutton"), for: .normal)
        button.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        selectableView.addSubview(button)
        selectableView.addSubview(randomViewLabel)
        view.addSubview(selectableView)
        
    }
    @objc func buttonClicked() {
        self.navigationController?.popViewController(animated: true)
        
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!,
                                 attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
    {
        let message = messages[indexPath.row]
        let messageUsername = message.senderDisplayName
        
        return NSAttributedString(string: messageUsername!)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    {
        return messages[indexPath.item].senderId == receiverID ? 0 : 15
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return messages.count
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        let message = messages[indexPath.row]
        
        if receiverID == message.senderId {
            return bubbleFactory?.outgoingMessagesBubbleImage(with: .black)
        } else {
            return bubbleFactory?.incomingMessagesBubbleImage(with: .blue)
        }
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        return nil
    }
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!)
    {
        let userID = s_default.integer(forKey: "user_id")
        let userID1 = s_default.string(forKey: "user_id")

        let userID2 = Int(receiverID)!
        let userImage = s_default.string(forKey: "Image")
        let timeStamp = Date.currentTimeStamp
        print(timeStamp)
        let timeStampStr = String(timeStamp)

        let ref = Constants.refs.databaseRoot.child("Messages").childByAutoId()
        
        let large = max(userID, userID2)
        let mini = min(userID, userID2)
        let msgKey = String(format:"%i-%i",mini,large)

        let userData = ["message_date" : timeStampStr,
                        "message_key" : msgKey,
                        "message_text" : text,
                        "type" : "text",
                        "sender_id" : userID1] as [String : Any]
        
        
        ref.setValue(userData)
        
        finishSendingMessage()
    }
}

