//
//  ProjectsController.swift
//  ThreeSector
//
//  Created by BestWeb on 13/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
class ProjectsController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblData: UITableView!
    var categoryId : Int = 0
    var tagValue : Int = 0
    @IBOutlet weak var titleHeader: UILabel!
    var pageCount : Int = 1
    @IBOutlet weak var viewExpired: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleHeader.text = "PROJECTS"
        self.getList()
        viewExpired.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projectsData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! projectsCell
        cell.shadowView.layer.shadowColor = UIColor.gray.cgColor
        cell.shadowView.layer.shadowOpacity = 0.3
        cell.shadowView.layer.shadowOffset = CGSize.zero
        cell.shadowView.layer.shadowRadius = 6
        cell.title.text = projectsData.data[indexPath.row].postTitle
        cell.imageCategory.layer.borderWidth = 1
        cell.imageCategory.layer.masksToBounds = false
        cell.imageCategory.layer.borderColor = UIColor.clear.cgColor
        cell.imageCategory.layer.cornerRadius = cell.imageCategory.frame.height/2
        cell.imageCategory.clipsToBounds = true
        cell.btnfav.tag = indexPath.row
        
        if projectsData.data[indexPath.row].oFavorite.isMyFavorite == true{
            cell.btnfav.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            cell.btnfav.isSelected = true
        }else{
            cell.btnfav.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            cell.btnfav.isSelected = false
            
        }
        cell.btnfav.addTarget(self, action: #selector(self.favouriteBtn(_:)), for: .touchUpInside)
        if !(projectsData.data[indexPath.row].logo.isEmpty){
            
            if URL(string: (projectsData.data[indexPath.row].logo)) != nil{
                let imageURL = URL(string: (projectsData.data[indexPath.row].logo))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.imageCategory)
            }
        }else{
            cell.imageCategory.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
        cell.lblReviews.text = String(format:"%i/5 Reviews",projectsData.data[indexPath.row].oReview.mode)
        
        if !( projectsData.data[indexPath.row].oAddress.address.isEmpty){
            cell.lblAddress.text = projectsData.data[indexPath.row].oAddress.address
        }else{
            cell.lblAddress.text = projectsData.data[indexPath.row].tagLine
        }
        if !( projectsData.data[indexPath.row].oAddress.address.isEmpty){
            cell.lblLocation.text = projectsData.data[indexPath.row].oAddress.address
        }else{
            cell.lblLocation.text = "NA"
        }
        //        if listingData.next == false{
        //
        //        }else{
        //            pageCount = pageCount+1
        //            self.getList()
        //        }
        //        if indexPath.row == listingData.data.count-3{
        //            if listingData.next == false{
        //
        //            }else{
        //            pageCount = pageCount+1
        //            self.getList()
        //            }
        //        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 273
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        // performSegue(withIdentifier: "CategoryToDetails", sender: self)
    }
    // MARK:- API CALL CATEGORIS By ID
    func getList(){
        LoadingIndicatorView.show("Loading...")
        service.getListings(page: pageCount, postType: "project",  completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                projectsData = userInfo!
                if projectsData.status == "success"{
                    // self.titleHeader.text = listingData.data[0].oTerm.name
                    self.tblData.delegate = self
                    self.tblData.dataSource = self
                    self.tblData.keyboardDismissMode = .onDrag
                    self.tblData.reloadData()
                    self.viewExpired.isHidden = true
                }else if userInfo?.msg == "tokenExpired"{
                self.viewExpired.isHidden = false
                    self.tblData.isHidden = true
            }else{
                    self.alert(message: userInfo!.status)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //        if segue.identifier == "CategoryToDetails"{
        //            let vc = segue.destination as! CategoryDetailsController
        //            vc.listingId = categoriesById.data[tagValue].ID
        //        }
    }
    @objc func favouriteBtn(_ sender: UIButton){
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            sender.isSelected = false
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : projectsData.data[sender.tag].ID])
            })
            
        }else{
            sender.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            sender.isSelected = true
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : projectsData.data[sender.tag].ID])
            })
        }
    }
    //MARK:- API CALL ADD/REMOVE FAVUORITES
    func favouriesAdd(param: [String:Any]){
        LoadingIndicatorView.show("Loading...")
        service.favouritesAddRemoveAPI(param: param, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        if userInfo?.isAdded == "removed"{
                            self.view.makeToast( "Removed as Favourite", duration: 2.0, position: .bottom)
                        }else{
                            self.view.makeToast( "Added as Favourite", duration: 2.0, position: .bottom)
                        }
                        
                    }else if userInfo?.msg == "tokenExpired"{
                        self.tblData.reloadData()
                        self.viewExpired.isHidden = false
                    }else{
                        self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }else{
                    self.tblData.reloadData()
                    
                    self.view.makeToast(userInfo!.msg, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    @IBAction func tapLogIn(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        self.performSegue(withIdentifier: "Logout", sender: self)
        
    }
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
class projectsCell : UITableViewCell{
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var ratingImage: UIImageView!
    
    @IBOutlet weak var registeredNo: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var lblcategory: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnfav: UIButton!
    
}
