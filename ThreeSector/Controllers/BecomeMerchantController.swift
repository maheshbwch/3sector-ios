//
//  BecomeMerchantController.swift
//  ThreeSector
//
//  Created by BestWeb on 31/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit

class BecomeMerchantController: UIViewController {

    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    @IBOutlet weak var btnTermsAndConditions: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var subView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //btnSubmit.roundCorners(cornerRadius: 10.0)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func tapTermsAndConditions(_ sender: UIButton) {
    }
    @IBAction func tapPrivacyPolicy(_ sender: UIButton) {
    }
    @IBAction func tapSubmit(_ sender: UIButton) {
        self.becomeAnAuthorAPI()
    }
    func becomeAnAuthorAPI(){
        LoadingIndicatorView.show("Loading...")
        service.becomeAnAuthorAPI(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == 1{
                        //print((userInfo?.data)!)
                        
                        //loginUserID = ((userInfo?.data.userId)!)
                        //s_default.synchronize()
                        //token = userInfo!.token
                        s_default.set(true, forKey: "Author")

                        isAuthor = true
                        self.performSegue(withIdentifier: "AddListing", sender: self)

                    }else{
                        if userInfo!.message == "This user is already an author"{
                            s_default.set(true, forKey: "Author")
                            isAuthor = true
                            self.performSegue(withIdentifier: "AddListing", sender: self)

                        }
                        self.view.makeToast( userInfo!.message, duration: 2.0, position: .bottom)
                    }
                }else{
                    //temporary
                    s_default.set(true, forKey: "Author")
                    isAuthor = true
                    self.performSegue(withIdentifier: "AddListing", sender: self)
                    //self.view.makeToast("failed response", duration: 2.0, position: .bottom)
                }
            }
            
        })

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
