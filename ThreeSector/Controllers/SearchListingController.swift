//
//  SearchListingController.swift
//  ThreeSector
//
//  Created by BestWeb on 10/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import TaggerKit
import Nuke
class SearchListingController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,CategoriesDelegate,RegionsDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var categoryTF: UITextField!
    @IBOutlet weak var regionTF: UITextField!
    @IBOutlet weak var collectionList: UICollectionView!
    var TAGS = ["Mass Massage","text","Food","Food Massage Massge Massegage","Mass Massage","text","Food","Food Massage Massge Massegage","Mass Massage","text","Food","Food Massage Massge Massegage","Mass Massage","text","Food","Food Massage Massge Massegage","Mass Massage","text","Food","Food Massage Massge Massegage","Mass Massage","text","Food","Food Massage Massge Massegage"]
    @IBOutlet weak var viewTagContainer: UIView!
    var productTags: TKCollectionView!
    var allTags: TKCollectionView!
    @IBOutlet weak var flowLayout: FlowLayout!
    @IBOutlet weak var tagsTblData: UITableView!
    var searchListData = searchFieldData()
    var arrFilterData : [String] = []
    var isSearch : Bool!
    var sizingCell: TagCell?
    var selectedItm:Int?
    var tags = [Tag]()
    var tagValueSelected : Int = 0
    var categoryID:Int = 0
    @IBOutlet weak var searchTF: UITextField!
    var AllData:Array<Dictionary<String,String>> = []
    var SearchData:Array<Dictionary<String,String>> = []
    var search:String=""

    var originalArr = [[String:String]]()
    var searchArrRes = [[String:Any]]()
    var searching:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        isSearch = false
        searchTF.delegate = self
        let cellNib = UINib(nibName: "TagCell", bundle: nil)
        self.collectionList.register(cellNib, forCellWithReuseIdentifier: "TagCell")
        self.collectionList.backgroundColor = UIColor.clear
        self.sizingCell = (cellNib.instantiate(withOwner: nil, options: nil) as NSArray).firstObject as! TagCell?
        self.flowLayout.sectionInset = UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
        self.getSearchListingAPI()
        categoryTF.delegate = self
        categoryTF.addTarget(self, action: #selector(textChanged(textField:)),for: .editingDidBegin)
        regionTF.resignFirstResponder()
        categoryTF.delegate = self
        regionTF.addTarget(self, action: #selector(textRegionChanged(textField:)),for: .editingDidBegin)
        regionTF.resignFirstResponder()
        tagsTblData.isHidden = true
        
      

        AllData = [["name":"Banquet Room", "ID":"38", "type":"Tag"],
                   ["name":"Food&amp; Drink", "ID":"46", "type":"Tag"],
                   ["name":"Shopping", "ID":"35", "type":"Tag"],
                   ["name":"Bar/Lounge", "ID":"39", "type":"Tag"],
                   ["name":"Concierge", "ID":"41", "type":"Tag"],
                   ["name":"Food", "ID":"63", "type":"Tag"],
                   ["name":"Expert", "ID":"31", "type":"Tag"],
                   ["name":"Breakfast Available", "ID":"40", "type":"Tag"],
                   ["name":"Restaurant", "ID":"34", "type":"Tag"],
                   ["name":"Contractor Services", "ID":"53", "type":"Category"],
                   ["name":"Hardware Trading", "ID":"47", "type":"Category"],
                   ["name":"Heavy Machinery", "ID":"49", "type":"Category"],
                   ["name":"Heavy Transportation", "ID":"32", "type":"Category"],
                   ["name":"Manufacturing", "ID":"43", "type":"Category"],
                   ["name":"Professttional Services", "ID":"51", "type":"Category"],
                   ["name":"Raw Materials", "ID":"62", "type":"Category"],
                   ["name":"Wholesale Dealer", "ID":"29", "type":"Region"],
                   ["name":"Kuatan", "ID":"33", "type":"Region"],
                   ["name":"Seremban", "ID":"48", "type":"Region"],
                   ["name":"Kalng", "ID":"61", "type":"Region"],
                   ["name":"Kelantan", "ID":"45", "type":"Region"],
                   ["name":"Petaling Jaya", "ID":"50", "type":"Region"],
                   ["name":"Johor Bahru", "ID":"42", "type":"Region"],
                   ["name":"Ipoh", "ID":"52", "type":"Region"],
                   ["name":"Georgr Town", "ID":"55", "type":"Region"],
                   ["name":"Kuala Lumpur", "ID":"22", "type":"Region"]
        ]
        
        SearchData=AllData
        // Do any additional setup after loading the view.
    }
    @objc func textChanged(textField: UITextField) {
        categoryTF.resignFirstResponder()
        self.navigationController?.navigationBar.isHidden = true
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesListView") as! CategoriesListView
        popOverVC.delegate = self
        self.addChild(popOverVC)
        popOverVC.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    @objc func textRegionChanged(textField: UITextField) {
        regionTF.resignFirstResponder()
        self.navigationController?.navigationBar.isHidden = true
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegionListView") as! RegionListView
        popOverVC.delegate = self
        self.addChild(popOverVC)
        popOverVC.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearch! {
            return SearchData.count
        }
        else{
        return self.tags.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCell", for: indexPath) as! TagCell
        self.configureCell(cell, forIndexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.sizingCell?.setMaximumCellWidth(collectionView.frame.width)
        self.configureCell(self.sizingCell!, forIndexPath: indexPath)
        return self.sizingCell!.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        collectionView.deselectItem(at: indexPath, animated: false)
         if isSearch! {
        for names in SearchData[indexPath.row]{
            tags[indexPath.row].selected = !tags[indexPath.row].selected
            selectedItm = indexPath.row
            if names.key == "ID"{
                tagValueSelected = Int(names.value)!
            }
            if names.key == "type"{
                let selectedType = names.value
                if selectedType == "Category"{
                    performSegue(withIdentifier: "Category", sender: self)
                }else if selectedType == "Region"{
                    self.getRegionTagsListingAPI()
                }else{
                    self.getTagsListingAPI()
                }
            }
            
        }
         }else{
        tags[indexPath.row].selected = !tags[indexPath.row].selected
        selectedItm = indexPath.row
        tagValueSelected = tags[indexPath.row].selectedId!

        if tags[indexPath.row].selectedStr == "Category"{
            performSegue(withIdentifier: "Category", sender: self)
        }else if tags[indexPath.row].selectedStr == "Region"{
            self.getRegionTagsListingAPI()
        }else{
            self.getTagsListingAPI()
        }
        }
        self.collectionList.reloadData()
    }
    
    func configureCell(_ cell: TagCell, forIndexPath indexPath: IndexPath) {
        if isSearch! {
            let tag = tags[indexPath.row]
            var Data:Dictionary<String,String> = SearchData[indexPath.row]

            cell.tagName.text = Data["name"]
            if selectedItm == indexPath.row{
                cell.backgroundColor = tag.selected ? UIColor(cgColor: #colorLiteral(red: 0.2221801281, green: 0.6598004103, blue: 0.9055172801, alpha: 1)) : UIColor(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                cell.tagName.textColor = tag.selected ? UIColor.white : UIColor(cgColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                if tag.selected == true{
                    tagsTblData.isHidden = false

                }else{
                    tagsTblData.isHidden = true
                }

            }else{
                cell.backgroundColor = UIColor.white
                cell.tagName.textColor = UIColor.black
            }
            cell.layer.cornerRadius = 15
            cell.layer.borderWidth = 1
            cell.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }else{
            let tag = tags[indexPath.row]
            cell.tagName.text = tag.name
            if selectedItm == indexPath.row{
                cell.backgroundColor = tag.selected ? UIColor(cgColor: #colorLiteral(red: 0.2221801281, green: 0.6598004103, blue: 0.9055172801, alpha: 1)) : UIColor(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
                cell.tagName.textColor = tag.selected ? UIColor.white : UIColor(cgColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                if tag.selected == true{
                    tagsTblData.isHidden = false
                    
                }else{
                    tagsTblData.isHidden = true
                }
                
            }else{
                cell.backgroundColor = UIColor.white
                cell.tagName.textColor = UIColor.black
            }
            cell.layer.cornerRadius = 15
            cell.layer.borderWidth = 1
            cell.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        
        
        
    }
    // MARK:- textfield
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        isSearch = true

        if string.isEmpty
        {
            search = String(search.dropLast())
        }
        else
        {
            search=textField.text!+string
        }
        
        print(search)
        let predicate=NSPredicate(format: "SELF.name CONTAINS[cd] %@", search)
        let arr=(AllData as NSArray).filtered(using: predicate)
        
        if arr.count > 0
        {
            SearchData.removeAll(keepingCapacity: true)
            SearchData=arr as! Array<Dictionary<String,String>>
        }
        else
        {
            SearchData=AllData
        }
        collectionList.reloadData()
        return true
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Category"{
            let vc = segue.destination as! CategoryController
            vc.categoryId = tagValueSelected
            //vc.listingId = categoriesById.data[tagValue].ID
        }
    }
    // Predicate to filter data
    func getSearchArrayContains(_ text : String) {
        var predicate : NSPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", text)
        let searchTerm = text
        
        let array = self.tags.filter { result in
            return self.tags[0].name!.contains(searchTerm)// and so on...
        }
        arrFilterData = (self.tags as NSArray).filtered(using: predicate) as! [String]
        isSearch = true
        collectionList.reloadData()
    }
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getListingTasData.data.count
    }
    @objc func favouriteBtn(_ sender: UIButton){
//        if sender.isSelected{
//            sender.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
//            sender.isSelected = false
//            DispatchQueue.main.async(execute: {
//                LoadingIndicatorView.show("Creating...")
//                self.favouriesAdd(param: ["postID" : categoriesById.data[sender.tag].ID])
//            })
//
//        }else{
//            sender.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
//            sender.isSelected = true
//            DispatchQueue.main.async(execute: {
//                LoadingIndicatorView.show("Creating...")
//                self.favouriesAdd(param: ["postID" : categoriesById.data[sender.tag].ID])
//            })
//        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! tagsDataCell
        cell.contentView.layer.cornerRadius = 6
        cell.contentView.addShadow()
        cell.title.text = getListingTasData.data[indexPath.row].postTitle
        cell.imageCategory.layer.borderWidth = 1
        cell.imageCategory.layer.masksToBounds = false
        cell.imageCategory.layer.borderColor = UIColor.clear.cgColor
        cell.imageCategory.layer.cornerRadius = cell.imageCategory.frame.height/2
        cell.imageCategory.clipsToBounds = true
        cell.btnfav.tag = indexPath.row
        
        if getListingTasData.data[indexPath.row].oFavorite.isMyFavorite == true{
            cell.btnfav.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            cell.btnfav.isSelected = true
        }else{
            cell.btnfav.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            cell.btnfav.isSelected = false
            
        }
        cell.btnfav.addTarget(self, action: #selector(self.favouriteBtn(_:)), for: .touchUpInside)
        
        if !(getListingTasData.data[indexPath.row].logo.isEmpty){
            
            if URL(string: (getListingTasData.data[indexPath.row].logo)) != nil{
                let imageURL = URL(string: (getListingTasData.data[indexPath.row].logo))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.imageCategory)
            }
        }else{
            cell.imageCategory.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
        cell.lblReviews.text = String(format:"%i/5 Reviews",getListingTasData.data[indexPath.row].oReview.mode)
        cell.ratingView.rating = Double(Float(getListingTasData.data[indexPath.row].oReview.mode))
        if !( getListingTasData.data[indexPath.row].oAddress.address.isEmpty){
            cell.lblAddress.text = getListingTasData.data[indexPath.row].oAddress.address
        }else{
            cell.lblAddress.text = getListingTasData.data[indexPath.row].tagLine
        }
        if !( getListingTasData.data[indexPath.row].oAddress.address.isEmpty){
            cell.lblLocation.text = getListingTasData.data[indexPath.row].oAddress.address
        }else{
            cell.lblLocation.text = "NA"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 273
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //tagValue = indexPath.row
        //performSegue(withIdentifier: "CategoryToDetails", sender: self)
    }

    @IBAction func categoryDropDown(_ sender: UIButton) {
        self.navigationController?.navigationBar.isHidden = true
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesListView") as! CategoriesListView
        popOverVC.delegate = self
        self.addChild(popOverVC)
        popOverVC.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    @IBAction func regionDropDown(_ sender: UIButton) {
        self.navigationController?.navigationBar.isHidden = true
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegionListView") as! RegionListView
        popOverVC.delegate = self
        self.addChild(popOverVC)
        popOverVC.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    func selectedTitle(CategoryName: String, ID: Int) {
        categoryTF.text = CategoryName
        categoryID = ID
    }
    func selectedRegionTitle(CategoryName: String, ID: Int) {
        regionTF.text = CategoryName
    }
    
    func back() {
        
    }
    func getCategoryList(){
        LoadingIndicatorView.show("Creating...")
        service.getCategoriesList(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                categoriesList = userInfo!
            
                if userInfo?.status == "success"{
                    for name in categoriesList.data{
                        let tag = Tag()
                        tag.name = name.name
                        tag.selectedId = name.term_id
                        tag.selectedStr = "Category"
                        print(name.term_id)

                        self.tags.append(tag)
                    }
                   self.getSearchList()
                }else{
                    self.alert(message: userInfo!.status)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    func getSearchList(){
        LoadingIndicatorView.show("Loading...")
        service.getSearchListingAPI(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        self.searchListData = userInfo!
                        for name in self.searchListData.oResults[3].options{
                            let tag = Tag()
                            tag.name = name.name
                            tag.selectedId = name.id
                            tag.selectedStr = "Region"
                            print(name.id)
                            self.tags.append(tag)
                        }
                        self.collectionList.delegate = self
                        self.collectionList.dataSource = self
                        self.collectionList.reloadData()
                    }else{
                        
                        self.view.makeToast( userInfo!.status, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.status, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    func getSearchListingAPI(){
        LoadingIndicatorView.show("Loading...")
        service.getSearchListingAPI(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        getSearchListingData = userInfo!
                        for name in getSearchListingData.oResults[5].options{
                            let tag = Tag()
                            tag.name = name.name
                            tag.selectedId = name.id
                            print(name.id)
                            tag.selectedStr = "Tags"
                            self.tags.append(tag)
                        }
                        self.getCategoryList()
//                        self.collectionList.delegate = self
//                        self.collectionList.dataSource = self
//                        self.collectionList.reloadData()
                    }else{
                        
                        self.view.makeToast( userInfo!.status, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.status, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    func getTagsListingAPI(){
        LoadingIndicatorView.show("Loading...")
        service.getListingTagDetails(tagId:tagValueSelected,completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        getListingTasData = userInfo!
                       
                        self.tagsTblData.delegate = self
                        self.tagsTblData.dataSource = self
                        self.tagsTblData.reloadData()
                    }else{
                        self.tagsTblData.isHidden = true
                        self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.status, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    func getRegionTagsListingAPI(){
        LoadingIndicatorView.show("Loading...")
        service.getRegionSearchListingAPI(tagId:tagValueSelected,completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        getListingTasData = userInfo!
                        
                        self.tagsTblData.delegate = self
                        self.tagsTblData.dataSource = self
                        self.tagsTblData.reloadData()
                    }else{
                        
                        self.view.makeToast( userInfo!.status, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.status, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "CategoryPop"{
//            let vc = segue.destination as! CategoryController
//            vc.categoryId = categoryID
//            //vc.listingId = categoriesById.data[tagValue].ID
//        }
//    }
}

class tagsDataCell : UITableViewCell{
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var ratingImage: UIImageView!
    
    @IBOutlet weak var registeredNo: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var lblcategory: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnfav: UIButton!
    @IBOutlet weak var ratingView: FloatRatingView!
}
