//
//  AddListingWebViewController.swift
//  ThreeSector
//
//  Created by BestWeb on 26/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import WebKit
class AddListingWebViewController: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    var isBecomeAnAuthor : Bool = false
    var loadURL : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let userName = s_default.value(forKey: "UserName") as! String
        let password = s_default.value(forKey: "Password") as! String
        if isBecomeAnAuthor == true{
            loadURL = "http://demo.bestweb.com.my/threesector/login/?ZvhxhcCelG=\(userName)&opEZBQxPYR=\(password)"
        }else{
            loadURL = "http://demo.bestweb.com.my/threesector/login/?ZvhxhcCelG=\(userName)&opEZBQxPYR=\(password)"
        }
        //let myBlog = "https://bestweb.my/3sectornew/login/?ZvhxhcCelG=\(userName)&opEZBQxPYR=\(password)"
        let encodedStr = loadURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

        let url = NSURL(string: encodedStr!)
        let request = NSURLRequest(url: url! as URL)
        
        // init and load request in webview.
       // webView = WKWebView(frame: self.view.frame)
        webView.navigationDelegate = self
        webView.load(request as URLRequest)
        // Do any additional setup after loading the view.
    }
    

    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    //MARK:- WKNavigationDelegate
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        LoadingIndicatorView.show("Loading...")

        print("Strat to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        LoadingIndicatorView.hide()
        print("finish to load")
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString {
            if urlStr == "http://demo.bestweb.com.my/threesector/"{
                var myBlog:String = ""
                if isBecomeAnAuthor == true{
                myBlog = "http://demo.bestweb.com.my/threesector/become-a-merchant/"
                }else{
                   myBlog = "http://demo.bestweb.com.my/threesector/add-listing-plan/"
                }
                let encodedStr = myBlog.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                
                let url = NSURL(string: encodedStr!)
                let request = NSURLRequest(url: url! as URL)
               
                webView.navigationDelegate = self
                webView.load(request as URLRequest)
            }
            //urlStr is what you want
        }
        
        decisionHandler(.allow)
    }
}
