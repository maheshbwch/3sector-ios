//
//  ProfileController.swift
//  ThreeSector
//
//  Created by BestWeb on 06/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
class ProfileController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var bgCoverImg: UIImageView!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var btnFollower: UIButton!
    @IBOutlet weak var btnFollowing: UIButton!
    @IBOutlet weak var tblData: UITableView!
    var titles = ["Account","Listing","Favourites","Change Password","Logout"]
    var images = ["AccountIcon.png","Listing","Favourites","ChangePassword","LogoutIcon"]
    @IBOutlet weak var viewSubHeader: UIView!
    @IBOutlet weak var viewExpired: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSubHeader.layer.shadowColor = UIColor.gray.cgColor
        viewSubHeader.layer.shadowOpacity = 0.3
        viewSubHeader.layer.shadowOffset = CGSize.zero
        viewSubHeader.layer.shadowRadius = 6
        viewExpired.isHidden = true
        self.getProfileDataAPI()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileDataAPI()
    }
    func getProfileDataAPI(){
        LoadingIndicatorView.show("Loading...")
        service.getProfileAPI(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                self.tblData.delegate = self
                self.tblData.dataSource = self
                self.tblData.reloadData()
                if success{
                    if userInfo?.status == "success"{
                         profileDetails = userInfo!
                        self.userIcon.layer.borderWidth = 1
                        self.userIcon.layer.masksToBounds = false
                         self.userIcon.layer.borderColor = UIColor.clear.cgColor
                         self.userIcon.layer.cornerRadius = self.userIcon.frame.height/2
                         self.userIcon.clipsToBounds = true
                         self.userIcon.layer.backgroundColor = UIColor.lightGray.cgColor
                        self.userName.text = profileDetails.oResults.oBasicInfo1.displayName

                        if !(profileDetails.oResults.oBasicInfo1.avatar.isEmpty){
                            
                            if URL(string: (profileDetails.oResults.oBasicInfo1.avatar)) != nil{
                                let imageURL = URL(string: (profileDetails.oResults.oBasicInfo1.avatar))
                                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                                    placeholder: UIImage(named: "Category"),
                                    transition: .fadeIn(duration: 0.20)
                                ), into: self.userIcon)
                            }
                        }else{
                            // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
                            print("no profile image")
                        }
                        if !(profileDetails.oResults.oBasicInfo1.coverImg.isEmpty){
                            
                            if URL(string: (profileDetails.oResults.oBasicInfo1.coverImg)) != nil{
                                let imageURL = URL(string: (profileDetails.oResults.oBasicInfo1.coverImg))
                                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                                    placeholder: UIImage(named: "Category"),
                                    transition: .fadeIn(duration: 0.20)
                                ), into: self.bgCoverImg)
                            }
                        }else{
                            // cell.image.image = #imageLiteral(resourceName: "IndustryIcon2")
                            print("no profile image")
                        }

                    }else if userInfo?.msg == "foundNoUser"{
                    self.alert(message: "Session Expired..Please Login again")
                    self.viewExpired.isHidden = false
                    }else{

                        self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.msg, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    @IBAction func tapEdit(_ sender: UIButton) {
        performSegue(withIdentifier: "EditProfile", sender: self)
    }
    @IBAction func tapLogIn(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        self.performSegue(withIdentifier: "Logout", sender: self)
        
    }
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! profileCell
        cell.title.text = titles[indexPath.row]
        cell.icon.layer.borderWidth = 1
        cell.icon.layer.masksToBounds = false
        cell.icon.layer.borderColor = UIColor.clear.cgColor
        cell.icon.layer.cornerRadius = cell.icon.frame.height/2
        cell.icon.clipsToBounds = true
        cell.icon.layer.backgroundColor = UIColor.clear.cgColor
        cell.icon.image = UIImage.init(imageLiteralResourceName: images[indexPath.row])
        
        cell.viewBG.layer.shadowColor = UIColor.gray.cgColor
        cell.viewBG.layer.shadowOpacity = 0.3
        cell.viewBG.layer.shadowOffset = CGSize.zero
        cell.viewBG.layer.shadowRadius = 6

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 64
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if indexPath.row == 4{
            let alertController = UIAlertController(title: "", message: "Are you sure to Log out?", preferredStyle: .alert)
            let ok = UIAlertAction(title: "No" , style: .default) { (_ action) in
                self.tabBarController?.tabBar.isHidden = false
                
            }
            let cancel = UIAlertAction(title: "Yes" , style: .default) { (_ action) in
                self.tabBarController?.tabBar.isHidden = true
                UserDefaults.standard.set(false, forKey: "isLoggedIn")
                self.performSegue(withIdentifier: "Logout", sender: self)
            }
            ok.setValue(UIColor.red, forKey: "titleTextColor")
            cancel.setValue(UIColor.green, forKey: "titleTextColor")
            alertController.addAction(ok)
            alertController.addAction(cancel)
            alertController.view.tintColor = .white
            alertController.view.backgroundColor = .white
            alertController.view.layer.cornerRadius = 6
            alertController.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor.white
            self.present(alertController, animated: true, completion: nil)
        }else if indexPath.row == 1{
            self.performSegue(withIdentifier: "Listing", sender: self)
        }else if indexPath.row == 0{
            self.performSegue(withIdentifier: "EditProfile", sender: self)
        }else if indexPath.row == 2{
            self.performSegue(withIdentifier: "Favourites", sender: self)
        }else if indexPath.row == 3{
        self.performSegue(withIdentifier: "ChangePassword", sender: self)
        }
        //performSegue(withIdentifier: "CategoryToDetails", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class profileCell : UITableViewCell{
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var viewBG : UIView!
}
