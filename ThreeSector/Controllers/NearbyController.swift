//
//  NearbyController.swift
//  ThreeSector
//
//  Created by BestWeb on 04/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import GoogleMaps
import Nuke
struct State {
    let name: String
    let long: CLLocationDegrees
    let lat: CLLocationDegrees
}
class NearbyController: UIViewController,UITableViewDelegate,UITableViewDataSource,CategoriesDelegate,UITextFieldDelegate,GMSMapViewDelegate {

    @IBOutlet weak var dropDownTF: UITextField!
    @IBOutlet weak var tblData: UITableView!
   
    @IBOutlet weak var viewNoDataFound: UIView!
    @IBOutlet weak var viewMap: UIView!
    var markerDict: [String: GMSMarker] = [:]
    var mapView: GMSMapView!
    var london: GMSMarker?
    var londonView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getNearByList()
        dropDownTF.delegate = self
        dropDownTF.addTarget(self, action: #selector(textChanged(textField:)),for: .editingDidBegin)
        dropDownTF.resignFirstResponder()
       
       
        //so the mapView is of width 200, height 200 and its center is same as center of the self.view
        //mapView?.center = self.viewMap.center
//        let states = [
//            State(name: "Alaska", long: -152.404419, lat: 61.370716),
//            State(name: "Alabama", long: -86.791130, lat: 32.806671),
//            // the other 51 states here...
//        ]
//        for state in states {
//            let state_marker = GMSMarker()
//            state_marker.position = CLLocationCoordinate2D(latitude: state.lat, longitude: state.long)
//            state_marker.title = state.name
//            state_marker.snippet = "Hey, this is \(state.name)"
//            state_marker.map = mapView
//        }
       
    }
    
   
    func updateMapsUI(){
        mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.viewMap.frame.size.height), camera: GMSCameraPosition.camera(withLatitude: Double(categoriesById.data[0].oAddress.lat) as! CLLocationDegrees, longitude: Double(categoriesById.data[0].oAddress.lng) as! CLLocationDegrees, zoom: 10.0))
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 50)
        for state in categoriesById.data.enumerated() {
                        let state_marker = GMSMarker()
            state_marker.position = CLLocationCoordinate2D(latitude: Double(state.element.oAddress.lat) as! CLLocationDegrees, longitude: Double(state.element.oAddress.lng) as! CLLocationDegrees)
//                        state_marker.title = state.name
//                        state_marker.snippet = "Hey, this is \(state.name)"
                        state_marker.map = mapView
                    }
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: 28.7041, longitude: 77.1025)
//        marker.title = "Delhi"
//        marker.snippet = "India’s capital"
//        marker.map = mapView
        self.viewMap.addSubview(mapView!)
    }
//    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//        UIView.animate(withDuration: 5.0, animations: { () -> Void in
//            self.londonView?.tintColor = .blue
//        }, completion: {(finished) in
//            // Stop tracking view changes to allow CPU to idle.
//            self.london?.tracksViewChanges = false
//        })
//    }
    @objc func textChanged(textField: UITextField) {
        dropDownTF.resignFirstResponder()
        self.navigationController?.navigationBar.isHidden = true
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesListView") as! CategoriesListView
        popOverVC.delegate = self
        self.addChild(popOverVC)
        popOverVC.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    @IBAction func tapDropDown(_ sender: UIButton) {
        self.navigationController?.navigationBar.isHidden = true
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesListView") as! CategoriesListView
        popOverVC.delegate = self
        self.addChild(popOverVC)
        popOverVC.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesById.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! categoryCell
        cell.contentView.layer.cornerRadius = 6
        cell.contentView.addShadow()
        cell.title.text = categoriesById.data[indexPath.row].postTitle
        cell.imageCategory.layer.borderWidth = 1
        cell.imageCategory.layer.masksToBounds = false
        cell.imageCategory.layer.borderColor = UIColor.clear.cgColor
        cell.imageCategory.layer.cornerRadius = cell.imageCategory.frame.height/2
        cell.imageCategory.clipsToBounds = true
        cell.btnfav.tag = indexPath.row
        
        if categoriesById.data[indexPath.row].oFavorite.isMyFavorite == true{
            cell.btnfav.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            cell.btnfav.isSelected = true
        }else{
            cell.btnfav.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            cell.btnfav.isSelected = false
            
        }
        cell.btnfav.addTarget(self, action: #selector(self.favouriteBtn(_:)), for: .touchUpInside)
        cell.btnCall.addTarget(self, action: #selector(self.callToCompany(_:)), for: .touchUpInside)
        
        if !(categoriesById.data[indexPath.row].logo.isEmpty){
            
            if URL(string: (categoriesById.data[indexPath.row].logo)) != nil{
                let imageURL = URL(string: (categoriesById.data[indexPath.row].logo))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.imageCategory)
            }
        }else{
            cell.imageCategory.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
        cell.lblReviews.text = String(format:"%i/5 Reviews",categoriesById.data[indexPath.row].oReview.mode)
        cell.ratingView.rating = Double(Float(categoriesById.data[indexPath.row].oReview.mode))
//        if !( categoriesById.data[indexPath.row].oAddress.address.isEmpty){
//            cell.lblAddress.text = categoriesById.data[indexPath.row].oAddress.address
//        }else{
            cell.lblAddress.text = categoriesById.data[indexPath.row].tagLine
       // }
        if !( categoriesById.data[indexPath.row].oAddress.address.isEmpty){
            cell.lblLocation.text = categoriesById.data[indexPath.row].oAddress.address
        }else{
            cell.lblLocation.text = "NA"
        }
        if !( categoriesById.data[indexPath.row].phone.isEmpty){
            cell.btnCall.setTitle(categoriesById.data[indexPath.row].phone, for: .normal)
        }else{
            cell.btnCall.setTitle("NA", for: .normal)
        }
        cell.lblcategory.text = categoriesById.data[indexPath.row].oTerm.name
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 273
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
       
        //performSegue(withIdentifier: "CategoryToDetails", sender: self)
    }
    func selectedTitle(CategoryName: String, ID: Int) {
        dropDownTF.text = CategoryName
    }
    
    func back() {
    
    }
    @objc func favouriteBtn(_ sender: UIButton){
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            sender.isSelected = false
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById.data[sender.tag].ID])
            })
            
        }else{
            sender.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            sender.isSelected = true
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById.data[sender.tag].ID])
            })
        }
    }
    //MARK:- API CALL ADD/REMOVE FAVUORITES
    func favouriesAdd(param: [String:Any]){
        LoadingIndicatorView.show("Loading...")
        service.favouritesAddRemoveAPI(param: param, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        if userInfo?.isAdded == "removed"{
                            
                            self.view.makeToast( "Removed as Favourite", duration: 2.0, position: .bottom)
                        }else{
                            self.view.makeToast( "Added as Favourite", duration: 2.0, position: .bottom)
                        }
                        
                    }else if userInfo?.msg == "tokenExpired"{
                            self.tblData.reloadData()
                            //self.viewExpired.isHidden = false
                    }else{
                        self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }else{
                    self.tblData.reloadData()
                    
                    self.view.makeToast(userInfo!.msg, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    @objc func callToCompany(_ sender: UIButton){
        dialNumber(number: categoriesById.data[sender.tag].phone)
    }
    func dialNumber(number : String) {
        if let phoneCallURL = URL(string: "tel://\(number)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }

   // MARK:- API CALL CATEGORIS By ID
    func getNearByList(){
        LoadingIndicatorView.show("Loading...")
        service.getNearbymeDetails(lat: 21.0407762, lng: 105.7587385, unit: "km", radius: 1500, postType: "nearby", completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)

            if success{
                categoriesById = userInfo!
                if categoriesById.status == "success"{
                    self.viewNoDataFound.isHidden = true
                    self.tblData.isHidden = false
                    self.tblData.delegate = self
                    self.viewMap.isHidden = false
                    self.tblData.dataSource = self
                    self.tblData.keyboardDismissMode = .onDrag
                    self.tblData.reloadData()
                    self.updateMapsUI()
                }else{
                    self.viewMap.isHidden = true
                    self.viewNoDataFound.isHidden = false
                    self.tblData.isHidden = true
                    self.alert(message: userInfo!.status)
                }
            }else{
                self.viewMap.isHidden = true
                self.viewNoDataFound.isHidden = false
                self.tblData.isHidden = true
                self.alert(message: userInfo!.status)
            }
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CategoryToDetails"{
            let vc = segue.destination as! CategoryDetailsController
            //vc.listingId = categoriesById.data[tagValue].ID
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    

}


class nearByCell : UITableViewCell{
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var ratingImage: UIImageView!
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var registeredNo: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var lblcategory: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnfav: UIButton!
    @IBOutlet weak var ratingView: FloatRatingView!
}
