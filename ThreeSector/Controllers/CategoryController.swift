//
//  CategoryController.swift
//  ThreeSector
//
//  Created by BestWeb on 28/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
class CategoryController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblData: UITableView!
    var categoryId : Int = 0
    var tagValue : Int = 0
    @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var viewExpired: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewExpired.isHidden = true
        self.getCategoriesList()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - UITableview delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesById.data.count
    }
    @objc func favouriteBtn(_ sender: UIButton){
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            sender.isSelected = false
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById.data[sender.tag].ID])
            })

        }else{
            sender.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            sender.isSelected = true
            DispatchQueue.main.async(execute: {
                LoadingIndicatorView.show("Creating...")
                self.favouriesAdd(param: ["postID" : categoriesById.data[sender.tag].ID])
            })
        }
    }
    @objc func callToCompany(_ sender: UIButton){
        dialNumber(number: categoriesById.data[sender.tag].phone)
    }
    func dialNumber(number : String) {
        if let phoneCallURL = URL(string: "tel://\(number)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! categoryCell
        cell.contentView.layer.cornerRadius = 6
        cell.contentView.addShadow()
        cell.title.text = categoriesById.data[indexPath.row].postTitle
        cell.imageCategory.layer.borderWidth = 1
        cell.imageCategory.layer.masksToBounds = false
        cell.imageCategory.layer.borderColor = UIColor.clear.cgColor
        cell.imageCategory.layer.cornerRadius = cell.imageCategory.frame.height/2
        cell.imageCategory.clipsToBounds = true
        cell.btnfav.tag = indexPath.row

        if categoriesById.data[indexPath.row].oFavorite.isMyFavorite == true{
            cell.btnfav.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
            cell.btnfav.isSelected = true
        }else{
            cell.btnfav.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
            cell.btnfav.isSelected = false

        }
        cell.btnfav.addTarget(self, action: #selector(self.favouriteBtn(_:)), for: .touchUpInside)
        cell.btnCall.addTarget(self, action: #selector(self.callToCompany(_:)), for: .touchUpInside)

        if !(categoriesById.data[indexPath.row].logo.isEmpty){
            
            if URL(string: (categoriesById.data[indexPath.row].logo)) != nil{
                let imageURL = URL(string: (categoriesById.data[indexPath.row].logo))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.imageCategory)
            }
        }else{
            cell.imageCategory.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
        cell.lblReviews.text = String(format:"%i/5 Reviews",categoriesById.data[indexPath.row].oReview.mode)
        cell.ratingView.rating = Double(Float(categoriesById.data[indexPath.row].oReview.mode))
        if !( categoriesById.data[indexPath.row].oAddress.address.isEmpty){
            cell.lblAddress.text = categoriesById.data[indexPath.row].oAddress.address
        }else{
            cell.lblAddress.text = categoriesById.data[indexPath.row].tagLine
        }
        if !( categoriesById.data[indexPath.row].oAddress.address.isEmpty){
            cell.lblLocation.text = categoriesById.data[indexPath.row].oAddress.address
        }else{
            cell.lblLocation.text = "NA"
        }
        if !( categoriesById.data[indexPath.row].phone.isEmpty){
            cell.btnCall.setTitle(categoriesById.data[indexPath.row].phone, for: .normal)
        }else{
            cell.btnCall.setTitle("NA", for: .normal)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 273
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tagValue = indexPath.row
        performSegue(withIdentifier: "CategoryToDetails", sender: self)
    }
    //MARK:- API CALL ADD/REMOVE FAVUORITES
    func favouriesAdd(param: [String:Any]){
        LoadingIndicatorView.show("Loading...")
        service.favouritesAddRemoveAPI(param: param, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        if userInfo?.isAdded == "removed"{
                             self.view.makeToast( "Removed as Favourite", duration: 2.0, position: .bottom)
                        }else{
                             self.view.makeToast( "Added as Favourite", duration: 2.0, position: .bottom)
                        }
                        
                    }else if userInfo?.msg == "tokenExpired"{
                        self.tblData.reloadData()
                        self.viewExpired.isHidden = false
                    }else{
                        self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }else{
                    self.tblData.reloadData()

                    self.view.makeToast(userInfo!.msg, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    //MARK:- API CALL CATEGORIS By ID
    func getCategoriesList(){
        LoadingIndicatorView.show("Loading...")
        service.getCategoryById(Id: categoryId, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                categoriesById = userInfo!
                if categoriesById.status == "success"{
                    self.titleHeader.text = categoriesById.data[0].oTerm.name
                    self.tblData.delegate = self
                    self.tblData.dataSource = self
                    self.tblData.keyboardDismissMode = .onDrag
                    self.tblData.reloadData()
                }else{
                    self.alert(message: userInfo!.status)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CategoryToDetails"{
            let vc = segue.destination as! CategoryDetailsController
            vc.listingId = categoriesById.data[tagValue].ID
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func tapLogIn(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        self.performSegue(withIdentifier: "Logout", sender: self)
        
    }
    @IBAction func tapBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
}
class categoryCell : UITableViewCell{
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var ratingImage: UIImageView!
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var registeredNo: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var lblcategory: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnfav: UIButton!
     @IBOutlet weak var ratingView: FloatRatingView!
}
