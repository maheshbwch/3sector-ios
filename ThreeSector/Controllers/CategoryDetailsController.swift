//
//  CategoryDetailsController.swift
//  ThreeSector
//
//  Created by BestWeb on 28/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Nuke
class CategoryDetailsController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var logoTop: UIImageView!
    @IBOutlet weak var viewCategoryImage: UIView!
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ratingIcon: UIImageView!
    @IBOutlet weak var lblrating: UILabel!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var lblregNumb: UILabel!
    @IBOutlet weak var imageSSM: UIImageView!
    @IBOutlet weak var businessCategory: UILabel!
    @IBOutlet weak var lblPhone: UITextField!
    @IBOutlet weak var lblFax: UITextField!
    @IBOutlet weak var lblWhatsApp: UITextField!
    @IBOutlet weak var lblEmail: UITextField!
    @IBOutlet weak var lblWebsite: UITextField!
    @IBOutlet weak var aboutCompany: UITextView!
    @IBOutlet weak var ratingPersonIcon: UIImageView!
    @IBOutlet weak var lblRatingPerson: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var titleRating: UILabel!
    @IBOutlet weak var ratingImage2: UIImageView!
    @IBOutlet weak var lblRatingCount: UILabel!
    @IBOutlet weak var ratingDesc: UILabel!
    var listingId : Int = 0
    @IBOutlet weak var titleHeader: UILabel!
    var categoryDetails = singleCategoryResponseDetails()
    @IBOutlet weak var photosCollection: UICollectionView!
    @IBOutlet weak var lblPhotosCount: UILabel!
    @IBOutlet weak var viewPhotos: UIView!
    @IBOutlet weak var viewContact: UIView!
    @IBOutlet weak var heightViewPhotos: NSLayoutConstraint!
    @IBOutlet weak var heightViewContact: NSLayoutConstraint!
    @IBOutlet weak var viewRating: UIView!
    @IBOutlet weak var heightViewRating: NSLayoutConstraint!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var ratingViewBottom: FloatRatingView!
    @IBOutlet weak var btnMessage: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCategoriesList()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- API CALL CATEGORIS By ID
    func getCategoriesList(){
        LoadingIndicatorView.show("Loading...")
        service.getListingDetails(Id: listingId, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                self.categoryDetails = userInfo!
                if self.categoryDetails.status == "success"{
                    self.titleHeader.text = self.categoryDetails.data.oTerm.name
                    self.photosCollection.delegate = self
                    self.photosCollection.dataSource = self
                    self.photosCollection.reloadData()
                    if self.categoryDetails.data.oGallery.large.count == 0{
                        self.heightViewPhotos.constant = 0
                        self.lblPhotosCount.isHidden = true
                    }else{
                        
                    }
                    self.lblPhotosCount.text = String(format:"%i Photos",self.categoryDetails.data.oGallery.large.count)
                    if !(self.categoryDetails.data.coverImg.isEmpty){
                        
                        if URL(string: (self.categoryDetails.data.coverImg)) != nil{
                            let imageURL = URL(string: (self.categoryDetails.data.coverImg))
                            Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                                placeholder: UIImage(named: "Category"),
                                transition: .fadeIn(duration: 0.20)
                            ), into: self.logoTop!)
                        }
                    }else{
                        //self.logoTop.image = #imageLiteral(resourceName: "IndustryIcon2")
                        print("no profile image")
                    }
                    if !(self.categoryDetails.data.logo.isEmpty){
                        
                        if URL(string: (self.categoryDetails.data.logo)) != nil{
                            let imageURL = URL(string: (self.categoryDetails.data.logo))
                            Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                                placeholder: UIImage(named: "Category"),
                                transition: .fadeIn(duration: 0.20)
                            ), into: self.imageCategory!)
                        }
                    }else{
                        //self.logoTop.image = #imageLiteral(resourceName: "IndustryIcon2")
                        print("no profile image")
                    }
                    let userID = s_default.string(forKey: "user_id")
                    if userID == self.categoryDetails.data.oAuthorCategory.userID{
                        self.btnMessage.isEnabled = false
                    }else{
                        self.btnMessage.isEnabled = true

                    }
                    if self.categoryDetails.data.oFavorite.isMyFavorite == true{
                        self.btnFav.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
                        self.btnFav.isSelected = true
                    }else{
                        self.btnFav.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
                        self.btnFav.isSelected = false
                        
                    }
                    self.ratingDesc.text = self.categoryDetails.data.postContent

                    self.imageCategory.layer.borderWidth = 1
                    self.imageCategory.layer.masksToBounds = false
                    self.imageCategory.layer.borderColor = UIColor.clear.cgColor
                    self.imageCategory.layer.cornerRadius = self.imageCategory.frame.height/2
                    self.imageCategory.clipsToBounds = true
                    self.lblTitle.text = self.categoryDetails.data.postTitle
                    self.lblrating.text = String(format:"%i Reviews",self.categoryDetails.data.oReview.mode)
                    self.businessCategory.text = self.categoryDetails.data.oTerm.name
                    self.lblPhone.text = self.categoryDetails.data.phone
                    self.aboutCompany.text = String(format:"%@ %@",self.categoryDetails.data.postTitle,self.categoryDetails.data.tagLine)
                }else{
                    self.alert(message: userInfo!.status)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryDetails.data.oGallery.large.count
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! photosCell
        if !(self.categoryDetails.data.oGallery.large[indexPath.row].url.isEmpty){
            if URL(string: (self.categoryDetails.data.oGallery.large[indexPath.row].url)) != nil{
                let imageURL = URL(string: (self.categoryDetails.data.oGallery.large[indexPath.row].url))
                Nuke.loadImage(with: imageURL!, options: ImageLoadingOptions(
                    placeholder: UIImage(named: "Category"),
                    transition: .fadeIn(duration: 0.20)
                ), into: cell.photo!)
            }
        }else{
            //self.logoTop.image = #imageLiteral(resourceName: "IndustryIcon2")
            print("no profile image")
        }
        return cell
    }
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    @IBAction func tapSeeAllReviews(_ sender: UIButton) {
        performSegue(withIdentifier: "AllReviews", sender: self)
    }
    @IBAction func tapfav(_ sender: UIButton) {
            if sender.isSelected{
                sender.setImage(#imageLiteral(resourceName: "Favourites"), for: .normal)
                sender.isSelected = false
                DispatchQueue.main.async(execute: {
                    LoadingIndicatorView.show("Creating...")
                    self.favouriesAdd(param: ["postID" : categoriesById.data[sender.tag].ID])
                })
                
            }else{
                sender.setImage(#imageLiteral(resourceName: "FavouriteBlue"), for: .normal)
                sender.isSelected = true
                DispatchQueue.main.async(execute: {
                    LoadingIndicatorView.show("Creating...")
                    self.favouriesAdd(param: ["postID" : categoriesById.data[sender.tag].ID])
                })
            }
    }
    //MARK:- API CALL ADD/REMOVE FAVUORITES
    func favouriesAdd(param: [String:Any]){
        LoadingIndicatorView.show("Loading...")
        service.favouritesAddRemoveAPI(param: param, completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        if userInfo?.isAdded == "removed"{
                            self.view.makeToast( "Added as Favourite", duration: 2.0, position: .bottom)
                        }else{
                            self.view.makeToast( "Removed as Favourite", duration: 2.0, position: .bottom)
                        }
                        
                    }else if userInfo?.msg == "tokenExpired"{
                    }else{
                        self.view.makeToast( userInfo!.msg, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.msg, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    @IBAction func tapback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func tapWriteReview(_ sender: UIButton) {
        performSegue(withIdentifier: "AddReview", sender: self)
    }
    @IBAction func tapMessage(_ sender: UIButton) {
        let userID = s_default.string(forKey: "user_id")
        let userImage = s_default.string(forKey: "Image")

        let ref = Constants.refs.databaseRoot.child("Friends").child(userID!).child(self.categoryDetails.data.oAuthorCategory.userID)
        let refUsers = Constants.refs.databaseRoot.child("Users").child(userID!)
        let timeStamp = Date.currentTimeStamp
        print(timeStamp)
        let timeStampStr = String(timeStamp)
        let msgKey = String(format:"%@-%@",userID!,self.categoryDetails.data.oAuthorCategory.userID)
        let message = ["date" : timeStampStr,
                       "message_key" : msgKey,
                       "receiver_id" : self.categoryDetails.data.oAuthorCategory.userID,
                       "receiver_image" : self.categoryDetails.data.oAuthorCategory.avatar,
                       "receiver_name" : self.categoryDetails.data.oAuthorCategory.displayName,
                       "sender_id" : userID,
                       "sender_image" :s_default.string(forKey: "Image") as Any,
                       "sender_name" : s_default.string(forKey: "GetUserName")!] as [String : Any]
                        let userData = ["createdDate" : timeStampStr,
                        "image" : userImage,
                        "name" : s_default.string(forKey: "GetUserName"),
                        "online" : "true",
                        "user_id" : userID] as [String : Any]
        ref.setValue(message)
        refUsers.setValue(userData)
        self.performSegue(withIdentifier: "Chat", sender: self)


    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AllReviews"{
            let vc = segue.destination as! AllReviewsController
            vc.id = self.categoryDetails.data.ID
        }else if segue.identifier == "Chat"{
            let vc = segue.destination as! ChatListController
            vc.receiverID = self.categoryDetails.data.oAuthorCategory.userID
        }else if segue.identifier == "AddReview"{
            let vc = segue.destination as! AddReviewController
            vc.id = self.categoryDetails.data.ID
        }
    }
}
extension CategoryDetailsController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width
        return CGSize(width: 200, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
class photosCell : UICollectionViewCell{
    @IBOutlet weak var photo: UIImageView!
}
