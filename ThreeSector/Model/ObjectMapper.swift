//
//  ObjectMapper.swift
//  ThreeSector
//
//  Created by BestWeb on 27/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import Foundation
import UIKit
import AlamofireObjectMapper
import ObjectMapper


//MARK: -  LOGIN Mapper
class LoginResponse: Mappable{
    
    var status = String()
    var data = LoginInfo()
    var message = String()
    var token = String()
    var orginalResponse = [String:Any]()

    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["oUserInfo"]
        message <- map["msg"]
        token <- map["token"]
        orginalResponse = map.JSON

    }
}
class LoginInfo:Mappable{
    
    var message = String()
    var userId = Int()
    var displayName = String()
    var userName = String()
    var avatar = String()
    var position = String()
    var coverImg = String()
    var role = String()

    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        userId <- map["userID"]
        displayName <- map["displayName"]
        userName <- map["userName"]
        avatar <- map["avatar"]
        position <- map["position"]
        coverImg <- map["coverImg"]
        role <- map["role"]

    }
}

//MARK: -  Registration Mapper
class UserResponse: Mappable{
    
    var status = String()
    var data = RegistrationInfo()
    var message = String()
    var token = String()

    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["oUserInfo"]
        message <- map["msg"]
        token <- map["token"]

    }
}
class RegistrationInfo:Mappable{
    
    var message = String()
    var userId = Int()
    var displayName = String()
    var avatar = String()
    var role = String()

    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        userId <- map["userID"]
        message <- map["Message"]
        displayName <- map["displayName"]
        avatar <- map["avatar"]
        role <- map["role"]

    }
}
//MARK: -  Categories Mapper
class CategoryResponse: Mappable{
    
    var status = String()
    var data = [CategoryInfo]()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["aTerms"]
        
    }
}

class CategoryInfo:Mappable{
    
    var term_id = Int()
    var name = String()
    var slug = String()
    var term_group = Int()
    var term_taxonomy_id = Int()
    var taxonomy = String()
    var description = String()
    var parent = Int()
    var count = Int()
    var filter = String()
    var featuredImg = String()
    var oIcon = oIconData()
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        term_id <- map["term_id"]
        name <- map["name"]
        slug <- map["slug"]
        term_group <- map["term_group"]
        term_taxonomy_id <- map["term_taxonomy_id"]
        taxonomy <- map["taxonomy"]
        description <- map["description"]
        parent <- map["parent"]
        filter <- map["filter"]
        featuredImg <- map["featuredImg"]
        oIcon <- map["oIcon"]
        
    }
}
class oIconData:Mappable{
    
    var type = String()
    var url = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        type <- map["type"]
        url <- map["url"]
 
        
    }
}
//MARK: -  Each Categorie Mapper
class singleCategoryResponse: Mappable{
    
    var status = String()
    var total = String()
    var next = Bool()
    var maxPages = Int()
    var msg = String()
    var data = [singleCategoryInfo]()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        total <- map["total"]
        next <- map["next"]
        maxPages <- map["maxPages"]
        msg <- map["msg"]

        data <- map["oResults"]
        
    }
}

class singleCategoryInfo:Mappable{
    
    var ID = Int()
    var postTitle = String()
    var postLink = String()
    var tagLine = String()
    var phone = String()
    var logo = String()
    var oVideos = String()
    var timezone = String()
    var coverImg = String()
    var oAddress = oAddressData()
    var featuredImg = String()
    var oFeaturedImg = oFeaturedImgData()
    var businessStatus = String()
    var claimStatus = String()
    var oPriceRange = oPriceRangeData()
    var oSocialNetworks = oSocialNetworksData()
    var oReview = oReviewData()
    var oTerm = oTermData()
    var oFavorite = oFavoriteData()

    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        ID <- map["ID"]
        postTitle <- map["postTitle"]
        postLink <- map["postLink"]
        tagLine <- map["tagLine"]
        phone <- map["phone"]
        logo <- map["logo"]
        oVideos <- map["oVideos"]
        timezone <- map["timezone"]
        coverImg <- map["coverImg"]
        oAddress <- map["oAddress"]
        oFeaturedImg <- map["oFeaturedImg"]
        businessStatus <- map["businessStatus"]
        claimStatus <- map["claimStatus"]
        oPriceRange <- map["oPriceRange"]
        oSocialNetworks <- map["oSocialNetworks"]
        oReview <- map["oReview"]
        oTerm <- map["oTerm"]
        oFavorite <- map["oFavorite"]

    }
}
class oAddressData:Mappable{
    
    var lat = String()
    var lng = String()
    var address = String()

  
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
        address <- map["address"]

        
    }
}
class oFavoriteData:Mappable{
    
    var isMyFavorite = Bool()
    var totalFavorites = Int()
    var text = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        isMyFavorite <- map["isMyFavorite"]
        totalFavorites <- map["totalFavorites"]
        text <- map["text"]
        
        
    }
}
class oFeaturedImgData:Mappable{
    
    var large = String()
    var medium = String()
    var thumbnail = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        large <- map["large"]
        medium <- map["medium"]
        thumbnail <- map["thumbnail"]

        
    }
}
class oPriceRangeData:Mappable{
    
    var mode = String()
    var description = String()
    var minimumPrice = String()
    var maximumPrice = String()
    var currency = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        mode <- map["mode"]
        description <- map["description"]
        minimumPrice <- map["minimumPrice"]
        maximumPrice <- map["maximumPrice"]
        currency <- map["currency"]
        
        
    }
}
class oSocialNetworksData:Mappable{
    
    var facebook = String()
    var twitter = String()
    var googlePlus = String()
    var tumblr = String()
    var vk = String()
    var odnoklassniki = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        facebook <- map["facebook"]
        twitter <- map["twitter"]
        googlePlus <- map["googlePlus"]
        tumblr <- map["tumblr"]
        vk <- map["vk"]
        odnoklassniki <- map["odnoklassniki"]
        
    }
}
class oReviewData:Mappable{
        
        var mode = Int()
        var average = Int()
        var quality = Int()
        
        
        required init?(map: Map) { }
        required init() {}
        
        func mapping(map: Map) {
            mode <- map["mode"]
            average <- map["average"]
            quality <- map["quality"]
            
            
        }
}
class oTermData:Mappable{
    
    var term_id = Int()
    var name = String()
    var slug = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        name <- map["name"]
        term_id <- map["term_id"]
        slug <- map["slug"]
        
        
    }
}

//MARK: -  Each CategorieDetails Mapper
class singleCategoryResponseDetails: Mappable{
    
    var status = String()
  
    
    var data = singleCategoryDetailsInfo()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
      
        
        data <- map["oResults"]
        
    }
}

class singleCategoryDetailsInfo:Mappable{
    
    var ID = Int()
    var postTitle = String()
    var postLink = String()
    var postContent = String()

    var tagLine = String()
    var phone = String()
    var logo = String()
    var oVideos = String()
    var timezone = String()
    var coverImg = String()
    var oAddress = oAddressDetailsData()
    var featuredImg = String()
    var oFeaturedImg = oFeaturedImgDetailsData()
    var businessStatus = String()
    var claimStatus = String()
    var oPriceRange = oPriceRangeDetailsData()
    var oSocialNetworks = oSocialNetworksDetailsData()
    var oReview = oReviewDetailsData()
    var oTerm = oTermDetailsData()
    var oGallery = oGalleryData()
    var oAuthorCategory = oAuthorsData()
    var oFavorite = oFavoriteData()

    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        ID <- map["ID"]
        postTitle <- map["postTitle"]
        postLink <- map["postLink"]
        tagLine <- map["tagLine"]
        phone <- map["phone"]
        logo <- map["logo"]
        oVideos <- map["oVideos"]
        timezone <- map["timezone"]
        coverImg <- map["coverImg"]
        oAddress <- map["oAddress"]
        oFeaturedImg <- map["oFeaturedImg"]
        businessStatus <- map["businessStatus"]
        claimStatus <- map["claimStatus"]
        oPriceRange <- map["oPriceRange"]
        oSocialNetworks <- map["oSocialNetworks"]
        oReview <- map["oReview"]
        oTerm <- map["oTerm"]
        oGallery <- map["oGallery"]
        oAuthorCategory <- map["oAuthor"]
        oFavorite <- map["oFavorite"]
        postContent <- map["postContent"]


        
    }
}
class oGalleryData:Mappable{
    
    var large = [largeData]()
    var medium = [mediumData]()

    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        large <- map["large"]
        medium <- map["medium"]
    }
}
class largeData:Mappable{
    
    var url = String()
    var id = String()
    
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        url <- map["url"]
        id <- map["id"]
    }
}
class mediumData:Mappable{
    
    var url = String()
    var id = String()
    
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        url <- map["url"]
        id <- map["id"]
    }
}
class oAddressDetailsData:Mappable{
    
    var lat = String()
    var lng = String()
    var address = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
        address <- map["address"]
        
        
    }
}
class oFeaturedImgDetailsData:Mappable{
    
    var large = String()
    var medium = String()
    var thumbnail = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        large <- map["large"]
        medium <- map["medium"]
        thumbnail <- map["thumbnail"]
        
        
    }
}
class oPriceRangeDetailsData:Mappable{
    
    var mode = String()
    var description = String()
    var minimumPrice = String()
    var maximumPrice = String()
    var currency = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        mode <- map["mode"]
        description <- map["description"]
        minimumPrice <- map["minimumPrice"]
        maximumPrice <- map["maximumPrice"]
        currency <- map["currency"]
        
        
    }
}
class oSocialNetworksDetailsData:Mappable{
    
    var facebook = String()
    var twitter = String()
    var googlePlus = String()
    var tumblr = String()
    var vk = String()
    var odnoklassniki = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        facebook <- map["facebook"]
        twitter <- map["twitter"]
        googlePlus <- map["googlePlus"]
        tumblr <- map["tumblr"]
        vk <- map["vk"]
        odnoklassniki <- map["odnoklassniki"]
        
    }
}
class oReviewDetailsData:Mappable{
    
    var mode = Int()
    var average = Int()
    var quality = Int()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        mode <- map["mode"]
        average <- map["average"]
        quality <- map["quality"]
        
        
    }
}
class oTermDetailsData:Mappable{
    
    var term_id = Int()
    var name = String()
    var slug = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        name <- map["name"]
        term_id <- map["term_id"]
        slug <- map["slug"]
        
        
    }
}
//MARK: -  Become An Author Mapper
class BecomeAnAuthor: Mappable{
    
    var status = Int()
    var message = String()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        message <- map["message"]
        
    }
}

class newsData: Mappable{
    
    var status = String()
    var blogTitl = String()
    var next = Bool()

    
    var data = [newsInfo]()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        blogTitl <- map["blogTitle"]

        
        data <- map["oResults"]
        
    }
}

class newsInfo:Mappable{
    
    var postID = Int()
    var postTitle = String()
    var postContent = String()
    var postDate = String()
    var countComments = Int()
    
    var oAuthor = oAuthorsData()
   var oFeaturedImg = oFeaturedImgDataNews()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        postID <- map["postID"]
        postTitle <- map["postTitle"]
        postContent <- map["postContent"]
        postDate <- map["postDate"]
        countComments <- map["countComments"]
       oFeaturedImg <- map["oFeaturedImg"]
        oAuthor <- map["oAuthor"]
        
        
    }
}
class oAuthorsData:Mappable{
    var userID = String()
    var avatar = String()
    var displayName = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        userID <- map["ID"]
        displayName <- map["displayName"]
        avatar <- map["avatar"]

        
    }
}
class oFeaturedImgDataNews:Mappable{
    
    var large = String()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        large <- map["large"]
        
        
    }
}
class profileData: Mappable{
    
    var status = String()
    var msg = String()
    var oResults = profileInfo()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]

        oResults <- map["oResults"]
        
    }
}

class profileInfo:Mappable{
    
    var oBasicInfo1 = oBasicInfo()
   
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        
        oBasicInfo1 <- map["oBasicInfo"]
        
        
    }
}
class oBasicInfo:Mappable{
    
    var userID = Int()
    var email = String()
    var displayName = String()
    var userName = String()
    var avatar = String()
    var position = String()
    var coverImg = String()
    var first_name = String()
    var last_name = String()
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        userID <- map["userID"]
        email <- map["email"]
        avatar <- map["avatar"]
        displayName <- map["display_name"]
        userName <- map["user_name"]
        position <- map["position"]
        coverImg <- map["cover_image"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        
    }
}
class favouritesData: Mappable{
    
    var status = String()
    var msg = String()

    var oResults = [favouritesInfo]()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]

        oResults <- map["oResults"]
        
    }
}

class favouritesInfo:Mappable{
    var postTitle = String()
    var tagLine = String()
    var ID = Int()

    var oFeaturedImg = oFeaturedImgInfo()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        
        oFeaturedImg <- map["oFeaturedImg"]
        postTitle <- map["title"]
        ID <- map["ID"]

        tagLine <- map["tagLine"]

    }
}
class oFeaturedImgInfo:Mappable{
    var large = String()
    var medium = String()

    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        
        large <- map["large"]
        medium <- map["medium"]

        
    }
}
class changePasswordData: Mappable{
    
    var status = String()
    
    
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]

        
    }
}
class favouritesAddRemoveData: Mappable{
    
    var status = String()
    var msg = String()
    var isAdded = String()

    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
        isAdded <- map["is"]

        
    }

}

class reviewsData:Mappable{
    var mode = Int()
    var average = Int()
    var quality = String()
    var total = Int()
    var maxPages = Int()
    var next = Bool()
    var oAverageDetailsReview = [oAverageDetailsReviewData]()
    var aReviews = [aReviewsData]()

    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        mode <- map["mode"]
        average <- map["average"]
        quality <- map["quality"]
        maxPages <- map["maxPages"]
        next <- map["next"]
        oAverageDetailsReview <- map["oAverageDetailsReview"]
        aReviews <- map["aReviews"]

    }
}
class oAverageDetailsReviewData:Mappable{
    var average = Int()
    var text = String()
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        average <- map["average"]
        text <- map["text"]

        
    }
}
class aReviewsData:Mappable{
    var ID = Int()
    var userID = Int()
    var postTitle = String()
    var permalink = String()
    var shareURL = String()
    var postContent = String()
    var postDate = String()
    var hasDiscussion = String()
    var oGallery = oGalleryDataInfo()
    var isLiked = Bool()
    var oReviews = oReviewsInfo()
    var oUserInfo = oUserInfoInfo()
    
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        ID <- map["ID"]
        userID <- map["userID"]
        postTitle <- map["postTitle"]
        permalink <- map["permalink"]
        shareURL <- map["shareURL"]
        postContent <- map["postContent"]
        postDate <- map["postDate"]
        hasDiscussion <- map["hasDiscussion"]
        oGallery <- map["oGallery"]
        isLiked <- map["isLiked"]
        oReviews <- map["oReviews"]
        oUserInfo <- map["oUserInfo"]

        
    }
}
class oGalleryDataInfo:Mappable{
    var large = [largeInfo]()
    var medium = [mediumInfo]()
  
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        large <- map["large"]
        medium <- map["medium"]
       
    }
}
class largeInfo:Mappable{
    var url = String()
    var id = Int()
    
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        url <- map["url"]
        id <- map["id"]
        
    }
}
class mediumInfo:Mappable{
    var url = String()
    var id = Int()
    
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        url <- map["url"]
        id <- map["id"]
        
    }
}
class oReviewsInfo:Mappable{
    var oDetails = [oDetailsInfo]()
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        oDetails <- map["oDetails"]
       
        
    }
}
class oDetailsInfo:Mappable{
    var name = String()
    var key = String()
    var isEditable = String()
    var score = Int()

    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        name <- map["name"]
        key <- map["key"]
        isEditable <- map["isEditable"]
        score <- map["score"]

    }
}
class oUserInfoInfo:Mappable{
    var userID = Int()
    var avatar = String()
    var displayName = String()
    var position = String()
    var phone = String()
    var address = String()
    var coverImage = String()
    var website = String()
    var email = oGalleryData()
  
    
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        userID <- map["userID"]
        avatar <- map["avatar"]
        displayName <- map["displayName"]
        position <- map["position"]
        phone <- map["phone"]
        address <- map["postContent"]
        coverImage <- map["coverImage"]
        website <- map["website"]
        email <- map["email"]
        
    }
    
}
class nearByListData:Mappable{
    var status = String()
    var msg = String()
    
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["url"]
        msg <- map["msg"]
        
    }
}
class searchFieldData:Mappable{
    var status = String()
    var oResults = [searchFieldInfo]()
    
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        oResults <- map["oResults"]
        
    }
}
class searchFieldInfo:Mappable{
    
    var type = String()
    var label = String()
    var fromLabel = String()
    var toLabel = String()
    var key = String()
    var isDefault = String()
    var adminCategory = String()
    var childType = String()
    var options = [optionsDetailsData]()
    var name = String()
    var oFeaturedImg = oFeaturedImgDetailsData()
    var value = String()
 
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        type <- map["type"]
        label <- map["label"]
        fromLabel <- map["fromLabel"]
        toLabel <- map["toLabel"]
        key <- map["key"]
        isDefault <- map["isDefault"]
        adminCategory <- map["adminCategory"]
        childType <- map["childType"]
        options <- map["options"]
        name <- map["name"]
        value <- map["value"]
   
    }
}
class optionsDetailsData:Mappable{
    
    var name = String()
    var slug = String()
    var id = Int()
    var selected = Bool()
    var count = Int()
   
    
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        name <- map["name"]
        slug <- map["slug"]
        id <- map["id"]
        selected <- map["selected"]
        count <- map["count"]
     
    }
}
class listingTagsResponse: Mappable{
    
    var status = String()
    var total = String()
    var next = Bool()
    var maxPages = Int()
    var msg = String()
    var data = [listingTagsInfo]()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        total <- map["total"]
        next <- map["next"]
        maxPages <- map["maxPages"]
        msg <- map["msg"]
        
        data <- map["aListings"]
        
    }
}

class listingTagsInfo:Mappable{
    
    var ID = Int()
    var postTitle = String()
    var postLink = String()
    var tagLine = String()
    var phone = String()
    var logo = String()
    var oVideos = String()
    var timezone = String()
    var coverImg = String()
    var oAddress = oAddressData()
    var featuredImg = String()
    var oFeaturedImg = oFeaturedImgData()
    var businessStatus = String()
    var claimStatus = String()
    var oPriceRange = oPriceRangeData()
    var oSocialNetworks = oSocialNetworksData()
    var oReview = oReviewData()
    var oTerm = oTermData()
    var oFavorite = oFavoriteData()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        ID <- map["ID"]
        postTitle <- map["postTitle"]
        postLink <- map["postLink"]
        tagLine <- map["tagLine"]
        phone <- map["phone"]
        logo <- map["logo"]
        oVideos <- map["oVideos"]
        timezone <- map["timezone"]
        coverImg <- map["coverImg"]
        oAddress <- map["oAddress"]
        oFeaturedImg <- map["oFeaturedImg"]
        businessStatus <- map["businessStatus"]
        claimStatus <- map["claimStatus"]
        oPriceRange <- map["oPriceRange"]
        oSocialNetworks <- map["oSocialNetworks"]
        oReview <- map["oReview"]
        oTerm <- map["oTerm"]
        oFavorite <- map["oFavorite"]
        
    }
}
class ProfilUpdateResponse:Mappable{
    
    var status = String()
    var message = String()
    var data = ProfilUpdateResponseData1()
    var orginalResponse = [String:Any]()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        message <- map["msg"]
        data <- map["oResults"]
        orginalResponse = map.JSON
        
    }
}
class ProfilUpdateResponseData1:Mappable{
    
  
    var oBasicInfoProfile = ProfilUpdateResponseData()
    var orginalResponse = [String:Any]()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        oBasicInfoProfile <- map["oBasicInfo"]
        
        orginalResponse = map.JSON
        
    }
}
class ProfilUpdateResponseData:  Mappable{
    
    var Message = String()
    var user_email = String()
    var user_name = String()
    var user_gender = String()
    var user_race = String()
    var user_date_of_birth = String()
    var user_phone_number = String()
    var user_lang_spoke_write = String()
    var user_highest_education = String()
    var user_profile_picture = String()
    var current_working_place = String()
    var working_option = String()
    var referral_code = String()
    
    var certificate_1 = String()
    var certificate_2 = String()
    var certificate_3 = String()
    var certificate_4 = String()
    var certificate_5 = String()
    var ic_image = String()
    var passport_image = String()
    var visa_image = String()
    var location = String()
    var emergeny_contact_no = String()
    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        Message <- map["Message"]
        user_email <- map["user_email"]
        user_name <- map["user_name"]
        user_gender <- map["user_gender"]
        user_race <- map["user_race"]
        user_date_of_birth <- map["user_date_of_birth"]
        user_phone_number <- map["user_phone_number"]
        user_lang_spoke_write <- map["user_lang_spoke_write"]
        user_highest_education <- map["user_highest_education"]
        user_profile_picture <- map["user_profile_picture"]
        current_working_place <- map["current_working_place"]
        working_option <- map["working_option"]
        referral_code <- map["referral_code"]
        
        certificate_1 <- map["certificate_1"]
        certificate_2 <- map["certificate_2"]
        certificate_3 <- map["certificate_3"]
        certificate_4 <- map["certificate_4"]
        certificate_5 <- map["certificate_5"]
        ic_image <- map["ic_image"]
        passport_image <- map["passport_image"]
        visa_image <- map["visa_image"]
        location <- map["location"]
        emergeny_contact_no <- map["emergeny_contact_no"]
        
    }
}
class chatHeadersData1: Mappable{
    
    var status = String()
    var msg = String()
    var oResults = [chatHeadersInfo]()
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
        
        oResults <- map["oResults"]

    }
}
class oProfileChatHeaders:Mappable{
    
    var oBasicInfo1 = oBasicInfoChatHeaders()

    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        
        oBasicInfo1 <- map["oBasicInfo"]

        
    }
}
class chatHeadersInfo:Mappable{
    var oProfile = oProfileChatHeaders()
    var oMessage = oMessageInfoChatHeaders()

    
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        oProfile <- map["oProfile"]
        oMessage <- map["oMessage"]

        
        
    }
}
class oBasicInfoChatHeaders:Mappable{
    
    var userID = String()
    var email = String()
    var displayName = String()
    var userName = String()
    var avatar = String()
    var position = String()
    var coverImg = String()
    var first_name = String()
    var last_name = String()
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        userID <- map["userID"]
        email <- map["email"]
        avatar <- map["avatar"]
        displayName <- map["display_name"]
        userName <- map["user_name"]
        position <- map["position"]
        coverImg <- map["cover_image"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        
    }
}
class oMessageInfoChatHeaders:Mappable{
  
    var at = String()
    var content = String()
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        at <- map["at"]
        content <- map["content"]
    
        
    }
}
class sendMessageData: Mappable{
    
    var status = String()
    var msg = String()
   
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
        
        
    }
}
class AddReviewData:Mappable{
    
    var status = String()
    var msg = String()
    required init?(map: Map) { }
    required init() {}
    
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
        
        
    }
}

//struct Search:Decodable {
//    var ID : String
//    var dt_bod : String
//    var e_gender : String
//    var int_glcode : String
//    var var_email : String
//    var var_fname : String
//    var var_phoneno : String
//    var var_uname : String
//
//    init(userdata : [String:Any]) {
//        self.ID = userdata["ID"] as! String
//        self.dt_bod = userdata["dt_bod"] as! String
//        self.e_gender = userdata["e_gender"] as! String
//        self.int_glcode = userdata["int_glcode"] as! String
//        self.var_email = userdata["var_email"] as! String
//        self.var_fname = userdata["var_fname"] as! String
//        self.var_phoneno = userdata["var_phoneno"] as! String
//        self.var_uname = userdata["var_uname"] as! String
//
// }
//}
