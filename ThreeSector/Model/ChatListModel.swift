//
//  ChatListModel.swift
//  ThreeSector
//
//  Created by BestWeb on 28/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import Foundation
struct ChatListDataModel{
    var messageKey       : String!
    var messageDate    : String!
    var messageText     : String!
    var senderID   : String!
    var messageType   : String!

}
