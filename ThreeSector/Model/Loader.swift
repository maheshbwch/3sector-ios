//
//  Loader.swift
//  ThreeSector
//
//  Created by BestWeb on 27/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import Foundation
import UIKit

class LoadingIndicatorView {
    
    static var currentOverlay : UIView?
    
    static func show() {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            return
        }
        show(currentMainWindow)
    }
    
    static func show(_ loadingText: String) {
        DispatchQueue.main.async {
            guard let currentMainWindow = UIApplication.shared.keyWindow else {
                return
            }
            show(currentMainWindow, loadingText: loadingText)
        }
    }
    
    static func show(_ overlayTarget : UIView) {
        show(overlayTarget, loadingText: nil)
    }
    
    static func show(_ overlayTarget : UIView, loadingText: String?) {
        hide()
        
        let overlay = UIView(frame: CGRect(x: 0, y: 0, width: overlayTarget.frame.width, height: overlayTarget.frame.height))//UIView(frame: CGRect(x: (overlayTarget.frame.width - 50)/2, y: (overlayTarget.frame.height - 50)/2 , width: 100, height: 100))
        overlay.tag = 100
        overlay.center = overlayTarget.center
        overlay.alpha = 1
        overlay.backgroundColor = UIColor.clear//.withAlphaComponent(0.5)
        overlay.layer.cornerRadius = 5
        overlayTarget.addSubview(overlay)
        overlayTarget.bringSubviewToFront(overlay)
        
        let overView = UIView(frame: CGRect(x: (overlay.frame.width - 150)/2, y: (overlay.frame.height - 150)/2 , width: 150, height: 150))
        overView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        overView.layer.cornerRadius = 8
        overlay.addSubview(overView)
        
        let indicator = UIActivityIndicatorView(style: .white)
        indicator.tag = 101
        indicator.frame = CGRect(x: (overView.frame.width - 30)/2, y: (overView.frame.height - 30)/2 - 10, width: 30, height: 30)
        indicator.color = .white
        //        indicator.center = overlay.center
        indicator.startAnimating()
        overView.addSubview(indicator)
        
        if let textString = loadingText {
            let label = UILabel()
            label.tag = 102
            label.text = textString
            label.textColor = .white
            label.font = UIFont(name: "Axiforma-Medium", size: 14)
            label.sizeToFit()
            label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30)
            overView.addSubview(label)
        }
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        //        overlay.alpha = overlay.alpha > 0 ? 0 : 1 // 0.7
        UIView.commitAnimations()
        
        currentOverlay = overlay
    }
    
    static func hide() {
        if currentOverlay != nil {
            //            DispatchQueue.main.async {
            currentOverlay?.removeFromSuperview()
            currentOverlay =  nil
            //            }
        }
    }
}
