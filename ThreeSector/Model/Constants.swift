//
//  Constants.swift
//  ThreeSector
//
//  Created by BestWeb on 27/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import Firebase

struct Constants
{
    struct refs
    {
        static let databaseRoot = Database.database().reference()
        static let databaseUsers = databaseRoot.child("TestFriends")
        static let databaseFriends = databaseRoot.child("TestUsers")
        
    }
}
let S_IS_IPAD = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
let S_SCREEN_WIDTH = UIScreen.main.bounds.size.width
let S_SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let s_default = UserDefaults.standard
let DEVELOPER = true

//http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/auth
var BASE_URL = "http://demo.bestweb.com.my/threesector/wp-json/"
var API_KEY = "293df0c34d1d7d71fe036200cb70e871"
var FCM_TOKEN = "fcmtoken"
var deviceId="1Xa26YMQnCtjL7alNa7KFTCAfwjfyJENESrWvAxX"
var token = ""



func S_rgb (_ red:Int, green:Int, blue:Int, alpha:CGFloat ) -> UIColor {
    
    return UIColor(red: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: alpha)
}

func alertTextColor ( _ alpha:CGFloat ) -> UIColor {
    
    return UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: alpha)
}


