//
//  Contact.swift
//  ThreeSector
//
//  Created by BestWeb on 24/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit

@objc class Contact : NSObject {
    @objc var givenName: String!
    @objc var familyName: String!
    @objc var mobile: String!
    
    init(givenName: String, familyName: String, mobile: String) {
        self.givenName = givenName
        self.familyName = familyName
        self.mobile = mobile
    }
}
