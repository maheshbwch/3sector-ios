//
//  Service.swift
//  ThreeSector
//
//  Created by BestWeb on 27/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import OAuthSwift


//import ReachabilitySwift

var AlamoManager = Alamofire.SessionManager.default
var oauth = OAuthSwiftClient.self

class eService{
    
    // base url is the store url
    var baseURL:String!
    
    init(baseURL:String) {
        self.baseURL = baseURL
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders//defaultHTTPHeaders
        
        AlamoManager =  Alamofire.SessionManager(configuration: configuration)
        
        
    }
    
    
    
    let header = ["Content-Type": "application/json"]
    let bear = String(format:"Bearer %@",token)
    let headerAuthor = ["Authorization": String(format:"Bearer %@",token)]

    // to cancel all the service which are in progress
    func cancelAllRequests(){
        AlamoManager.session.getAllTasks(completionHandler: {tasks in tasks.forEach({ $0.cancel() })})
}
    //MARK: -  USER LOGIN API
    func UserLogin(param:[String: Any],completionHandler:@escaping (  _ success: Bool,   _ response: LoginResponse?) -> ()) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if value is String{
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                }else if value is Int{
                    multipartFormData.append((value as! Int).toString().data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: false)!, withName: key)
                }else{
                    print("key",key)
                }
            }
        }, to: self.baseURL + "wiloke/v2/auth", method: .post, headers: ["Content-Type":"multipart/form-data;"], encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<LoginResponse>) in
                    
                    if let statusCode = response.response?.statusCode{
                        switch statusCode{
                        case 200:
                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                        default:
                            completionHandler(false,response.result.value)
                        }
                    }else{
                        completionHandler(false,response.result.value)
                    }
                }
            case .failure(_):
                print("error")
                // completionHandler(false,response.result.value)
            }
        })
    }
    //MARK: -  Registration API
    func newUserRegisteration(param:[String: Any],completionHandler:@escaping (  _ success: Bool,   _ response: UserResponse?) -> ()) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if value is String{
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                }else if value is Int{
                    multipartFormData.append((value as! Int).toString().data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: false)!, withName: key)
                }else{
                    print("key",key)
                }
            }
        }, to: self.baseURL + "wiloke/v2/signup", method: .post, headers: ["Content-Type":"multipart/form-data;"], encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<UserResponse>) in
                    
                    if let statusCode = response.response?.statusCode{
                        switch statusCode{
                        case 200:
                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                        default:
                            completionHandler(false,response.result.value)
                        }
                    }else{
                        completionHandler(false,response.result.value)
                    }
                }
            case .failure(_):
                print("error")
                // completionHandler(false,response.result.value)
            }
        })
    }
    func getCategoriesList(completionHandler:@escaping (  _ success: Bool,   _ response: CategoryResponse?) -> ()) {
        
        let requestURL =  self.baseURL + "wiloke/v2/taxonomies/listing-categories"
        
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).validate().responseObject { (response: DataResponse<CategoryResponse>) in
                let json = response.data
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func getCategoryById(Id:Int,completionHandler:@escaping (  _ success: Bool,   _ response: singleCategoryResponse?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/custom-listing-category/\(Id)"
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: headers).validate().responseObject { (response: DataResponse<singleCategoryResponse>) in
                let json = response.data
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func getListingDetails(Id:Int,completionHandler:@escaping (  _ success: Bool,   _ response: singleCategoryResponseDetails?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/custom-listing-detail/\(Id)"
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: headers).validate().responseObject { (response: DataResponse<singleCategoryResponseDetails>) in
                let json = response.data
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }

    //MARK: -  Become An Author API
    func becomeAnAuthorAPI(completionHandler:@escaping (  _ success: Bool,   _ response: BecomeAnAuthor?) -> ()) {
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in

        }, to: self.baseURL + "wiloke/v2/bw_become_author", method: .post, headers:headers, encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<BecomeAnAuthor>) in
                    
                    if let statusCode = response.response?.statusCode{
                        switch statusCode{
                        case 200:
                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                        default:
                            completionHandler(false,response.result.value)
                        }
                    }else{
                        completionHandler(false,response.result.value)
                    }
                }
            case .failure(_):
                print("error")
                // completionHandler(false,response.result.value)
            }
        })
    }
    func changesPasswordAPI(param:[String: Any],completionHandler:@escaping (  _ success: Bool,   _ response: changePasswordData?) -> ()) {
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if value is String{
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                }else if value is Int{
                    multipartFormData.append((value as! Int).toString().data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: false)!, withName: key)
                }else{
                    print("key",key)
                }
            }
        }, to: self.baseURL + "wiloke/v2/update-password", method: .post, headers: headers, encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<changePasswordData>) in
                    
                    if let statusCode = response.response?.statusCode{
                        switch statusCode{
                        case 200:
                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                        default:
                            completionHandler(false,response.result.value)
                        }
                    }else{
                        completionHandler(false,response.result.value)
                    }
                }
            case .failure(_):
                print("error")
                // completionHandler(false,response.result.value)
            }
        })
    }
//    func getProfileAPI(completionHandler:@escaping (  _ success: Bool,   _ response: profileData?) -> ()) {
//
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//
//        }, to: "https://bestweb.my/3sectornew/wp-json/wiloke/v2/get-profile", method: .get, headers: ["Authorization": "Bearer \(token)"], encodingCompletion: { encodingResult in
//
//            switch encodingResult {
//
//            case .success(let upload, _, _):
//                upload.validate().responseObject {(response: DataResponse<profileData>) in
//
//                    if let statusCode = response.response?.statusCode{
//                        switch statusCode{
//                        case 200:
//                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
//                        default:
//                            completionHandler(false,response.result.value)
//                        }
//                    }else{
//                        completionHandler(false,response.result.value)
//                    }
//                }
//            case .failure(_):
//                print("error")
//                // completionHandler(false,response.result.value)
//            }
//        })
//    }
    func getNews(completionHandler:@escaping (  _ success: Bool,   _ response: newsData?) -> ()) {
        
        let requestURL =  "https://bestweb.my/3sectornew/wp-json/wiloke/v2/posts"
        
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).validate().responseObject { (response: DataResponse<newsData>) in
                let json = response.data
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func getProfileAPI(completionHandler:@escaping (  _ success: Bool,   _ response: profileData?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/get-profile"
        var headers = [String: String]()

        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: headers).validate().responseObject { (response: DataResponse<profileData>) in
                let json = response.data
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func getFavouritesAPI(completionHandler:@escaping (  _ success: Bool,   _ response: favouritesData?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/get-my-favorites"
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: headers).validate().responseObject { (response: DataResponse<favouritesData>) in
                let json = response.data
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func getListings(page:Int,postType:String,completionHandler:@escaping (  _ success: Bool,   _ response: singleCategoryResponse?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/get-my-listings?postType=\(postType)&page=\(page)"
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: headers).validate().responseObject { (response: DataResponse<singleCategoryResponse>) in
                let json = response.data
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func getReviews(Id:Int,completionHandler:@escaping (  _ success: Bool,   _ response: reviewsData?) -> ()) {
        
        let requestURL =  self.baseURL + "wiloke/v2/posts/\(Id)/reviews"
        
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).validate().responseObject { (response: DataResponse<reviewsData>) in
                let json = response.data
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func favouritesAddRemoveAPI(param:[String: Any],completionHandler:@escaping (  _ success: Bool,   _ response: favouritesAddRemoveData?) -> ()) {
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if value is String{
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                }else if value is Int{
                    multipartFormData.append((value as! Int).toString().data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: false)!, withName: key)
                }else{
                    print("key",key)
                }
            }
        }, to: self.baseURL + "wiloke/v2/add-to-my-favorites", method: .post, headers: headers, encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<favouritesAddRemoveData>) in
                    
                    if let statusCode = response.response?.statusCode{
                        switch statusCode{
                        case 200:
                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                        default:
                            completionHandler(false,response.result.value)
                        }
                    }else{
                        completionHandler(false,response.result.value)
                    }
                }
            case .failure(_):
                print("error")
                // completionHandler(false,response.result.value)
            }
        })
    }
    func favouritesRemoveAPI(param:[String: Any],completionHandler:@escaping (  _ success: Bool,   _ response: favouritesAddRemoveData?) -> ()) {
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if value is String{
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                }else if value is Int{
                    multipartFormData.append((value as! Int).toString().data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: false)!, withName: key)
                }else{
                    print("key",key)
                }
            }
        }, to: self.baseURL + "wiloke/v2/remove-from-my-favorites", method: .post, headers: headers, encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<favouritesAddRemoveData>) in
                    
                    if let statusCode = response.response?.statusCode{
                        switch statusCode{
                        case 200:
                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                        default:
                            completionHandler(false,response.result.value)
                        }
                    }else{
                        completionHandler(false,response.result.value)
                    }
                }
            case .failure(_):
                print("error")
                // completionHandler(false,response.result.value)
            }
        })
    }
    func getNearbymeDetails(lat:Double,lng:Double,unit:String,radius:Double,postType:String,completionHandler:@escaping (  _ success: Bool,   _ response: singleCategoryResponse?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/custom_nearbyme?lat=\(lat)&lng=\(lng)&unit=\(unit)&radius=\(radius)"
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: headers).validate().responseObject { (response: DataResponse<singleCategoryResponse>) in
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func getSearchListingAPI(completionHandler:@escaping (  _ success: Bool,   _ response: searchFieldData?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/search-fields/listing"
        
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).validate().responseObject { (response: DataResponse<searchFieldData>) in
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func getRegionSearchListingAPI(tagId:Int,completionHandler:@escaping (  _ success: Bool,   _ response: listingTagsResponse?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/custom-listing-location/\(tagId)"
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: headers).validate().responseObject { (response: DataResponse<listingTagsResponse>) in
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func getListingTagDetails(tagId:Int,completionHandler:@escaping (  _ success: Bool,   _ response: listingTagsResponse?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/listing_tag/\(tagId)"
        
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).validate().responseObject { (response: DataResponse<listingTagsResponse>) in
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func updateProfileImage(data: Data,data1: Data,param:[String: Any],completionHandler:@escaping (  _ success: Bool,   _ response: ProfilUpdateResponse?) -> ()) {
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if value is String{
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                }else if value is Int{
                    multipartFormData.append((value as! Int).toString().data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: false)!, withName: key)
                }else{
                    print("key",key)
                }
                
                
                multipartFormData.append(data, withName: "async-upload", fileName:"image.png", mimeType: "image/png")
                
                
            }
            
            
        }, to: "http://demo.bestweb.com.my/threesector/wp-admin/custom_ajax_uploads.php", method: .post, headers: nil, encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<ProfilUpdateResponse>) in
                    
                    if let statusCode = response.response?.statusCode{
                        switch statusCode{
                        case 200:
                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                        default:
                            completionHandler(false,response.result.value)
                        }
                    }else{
                        completionHandler(false,response.result.value)
                    }
                }
            case .failure(_):
                print("error")
                //completionHandler(false,response.result.value)
            }
        })
    }
    func updateProfile(data: Data,data1: Data,param:[String: Any],completionHandler:@escaping (  _ success: Bool,   _ response: ProfilUpdateResponse?) -> ()) {
        var headers = [String: String]()

        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if value is String{
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                }else if value is Int{
                    multipartFormData.append((value as! Int).toString().data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: false)!, withName: key)
                }else{
                    print("key",key)
                }
                
                
                    multipartFormData.append(data, withName: "cover_image", fileName:"image.png", mimeType: "image/png")
                    multipartFormData.append(data1, withName: "avatar", fileName:"image.png", mimeType: "image/png")

                
            }
            
            
        }, to: self.baseURL + "wiloke/v2/update_profile", method: .post, headers: headers, encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<ProfilUpdateResponse>) in
                    
                    if let statusCode = response.response?.statusCode{
                        switch statusCode{
                        case 200:
                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                        default:
                            completionHandler(false,response.result.value)
                        }
                    }else{
                        completionHandler(false,response.result.value)
                    }
                }
            case .failure(_):
                print("error")
                //completionHandler(false,response.result.value)
            }
        })
    }
    func getChatHeaders(completionHandler:@escaping (  _ success: Bool,   _ response: chatHeadersData1?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/get-authors-chatted"
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: headers).validate().responseObject { (response: DataResponse<chatHeadersData1>) in
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func getChatList(page:Int,chatID:String,completionHandler:@escaping (  _ success: Bool,   _ response: chatHeadersData1?) -> ()) {
        
        let requestURL =  "http://demo.bestweb.com.my/threesector/wp-json/wiloke/v2/get-author-messages?chatFriendID=\(chatID)&postsPerPage=\(page)"
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        DispatchQueue.global().async {
            AlamoManager.request(requestURL, method:.get, parameters: nil, encoding: JSONEncoding.default,headers: headers).validate().responseObject { (response: DataResponse<chatHeadersData1>) in
                if let statusCode = response.response?.statusCode{
                    switch statusCode{
                    case 200:
                        response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                    default:
                        completionHandler(false,response.result.value)
                    }
                }else{
                    completionHandler(false,response.result.value)
                }
            }
        }
        
    }
    func sendMessageAPI(param:[String: Any],completionHandler:@escaping (  _ success: Bool,   _ response: sendMessageData?) -> ()) {
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if value is String{
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                }else if value is Int{
                    multipartFormData.append((value as! Int).toString().data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: false)!, withName: key)
                }else{
                    print("key",key)
                }
            }
        }, to: self.baseURL + "wiloke/v2/send-message", method: .post, headers: headers, encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<sendMessageData>) in
                    
                    if let statusCode = response.response?.statusCode{
                        switch statusCode{
                        case 200:
                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                        default:
                            completionHandler(false,response.result.value)
                        }
                    }else{
                        completionHandler(false,response.result.value)
                    }
                }
            case .failure(_):
                print("error")
                // completionHandler(false,response.result.value)
            }
        })
    }
    func AddReviewAPI(param:[String: Any],completionHandler:@escaping (  _ success: Bool,   _ response: AddReviewData?) -> ()) {
        var headers = [String: String]()
        
        if let token = s_default.value(forKey: "token") as? String {
            headers = [
                "Authorization": "Bearer \(token)"
            ]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if value is String{
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: key)
                }else if value is Int{
                    multipartFormData.append((value as! Int).toString().data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: false)!, withName: key)
                }else{
                    print("key",key)
                }
            }
        }, to: self.baseURL + "wiloke/v2/custom-post-review", method: .post, headers: headers, encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                upload.validate().responseObject {(response: DataResponse<AddReviewData>) in
                    
                    if let statusCode = response.response?.statusCode{
                        switch statusCode{
                        case 200:
                            response.result.isSuccess ? completionHandler(true,response.result.value) : completionHandler(false,response.result.value)
                        default:
                            completionHandler(false,response.result.value)
                        }
                    }else{
                        completionHandler(false,response.result.value)
                    }
                }
            case .failure(_):
                print("error")
                // completionHandler(false,response.result.value)
            }
        })
    }
}

