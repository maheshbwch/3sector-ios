//
//  Extension.swift
//  ThreeSector
//
//  Created by BestWeb on 24/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    
    func addShadow(){
        //Remove previous shadow views
        superview?.viewWithTag(119900)?.removeFromSuperview()
        
        //Create new shadow view with frame
        let shadowView = UIView(frame: frame)
        shadowView.tag = 119900
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 2, height: 3)
        shadowView.layer.masksToBounds = false
        
        shadowView.layer.shadowOpacity = 0.3
        shadowView.layer.shadowRadius = 3
        shadowView.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        shadowView.layer.rasterizationScale = UIScreen.main.scale
        shadowView.layer.shouldRasterize = true
        
        superview?.insertSubview(shadowView, belowSubview: self)
    }
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        layer.shadowRadius = 10
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func roundCorners(cornerRadius: Double) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    
    
}
extension Date {
    static var currentTimeStamp: Int64{
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
}
extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy"
        //        dateFormatter.timeStyle = .none
        //        dateFormatter.dateStyle = .short
        
        let formattedDateString = dateFormatter.string(from: date)
        
        if years(from: date)   > 0 { return formattedDateString  }
        if months(from: date)  > 0 {
            return "\(months(from: date)) month ago"
            
        }
        if weeks(from: date)   > 0 {
            return "\(weeks(from: date)) week ago"
        }
        if days(from: date)    > 0 {
            return "\(days(from: date)) days ago"
        }
        if hours(from: date)   > 0 {
            return "\(hours(from: date)) hours ago"
        }
        if minutes(from: date) > 0 {
            return "\(minutes(from: date)) minutes ago"
        }
        if seconds(from: date) > 0 {
            return "\(seconds(from: date)) seconds ago"
        }
        return "Just now"
    }
}

extension UIViewController{
    
    func showAlertWithDelay(withTitle title: String, message: String?, dismissParent: Bool){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.present(alert, animated: true, completion: {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                alert.dismiss(animated: true, completion: {
                    if dismissParent{
                        self.navigationController?.popViewController(animated: true)
                        //self.dismiss(animated: true, completion: {self.dismiss(animated: true, completion: nil)})
                    }
                })
            })
        })
    }
    
    
    func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    func customAttribute(fullText: String, fullTextColor: UIColor , fullTextFont : UIFont , toAttributeText: String , toTextColor:UIColor , toTextFont: UIFont) -> NSAttributedString {
        
        let globalAttribute = [NSAttributedString.Key.font:fullTextFont,NSAttributedString.Key.foregroundColor:fullTextColor]
        
        
        let range = (fullText as NSString).range(of: toAttributeText)
        let priceAttributedStrings = NSMutableAttributedString(string: fullText, attributes: globalAttribute)
        priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:toTextColor,NSAttributedString.Key.font:toTextFont], range: range)
        
        return priceAttributedStrings
    }
    
    func customAttribute1(fullText: String, fullTextColor: UIColor , fullTextFont : UIFont) -> NSAttributedString {
        
        let globalAttribute = [NSAttributedString.Key.font:fullTextFont,NSAttributedString.Key.foregroundColor:fullTextColor]
        
        
        let range = (fullText as NSString).range(of: fullText)
        let priceAttributedStrings = NSMutableAttributedString(string: fullText, attributes: globalAttribute)
        priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:fullTextColor,NSAttributedString.Key.font:fullTextFont], range: range)
        
        return priceAttributedStrings
    }
    
    func customAttributeThree(fullText: String, fullTextColor: UIColor , fullTextFont : UIFont , toAttributeText1: String , toTextColor1:UIColor , toTextFont1: UIFont,toAttributeText2: String , toTextColor2:UIColor , toTextFont2: UIFont) -> NSAttributedString {
        
        let globalAttribute = [NSAttributedString.Key.font:fullTextFont,NSAttributedString.Key.foregroundColor:fullTextColor]
        let priceAttributedStrings = NSMutableAttributedString(string: fullText, attributes: globalAttribute)
        
        let range1 = (fullText as NSString).range(of: toAttributeText1)
        priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:toTextColor1, NSAttributedString.Key.font:toTextFont1], range: range1)
        
        let range2 = (fullText as NSString).range(of: toAttributeText2)
        //priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:toTextColor2,NSAttributedString.Key.font:toTextFont2], range: range2)
        
        //let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "Your Text")
        priceAttributedStrings.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: range2)//NSMakeRange(0, attributeString.length))
        
        return priceAttributedStrings
    }
    
    func customAttributeTwo(fullText: String, fullTextColor: UIColor , fullTextFont : UIFont , toAttributeText1: String , toTextColor1:UIColor , toTextFont1: UIFont,toAttributeText2: String , toTextColor2:UIColor , toTextFont2: UIFont) -> NSAttributedString {
        
        let globalAttribute = [NSAttributedString.Key.font:fullTextFont,NSAttributedString.Key.foregroundColor:fullTextColor]
        let priceAttributedStrings = NSMutableAttributedString(string: fullText, attributes: globalAttribute)
        
        let range1 = (fullText as NSString).range(of: toAttributeText1)
        priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:toTextColor1, NSAttributedString.Key.font:toTextFont1], range: range1)
        
        let range2 = (fullText as NSString).range(of: toAttributeText2)
        //priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:toTextColor2,NSAttributedString.Key.font:toTextFont2], range: range2)
        
        //let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "Your Text")
        priceAttributedStrings.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: range2)//NSMakeRange(0, attributeString.length))
        
        return priceAttributedStrings
    }
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alertSessionExpired(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
       // let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
        }
       
        
        // Add the actions
        alertController.addAction(okAction)
      //  alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
//    func logOut(){
//        isUserLogin = false
//        loginUsername = ""
//        loginUserImg = ""
//        loginUserEmail = ""
//        
//        UserDefaults.standard.removeObject(forKey: "LoginStatus")
//        UserDefaults.standard.removeObject(forKey: "UserID")
//        UserDefaults.standard.removeObject(forKey: "UserName")
//    }
    
}

extension UITableViewController{
    func customAttributeTable(fullText: String, fullTextColor: UIColor , fullTextFont : UIFont , toAttributeText: String , toTextColor:UIColor , toTextFont: UIFont) -> NSAttributedString {
        
        let globalAttribute = [NSAttributedString.Key.font:fullTextFont,NSAttributedString.Key.foregroundColor:fullTextColor]
        
        
        let range = (fullText as NSString).range(of: toAttributeText)
        let priceAttributedStrings = NSMutableAttributedString(string: fullText, attributes: globalAttribute)
        priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:toTextColor,NSAttributedString.Key.font:toTextFont], range: range)
        
        return priceAttributedStrings
    }
}

extension UITableViewCell{
    
    //    func alert(message: String, title: String = "") {
    //        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    //        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    //        alertController.addAction(OKAction)
    //        self.present(alertController, animated: true, completion: nil)
    //    }
    //
    func customAttributeTable(fullText: String, fullTextColor: UIColor , fullTextFont : UIFont , toAttributeText: String , toTextColor:UIColor , toTextFont: UIFont) -> NSAttributedString {
        
        let globalAttribute = [NSAttributedString.Key.font:fullTextFont,NSAttributedString.Key.foregroundColor:fullTextColor]
        let priceAttributedStrings = NSMutableAttributedString(string: fullText, attributes: globalAttribute)
        
        let range = (fullText as NSString).range(of: toAttributeText)
        priceAttributedStrings.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: range)
        
        // priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:toTextColor,NSAttributedString.Key.font:toTextFont], range: range)
        
        return priceAttributedStrings
    }
    
    func customAttribute1(fullText: String, fullTextColor: UIColor , fullTextFont : UIFont) -> NSAttributedString {
        
        let globalAttribute = [NSAttributedString.Key.font:fullTextFont,NSAttributedString.Key.foregroundColor:fullTextColor]
        
        
        let range = (fullText as NSString).range(of: fullText)
        let priceAttributedStrings = NSMutableAttributedString(string: fullText, attributes: globalAttribute)
        priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:fullTextColor,NSAttributedString.Key.font:fullTextFont], range: range)
        
        return priceAttributedStrings
    }
    
    func customAttribute(fullText: String, fullTextColor: UIColor , fullTextFont : UIFont , toAttributeText1: String , toTextColor1:UIColor , toTextFont1: UIFont,toAttributeText2: String , toTextColor2:UIColor , toTextFont2: UIFont) -> NSAttributedString {
        
        let globalAttribute = [NSAttributedString.Key.font:fullTextFont,NSAttributedString.Key.foregroundColor:fullTextColor]
        let priceAttributedStrings = NSMutableAttributedString(string: fullText, attributes: globalAttribute)
        
        let range1 = (fullText as NSString).range(of: toAttributeText1)
        priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:toTextColor1, NSAttributedString.Key.font:toTextFont1], range: range1)
        
        let range2 = (fullText as NSString).range(of: toAttributeText2)
        //priceAttributedStrings.addAttributes([NSAttributedString.Key.foregroundColor:toTextColor2,NSAttributedString.Key.font:toTextFont2], range: range2)
        
        //let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "Your Text")
        priceAttributedStrings.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: range2)//NSMakeRange(0, attributeString.length))
        
        return priceAttributedStrings
    }
}

class ButtonWithImage: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 40), bottom: 5, right: 5)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
        }
    }
}

public class BadgeBarButtonItem: UIBarButtonItem
{
    @IBInspectable
    public var badgeNumber: Int = 0 {
        didSet {
            self.updateBadge()
        }
    }
    
    private let label: UILabel
    
    required public init?(coder aDecoder: NSCoder)
    {
        let label = UILabel()
        label.backgroundColor = .red
        label.alpha = 0.9
        label.layer.cornerRadius = 9
        label.clipsToBounds = true
        label.isUserInteractionEnabled = false
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .white
        label.layer.zPosition = 1
        self.label = label
        
        super.init(coder: aDecoder)
        
        self.addObserver(self, forKeyPath: "view", options: [], context: nil)
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        self.updateBadge()
    }
    
    private func updateBadge()
    {
        guard let view = self.value(forKey: "view") as? UIView else { return }
        
        self.label.text = "\(badgeNumber)"
        
        if self.badgeNumber > 0 && self.label.superview == nil
        {
            view.addSubview(self.label)
            
            self.label.widthAnchor.constraint(equalToConstant: 18).isActive = true
            self.label.heightAnchor.constraint(equalToConstant: 18).isActive = true
            self.label.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 9).isActive = true
            self.label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -9).isActive = true
        }
        else if self.badgeNumber == 0 && self.label.superview != nil
        {
            self.label.removeFromSuperview()
        }
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: "view")
    }
}

extension Int
{
    func toString() -> String
    {
        var myString = String(self)
        return myString
    }
}

