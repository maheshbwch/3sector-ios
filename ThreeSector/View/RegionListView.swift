//
//  RegionListView.swift
//  ThreeSector
//
//  Created by BestWeb on 20/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
protocol RegionsDelegate {
    func selectedRegionTitle(CategoryName: String,ID: Int)
    func back()
}
class RegionListView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var tblData: UITableView!
    var delegate:RegionsDelegate?
    //    var bankNames = ["Affin Bank", "Ambank", "Bank Islam","Bank Rakyant","Bank Simpanan Nasional","CIMB Bank","Hong Leong Bank","HSBC","Maybank2U","Public Bank","RHB Now","Standard Chartered","Uinted Overseas Bank"]
    //        var bankImages = ["affinbank 1", "ambank 1", "bankislam 1","bankrakyat 1","bsn 1","cimb 1","hong leong connect 1","hsbc 1","maybank 1","public bank 1","rhb now 1","standardchartered 1","Group 533"]
    var regionNames = [String]()
    
    var searchListData = searchFieldData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        outerView.layer.cornerRadius = 6
        //        outerView.addShadow()
        tblData.isHidden = true
        tblData.layer.shadowColor = UIColor.gray.cgColor
        tblData.layer.shadowOpacity = 0.3
        tblData.layer.shadowOffset = CGSize.zero
        tblData.layer.shadowRadius = 6
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutside)))
        
        self.getCategoryList()
        
        // Do any additional setup after loading the view.
    }
    @objc func dismissOnTapOutside(){
        self.dismiss(animated: true, completion: nil)
    }
   
    func getCategoryList(){
        LoadingIndicatorView.show("Loading...")
        service.getSearchListingAPI(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if success{
                    if userInfo?.status == "success"{
                        self.searchListData = userInfo!
//                        for name in getSearchListingData.oResults[5].options{
//                            let tag = Tag()
//                            tag.name = name.name
//                            self.tags.append(tag)
//                        }
                        self.tblData.isHidden = false
                        self.tblData.delegate = self
                        self.tblData.dataSource = self
                        self.tblData.keyboardDismissMode = .onDrag
                        self.tblData.reloadData()
                    }else{
                        
                        self.view.makeToast( userInfo!.status, duration: 2.0, position: .bottom)
                    }
                }else{
                    
                    self.view.makeToast(userInfo!.status, duration: 2.0, position: .bottom)
                }
            }
            
        })
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchListData.oResults[3].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "regionListCell", for: indexPath) as! regionListCell
        cell.categoryName.text = searchListData.oResults[3].options[indexPath.row].name
        cell.viewBG.layer.shadowColor = UIColor.gray.cgColor
        cell.viewBG.layer.shadowOpacity = 0.3
        cell.viewBG.layer.shadowOffset = CGSize.zero
        cell.viewBG.layer.shadowRadius = 6
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedRegionTitle(CategoryName: searchListData.oResults[3].options[indexPath.row].name, ID: searchListData.oResults[3].options[indexPath.row].id)
        self.view.removeFromSuperview()
        //        //        dismiss(animated: false, completion: {  DispatchQueue.main.async{
        //        //
        //        //        } })
        //
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
class regionListCell:UITableViewCell{
    @IBOutlet weak var categoryName : UILabel!
    @IBOutlet weak var viewBG : UIView!
}
