//
//  CategoriesListView.swift
//  ThreeSector
//
//  Created by BestWeb on 19/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
protocol CategoriesDelegate {
    func selectedTitle(CategoryName: String,ID: Int)
    func back()
}
class CategoriesListView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var tblData: UITableView!
    var delegate:CategoriesDelegate?
    //    var bankNames = ["Affin Bank", "Ambank", "Bank Islam","Bank Rakyant","Bank Simpanan Nasional","CIMB Bank","Hong Leong Bank","HSBC","Maybank2U","Public Bank","RHB Now","Standard Chartered","Uinted Overseas Bank"]
    //        var bankImages = ["affinbank 1", "ambank 1", "bankislam 1","bankrakyat 1","bsn 1","cimb 1","hong leong connect 1","hsbc 1","maybank 1","public bank 1","rhb now 1","standardchartered 1","Group 533"]
    var categoryNames = [String]()
    var categoryID : Int = 0
    var categoryData = CategoryResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        outerView.layer.cornerRadius = 6
        outerView.addShadow()
        tblData.isHidden = true
        tblData.layer.shadowColor = UIColor.gray.cgColor
        tblData.layer.shadowOpacity = 0.3
        tblData.layer.shadowOffset = CGSize.zero
        tblData.layer.shadowRadius = 6
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutside)))

        self.getCategoryList()
        
        // Do any additional setup after loading the view.
    }
    @objc func dismissOnTapOutside(){
        self.dismiss(animated: true, completion: nil)
    }
    func getCategoryList(){
        LoadingIndicatorView.show("Creating...")
        service.getCategoriesList(completionHandler: {success, userInfo in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            print("Response",userInfo!)
            
            if success{
                self.categoryData = userInfo!
                if userInfo?.status == "success"{
                    self.tblData.isHidden = false
                    self.tblData.delegate = self
                    self.tblData.dataSource = self
                    self.tblData.keyboardDismissMode = .onDrag
                    self.tblData.reloadData()
                }else{
                    self.alert(message: userInfo!.status)
                }
            }else{
                self.alert(message: userInfo!.status)
            }
        })
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryListCell", for: indexPath) as! categoryListCell
        cell.categoryName.text = self.categoryData.data[indexPath.row].name
        cell.viewBG.layer.shadowColor = UIColor.gray.cgColor
        cell.viewBG.layer.shadowOpacity = 0.3
        cell.viewBG.layer.shadowOffset = CGSize.zero
        cell.viewBG.layer.shadowRadius = 6
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "CategoryPop", sender: self)
        categoryID = self.categoryData.data[indexPath.row].term_id
        self.delegate?.selectedTitle(CategoryName: self.categoryData.data[indexPath.row].name, ID: self.categoryData.data[indexPath.row].term_id)
        self.view.removeFromSuperview()
//        //        dismiss(animated: false, completion: {  DispatchQueue.main.async{
//        //
//        //        } })
//        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CategoryPop"{
            let vc = segue.destination as! CategoryController
            vc.categoryId = categoryID
            //vc.listingId = categoriesById.data[tagValue].ID
        }
    }
    
}
class categoryListCell:UITableViewCell{
    @IBOutlet weak var categoryName : UILabel!
    @IBOutlet weak var viewBG : UIView!
}
