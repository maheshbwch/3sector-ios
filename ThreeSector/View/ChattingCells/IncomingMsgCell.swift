//
//  IncomingMsgCell.swift
//  ThreeSector
//
//  Created by BestWeb on 24/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit

class IncomingMsgCell: UITableViewCell {

    @IBOutlet weak var imgIncoming: UIImageView!
    @IBOutlet weak var incomingTtl: UILabel!
    @IBOutlet weak var incomingTime: UILabel!
    @IBOutlet weak var incomingView: UIView!
    @IBOutlet weak var incomingMessage: UILabel!
    @IBOutlet weak var incomingImg: UIImageView!
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var leadingView: NSLayoutConstraint!
    override func awakeFromNib() {
        incomingView.layer.shadowColor = UIColor.black.cgColor;
      //  incomingView.layer.shadowOpacity = 0.5;
        incomingView.layer.shadowRadius  = 5;
        incomingView.layer.shadowOffset  = CGSize(width :0, height :0)
        incomingView.layer.masksToBounds = false;
        incomingView.layer.cornerRadius  =  5.0;
        incomingView.layer.borderWidth   = 0;
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
