//
//  OutgoingMsgCell.swift
//  ThreeSector
//
//  Created by BestWeb on 24/02/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit

class OutgoingMsgCell: UITableViewCell {

    @IBOutlet weak var timeOut: UILabel!
    @IBOutlet weak var viewOutGoing: UIView!
    @IBOutlet weak var outGoingText: UILabel!
    @IBOutlet weak var imageOut: UIImageView!
    @IBOutlet weak var imgOutgoing: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewOutGoing.layer.shadowColor = UIColor.black.cgColor;
        //viewOutGoing.layer.shadowOpacity = 0.5;
        viewOutGoing.layer.shadowRadius  = 5;
        viewOutGoing.layer.shadowOffset  = CGSize(width :0, height :0)
        viewOutGoing.layer.masksToBounds = false;
        viewOutGoing.layer.cornerRadius  =  5.0;
        viewOutGoing.layer.borderWidth   = 0;    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
