//
//  AppDelegate.swift
//  ThreeSector
//
//  Created by BestWeb on 21/01/20.
//  Copyright © 2020 BestWeb. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKLoginKit
import FBSDKCoreKit
import Firebase
import GoogleMaps
import GooglePlaces
import UserNotifications

var loginUserID = Int()

let service = eService(baseURL: BASE_URL)
var categoriesList = CategoryResponse()
var categoriesById = singleCategoryResponse()
var categoriesById1 = singleCategoryResponse()
var categoriesById2 = singleCategoryResponse()
var categoriesById3 = singleCategoryResponse()
var categoriesById4 = singleCategoryResponse()

var newsDataDetails = newsData()
var loginData = LoginResponse()
var profileDetails = profileData()
var favouriteDetails = favouritesData()
var listingData = singleCategoryResponse()
var projectsData = singleCategoryResponse()
var getReviewsData = reviewsData()
var getSearchListingData = searchFieldData()
var getListingTasData = listingTagsResponse()
var getChatHeaders = chatHeadersData1()
var FCMTOKEN = String()
var DEVICE_ID = UIDevice.current.identifierForVendor!.uuidString

var isAuthor : Bool = false
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        FirebaseApp.configure()

        let userDefault = UserDefaults.standard
        GMSServices.provideAPIKey("AIzaSyBkbyapeBS0v13NPHTdz8M78z9llWalIEY")
        GMSPlacesClient.provideAPIKey("AIzaSyBkbyapeBS0v13NPHTdz8M78z9llWalIEY")
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        UserDefaults.standard.set(token, forKey: "FCMToken")
        //FCMTOKEN = token!
        
        //Added Code to display notification when app is in Foreground
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }

        let savedData = userDefault.bool(forKey: "isLoggedIn")
        if(savedData){
            if let userId = UserDefaults.standard.value(forKey: "Userid") as? Int{
                loginUserID = userId
            }
            var storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController: UITabBarController = storyboard.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
            
            window?.rootViewController = viewController
            window?.makeKeyAndVisible()
            
        }else{
            // this is the main view. just make the object of the class and called it.
        }
        // Override point for customization after application launch.
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        return true
    }
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
        ) -> Bool {
        return ApplicationDelegate.shared.application(
            app,
            open: url,
            options: options
        )
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.hexString
        print(deviceTokenString)
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        // Print full message.
        print(userInfo)
        
    }
    
    // This method will be called when app received push notifications in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    { completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
    }
    
    
    // MARK:- Messaging Delegates
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                UserDefaults.standard.set(result.token, forKey: "FCMToken")
                FCMTOKEN = result.token
            }
        }
    }
    
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("received remote notification")
    }
    
    
    
}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
